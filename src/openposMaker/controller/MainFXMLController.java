package openposMaker.controller;

import java.beans.Statement;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import openposMaker.model.dao.CardTypeDao;
import openposMaker.model.dao.DocumentDao;
import openposMaker.model.dao.EmployeeDao;
import openposMaker.model.dao.IncidenceDao;
import openposMaker.model.dao.InterfaceDao;
import openposMaker.model.dao.PeripheralDao;
import openposMaker.model.dao.PosTerminalDao;
import openposMaker.model.dao.PosTerminalGeneralParameterDao;
import openposMaker.model.dao.TaxInfoDao;
import openposMaker.model.dto.CompanyInfoDto;
import openposMaker.model.dto.EmployeeDto;
import openposMaker.model.dto.IncidenceDto;
import openposMaker.model.dto.PosTerminalDto;
import openposMaker.model.dto.SiteInfoDto;
import openposMaker.model.entities.AdditionalParameter;
import openposMaker.model.entities.CardType;
import openposMaker.model.entities.CardTypeDataList;
import openposMaker.model.entities.Document;
import openposMaker.model.entities.Employee;
import openposMaker.model.entities.Incidence;
import openposMaker.model.entities.OpenShiftTimeSlot;
import openposMaker.model.entities.OpenposUnit;
import openposMaker.model.entities.Peripheral;
import openposMaker.model.entities.PosTerminal;
import openposMaker.model.entities.PosTerminalAdditionalParameter;
import openposMaker.model.entities.SectionParam;
import openposMaker.model.entities.TaxInfo;
import openposMaker.utils.JSONFileChooser;
import openposMaker.utils.JSONReader;
import openposMaker.utils.JSONSaveFileChooser;
import openposMaker.utils.JSONWriter;

public class MainFXMLController {

	private CompanyInfoDto companyDto;
	private IncidenceDto incidenceDto;
	private SiteInfoDto siteDto;
	private PosTerminalDto posTerminalDto;
	private EmployeeDto employeeDto;

	@FXML
	private MenuItem menuItemOpenFile;

	@FXML
	private MenuItem menuItemNewFile;

	@FXML
	private MenuItem menuItemSaveFileAs;

	@FXML
	private MenuItem menuItemSaveFile;

	@FXML
	private MenuItem menuItemExit;

	@FXML
	private AnchorPane mainAnchorPane;

	@FXML
	private TextField companyCode;

	@FXML
	private TextField companyShortName;

	@FXML
	private TextField companyLongName;

	@FXML
	private TextField addressTypeCompany;

	@FXML
	private TextField addressCompany;

	@FXML
	private TextField addressNumberCompany;

	@FXML
	private TextField postCodeCompany;

	@FXML
	private TextField cityCompany;

	@FXML
	private TextField phoneNumberCompany;

	@FXML
	private TextField companyMainCurrencyCode;

	@FXML
	private TextField companyMainCurrencyName;

	@FXML
	private TextField companyMainCurrencyDecimals;

	@FXML
	private TextField companyMainCurrencyValue;

	@FXML
	private TextField companySecondaryCurrencyCode;

	@FXML
	private TextField companySecondaryCurrencyName;

	@FXML
	private TextField companySecondaryCurrencyDecimals;

	@FXML
	private TextField companySecondaryCurrencyValue;

	@FXML
	private TextField siteCode;

	@FXML
	private TextField siteVat;

	@FXML
	private TextField siteName;

	@FXML
	private TextField siteAddress;

	@FXML
	private TextField sitePostCode;

	@FXML
	private TextField siteCity;

	@FXML
	private TextField siteProvince;

	@FXML
	private TextField siteGreetingFirstLine;

	@FXML
	private TextField siteGreetingSecondLine;

	@FXML
	private ComboBox<String> siteNumberOfShifts;

	@FXML
	private CheckBox siteCommissionist;

	@FXML
	private TextField siteOfferFamily;

	@FXML
	private TextField siteServiceFamily;

	@FXML
	private TextField siteDiscountItem;

	@FXML
	private TableView<String> seriesTable;

	@FXML
	private TableColumn<String, String> seriesTableColumn;

	@FXML
	private TableView<PosTerminal> posTerminalConfiguratorTable;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTableCodeColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTablePortColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTablePrinterProcessColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTableInvoiceFormatColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTableCloseDocFormatColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTableControlPumpColumn;

	@FXML
	private TableColumn<PosTerminal, String> posTerminalConfiguratorTableControlAuthColumn;

	@FXML
	private ComboBox<String> posDocumentsTpvSelector;

	@FXML
	private TableView<Document> posTerminalDocumentsTable;

	@FXML
	private TableColumn<Document, String> posDocumentsTableIdColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableSerieColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableFormColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableCopiesColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableRegColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableRefundSerieColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableRefundFormColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableRefundCopiesColumn;

	@FXML
	private TableColumn<Document, String> posDocumentsTableRefundRegColumn;

	@FXML
	private ComboBox<String> posPeripheralsTpvSelector;

	@FXML
	private TableView<Peripheral> posTerminalPeripheralsTable;

	@FXML
	private TableColumn<Peripheral, String> posPeripheralsTableIdColumn;

	@FXML
	private TableColumn<Peripheral, String> posPeripheralsTableTypeColumn;

	@FXML
	private TableColumn<Peripheral, String> posPeripheralsTableDriverColumn;

	@FXML
	private TableColumn<Peripheral, String> posPeripheralsTablePortColumn;

	@FXML
	private TextField TPVINIGeneralParamName;

	@FXML
	private TextField TPVINIGeneralParamValue;

	@FXML
	private Button btnConfirmTPVINIGeneralParam;

	@FXML
	private Tab tabPosTerminalsGeneralParameters;

	@FXML
	private ComboBox<String> posTerminalGeneralParametersTpvSelector;

	@FXML
	private TableView<SectionParam> posTerminalGeneralParametersTable;

	@FXML
	private TableColumn<SectionParam, String> posGeneralParametersTableNameColumn;

	@FXML
	private TableColumn<SectionParam, String> posGeneralParametersTableValueColumn;

	@FXML
	private Tab tabPosTerminalsAdditionalParameters;

	@FXML
	private ComboBox<String> posAdditionalParametersTpvSelector;

	@FXML
	private TableView<PosTerminalAdditionalParameter> posTerminalAdditionalParametersTable;

	@FXML
	private TableColumn<PosTerminalAdditionalParameter, String> posAdditionalParametersTableNameColumn;

	@FXML
	private TableColumn<PosTerminalAdditionalParameter, String> posAdditionalParametersTableValueColumn;

	@FXML
	private TextField incidenceCode;

	@FXML
	private TextField incidenceName;

	@FXML
	private CheckBox incidenceModifiable;

	@FXML
	private ComboBox<String> incidenceType;

	@FXML
	private TextField incidenceAccount;

	@FXML
	private TextField incidenceAccountingConcept;

	@FXML
	private Button btnClearIncidence;

	@FXML
	private TableView<Incidence> incidencesTable;

	@FXML
	private TableColumn<Incidence, String> incidencesTableCodeColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableNameColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableTypeColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableUseColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableAccountColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableAccountingConceptColumn;

	@FXML
	private TableColumn<Incidence, String> incidencesTableModificableColumn;

	@FXML
	private TextField employeePrefCode;

	@FXML
	private TextField employeeInitNumber;

	@FXML
	private TextField employeeMaxNumber;

	@FXML
	private Button btnClearEmployee;

	@FXML
	private Button btnConfirmEmployee;

	@FXML
	private Button btnConfirmIncidence;

	@FXML
	private TableView<Employee> employeesTable;

	@FXML
	private TableColumn<Employee, String> employeesTableCodeColumn;

	@FXML
	private TableColumn<Employee, String> employeesTableNameColumn;

	@FXML
	private TableColumn<Employee, String> employeesTableVatColumn;

	@FXML
	private TableColumn<Employee, String> employeesTableLeftHandedColumn;

	@FXML
	private ComboBox<String> taxInfoType;

	@FXML
	private VBox taxInfoIdContainer;

	@FXML
	private TextField taxInfoId;

	@FXML
	private VBox taxInfoNameContainer;

	@FXML
	private TextField taxInfoName;

	@FXML
	private VBox taxInfoValueContainer;

	@FXML
	private TextField taxInfoValue;

	@FXML
	private VBox taxInfoChecBoxesContainer;

	@FXML
	private CheckBox taxInfoIVA;

	@FXML
	private CheckBox taxInfoIGIC;

	@FXML
	private Button btnConfirmTaxInfo;

	@FXML
	private TableView<TaxInfo> taxTable;

	@FXML
	private TableColumn<TaxInfo, String> taxIdColumn;

	@FXML
	private TableColumn<TaxInfo, String> taxNameColumn;

	@FXML
	private TableColumn<TaxInfo, String> taxValueColumn;

	@FXML
	private TextField cardTypeId;

	@FXML
	private TextField cardTypeName;

	@FXML
	private ComboBox<String> cardTypeAutomatic;

	@FXML
	private ComboBox<String> cardTypeFunction;

	@FXML
	private CheckBox cardTypeAuthorisation;

	@FXML
	private TextField cardTypeCenterType;

	@FXML
	private TextField cardTypeVehicle;

	@FXML
	private TextField cardTypeUser;

	@FXML
	private TextField cardTypeActivateOperative;

	@FXML
	private TextField cardTypeManageType;

	@FXML
	private TextField cardTypeIncidencePoint;

	@FXML
	private CheckBox cardTypeOdometer;

	@FXML
	private CheckBox cardTypeLicense;

	@FXML
	private TableView<CardType> cardTypesTable;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnId;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnName;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnFunction;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnUser;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnVehicle;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnAuthorisation;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnCenterType;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnPointsIncidence;

	@FXML
	private TableColumn<CardType, String> cardTypesColumnBines;

	@FXML
	TableColumn<CardType, Void> cardTypesColumnActions;

	@FXML
	private TableView<String> cardTypeBinesTable;

	@FXML
	private TableColumn<String, String> cardTypeBinesTableBinColumn;

	@FXML
	private TableColumn<String, Void> cardTypeBinesTableActionColumn;

	@FXML
	private TextField posConfigurationInitialCode;

	@FXML
	private TextField posConfigurationMaxNumber;

	@FXML
	private TextField posConfigurationInitialIP;

	@FXML
	private ComboBox<String> posConfigurationPrinterProcess;

	@FXML
	private ComboBox<String> posConfigurationInvoiceFormat;

	@FXML
	private ComboBox<String> posConfigurationCloseDocFormat;

	@FXML
	private CheckBox posConfigurationControlPumps;

	@FXML
	private CheckBox posConfigurationControlAuth;

	@FXML
	private CheckBox posConfigurationDrawer;

	@FXML
	private CheckBox posConfigurationVisor;

	@FXML
	private Button btnConfirmPosConfiguration;

	@FXML
	private Button btnConfirmCardType;

	@FXML
	private Button btnConfirmCompanyData;

	@FXML
	private Button btnConfirmSiteInfoEstablishment;

	@FXML
	private Button btnConfirmSiteInfoGeneralParameters;

	@FXML
	private Button btnConfirmSiteInfoEstablishmentConfig;

	@FXML
	private Button btnConfirmCurrencyCompanyData;

	@FXML
	private Button btnClearCardType;

	@FXML
	private TextField cardTypeBine;

	@FXML
	private Button btnAddBine;

	@FXML
	private Tab tabEmployees;

	@FXML
	private Tab tabPosTerminals;

	@FXML
	private Tab tabPosTerminalsConfiguration;

	@FXML
	private Tab tabPosTerminalsDocuments;

	@FXML
	private Tab tabPosTerminalsPeripherals;
	@FXML
	private Tab tabPosTPVINI;

	@FXML
	private TabPane mainTab;

	private InterfaceDao<CardType> listCardTypes;
	private InterfaceDao<PosTerminal> listPosTerminals;
	private InterfaceDao<Document> documents;
	private InterfaceDao<Peripheral> peripherals;
	private PosTerminalGeneralParameterDao listPosTerminalGeneralParameters;
	private InterfaceDao<Employee> listEmployees;
	private InterfaceDao<Incidence> listIncidences;
	private InterfaceDao<TaxInfo> listTaxInfo;
	private OpenposUnit openposUnit;

	private String programName = "OpenPOS Maker";
	private Stage stage;
	private SimpleStringProperty fileSelectedName;

	@FXML
	private void initialize() {
		try {
			mainTab.setDisable(true);
			initializeMenuItems();
			activeMenuSaveFileItem(true);
			initializeFileSelectedProperty();
			mainAnchorPane.sceneProperty().addListener((observableScene, oldScene, newScene) -> {
				System.out.println(observableScene);
				if (oldScene == null && newScene != null) {
					newScene.windowProperty().addListener((observableWindow, oldWindow, newWindow) -> {
						if (oldWindow == null && newWindow != null) {
							stage = (Stage) newScene.getWindow();
							Platform.runLater(() -> {
								showStartAlert();
							});
//							createNewFile();
						}
					});
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initializeMenuItems() {
		this.menuItemOpenFile.setOnAction(event -> {
			openFile();
		});
		this.menuItemNewFile.setOnAction(event -> {
			createNewFile();
		});
		this.menuItemSaveFile.setOnAction(event -> {
			saveFile();
		});
		this.menuItemSaveFileAs.setOnAction(event -> {
			saveFileAs();
		});
		this.menuItemExit.setOnAction(event -> {
			exit();
		});
	}

	private void initializeFileSelectedProperty() {
		fileSelectedName = new SimpleStringProperty();
		fileSelectedName.addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				changeWindowTitle(newValue);
				activeMenuSaveFileItem(newValue.equals("Nuevo archivo"));
			}
		});

	}

	private void activeMenuSaveFileItem(boolean value) {
		Platform.runLater(() -> {
			menuItemSaveFile.setDisable(value);
		});

	}

	private void changeWindowTitle(String title) {
		Platform.runLater(() -> {
			stage.setTitle(programName + " - " + title);
		});
	}

	private void openFile() {
		String src = "";
		try {
			mainTab.setDisable(true);
			src = JSONFileChooser.chooseFile(stage);

//			System.out.println(src);
			File file = new File(src);
			openposUnit = JSONReader.readJSONFile(file, OpenposUnit.class);

			this.fileSelectedName.set(src);
			System.out.println("Archivo abierto: " + src);
			initializeTabs(openposUnit);

		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			if (mainTab.isDisabled()) {
				showStartAlert();
			}
		} catch (UnrecognizedPropertyException e) {
			System.out.println(e.getLocalizedMessage());
			showAlert("ERROR", "Fichero incompleto: " + src, e.getMessage());
			initializeMainTab();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveFileAs() {
		try {
			String src = JSONSaveFileChooser.saveFile((Stage) mainAnchorPane.getScene().getWindow());
			File target = new File(src);
			JSONWriter.writeJSONFile(target, openposUnit);
			this.fileSelectedName.set(src);
			System.out.println("Archivo guardado como: " + src);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void exit() {
		System.out.println("Saliendo del programa");
		stage.close();
//		System.exit(0);
	}

	private void saveFile() {
		try {
			String src = this.fileSelectedName.get();
			File target = new File(src);
			JSONWriter.writeJSONFile(target, openposUnit);
			System.out.println("Archivo guardado: " + src);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createNewFile() {
		openposUnit = OpenposUnit.getDefaultInstance();
		this.fileSelectedName.set("Nuevo archivo");
		System.out.println("Comenzando nuevo fichero");
//		changeWindowTitle("Nuevo archivo");
		initializeTabs(openposUnit);

	}

	private void initializeTabs(OpenposUnit openposUnit) {
		initializeMainTab();
		initializeSeriesTable(openposUnit);
		initializeCompanyData(openposUnit);
		initializeSiteData(openposUnit);

		initializePosTerminalsTab(openposUnit);

//		listIncidences = new IncidenceDao();
		listIncidences = new IncidenceDao();
		listIncidences.setItems(openposUnit.getIncidencesList());
		initializeIncidencesTable(listIncidences);

		initializeIncidencesInitForm();

		listEmployees = new EmployeeDao();
		listEmployees.setItems(openposUnit.getEmployees());
		initializeEmployeesInitForm(openposUnit);
		if (tabEmployees.isSelected()) {
			initializeEmployeesInitForm(openposUnit);
			initializeEmployeesTable(listEmployees);
		}
		tabEmployees.setOnSelectionChanged(event -> {
			if (tabEmployees.isSelected()) {
				initializeEmployeesInitForm(openposUnit);
				initializeEmployeesTable(listEmployees);
			}
		});

		listTaxInfo = new TaxInfoDao();
		listTaxInfo.setItems(openposUnit.getTaxInfo());
		initializeTaxInfoInitForm(openposUnit);
		initializeTaxTable(listTaxInfo);

		listCardTypes = new CardTypeDao();
		listCardTypes.setItems(openposUnit.getCardTypes());

		initializeCardTypesData(listCardTypes);

	}

	private void initializeMainTab() {
		Platform.runLater(() -> {
			mainTab.setDisable(false);
		});
	}

	private void initializeSeriesTable(OpenposUnit openposUnit2) {
		ObservableList<String> details = FXCollections.observableList(openposUnit2.getSeriesList());
		seriesTable.setItems(details);
		seriesTable.setEditable(true);
		seriesTableColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue()));
		seriesTableColumn.setCellFactory(TextFieldTableCell.forTableColumn());

		seriesTableColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			openposUnit2.getSeriesList().set(data.getTablePosition().getRow(), data.getNewValue());
			System.out.println(openposUnit2.getSeriesList());
			System.out.println(details);

		});
//		addRemoveButtonToTable(seriesTable, nameField, list);
	}

	private void initializeIncidencesInitForm() {
		incidenceDto = new IncidenceDto();
//		System.out.println(this.incidenceDto.getIncidence());
		btnConfirmIncidence.setOnAction(event -> {
			addIncidence();
		});
		btnClearIncidence.setOnAction(event -> {
			clearIncidenceForm();
		});
		texfieldInitialize(incidenceCode, this.incidenceDto.getCode(), 2, false, false, true);
		texfieldInitialize(incidenceName, this.incidenceDto.getName(), 30, false, false, true);
		comboBoxStringInitialize(incidenceType, this.incidenceDto.getType(), true, Incidence.INCIDENCE_TYPE_LIST);
		texfieldInitialize(incidenceAccount, this.incidenceDto.getAccount(), 10, false, false, false);
		texfieldInitialize(incidenceAccountingConcept, this.incidenceDto.getAccountingConcept(), 30, false, false,
				false);
		incidenceModifiable.selectedProperty().bindBidirectional(this.incidenceDto.getModifiable());
	}

	private void clearIncidenceForm() {
		this.incidenceDto = new IncidenceDto();
		initializeIncidencesInitForm();
		incidenceCode.requestFocus();
	}

	Incidence incidence = new Incidence();
	SimpleStringProperty oldIncidenceCode = new SimpleStringProperty();

	private void initializeIncidencesTable(InterfaceDao<Incidence> incidences) {

//		List<Incidence> listArg = Arrays.asList(this.incidence);
		initializeTable(incidencesTable, incidences, this.incidence, oldIncidenceCode, "Code");
		tableColumnTextfieldInitialize(incidencesTable, incidencesTableCodeColumn, "Code", "Code", true,
				oldIncidenceCode, this.incidence, incidences);
		tableColumnTextfieldInitialize(incidencesTable, incidencesTableNameColumn, "Code", "Name", true,
				oldIncidenceCode, this.incidence, incidences);
		String[] listTypeValues = { "H", "+", "-", "D" };
		tableColumnComboBoxStringInitialize(incidencesTable, incidencesTableTypeColumn, "Code", "Type", true,
				oldIncidenceCode, this.incidence, incidences, listTypeValues);
		String[] listUseValues = { "meanOfPayment", "cashMovement" };
		tableColumnComboBoxStringInitialize(incidencesTable, incidencesTableUseColumn, "Code", "Use", true,
				oldIncidenceCode, this.incidence, incidences, listUseValues);
		String[] listModifiableValues = { "yes", "no" };
		tableColumnComboBoxStringInitialize(incidencesTable, incidencesTableModificableColumn, "Code", "Modifiable",
				true, oldIncidenceCode, this.incidence, incidences, listModifiableValues);
		tableColumnTextfieldInitialize(incidencesTable, incidencesTableAccountColumn, "Code", "Account", true,
				oldIncidenceCode, this.incidence, incidences);
		tableColumnTextfieldInitialize(incidencesTable, incidencesTableAccountingConceptColumn, "Code",
				"AccountingConcept", true, oldIncidenceCode, this.incidence, incidences);
//		ObservableList<Incidence> incidencesObsList = FXCollections.observableList(incidences.getItems());
//		incidencesTable.setItems(incidencesObsList);
//		incidencesTable.setEditable(true);
//		incidencesTableCodeColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getCode());
//					}
//				});
//		incidencesTableCodeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//		incidencesTableCodeColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setCode(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//		});
//		incidencesTableNameColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getName());
//					}
//				});
//		incidencesTableNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//		incidencesTableNameColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setName(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//		});
//		incidencesTableTypeColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getType());
//					}
//				});
//		incidencesTableTypeColumn.setCellFactory(ComboBoxTableCell.forTableColumn("H", "+", "-", "D"));
//		incidencesTableTypeColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setType(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//
//		});
//		incidencesTableUseColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getUse());
//					}
//				});
//
//		incidencesTableUseColumn.setCellFactory(ComboBoxTableCell.forTableColumn("meanOfPayment", "cashMovement"));
//		incidencesTableUseColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setUse(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//
//		});
//		incidencesTableModificableColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getModifiable());
//					}
//				});
//		incidencesTableModificableColumn.setCellFactory(ComboBoxTableCell.forTableColumn("yes", "no"));
//		incidencesTableModificableColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setModifiable(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//		});
//
//		incidencesTableAccountColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getAccount());
//					}
//				});
//		incidencesTableAccountColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//		incidencesTableAccountColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setAccount(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//		});
//
//		incidencesTableAccountingConceptColumn
//				.setCellValueFactory(new Callback<CellDataFeatures<Incidence, String>, ObservableValue<String>>() {
//					public ObservableValue<String> call(CellDataFeatures<Incidence, String> incidence) {
//
//						return new SimpleStringProperty(incidence.getValue().getAccountingConcept());
//					}
//				});
//		incidencesTableAccountingConceptColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//		incidencesTableAccountingConceptColumn.setOnEditCommit(data -> {
//			System.out.println("Nuevo dato: " + data.getNewValue());
//			System.out.println("Antiguo dato: " + data.getOldValue());
//			incidence.setAccountingConcept(data.getNewValue());
//			modifyIncidence(oldIncidenceCode, incidence);
//		});

//		incidencesTable.setRowFactory(tv -> {
//			TableRow<Incidence> row = new TableRow<>();
//			row.setOnMouseClicked(event -> {
//				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
//				if (!row.isEmpty()) {
//					// incidence = row.getItem();
//					oldIncidenceCode = row.getItem().getCode();
//					incidence = new Incidence();
//					incidence.setCode(row.getItem().getCode());
//					incidence.setName(row.getItem().getName());
//					incidence.setType(row.getItem().getType());
//					incidence.setUse(row.getItem().getUse());
//					incidence.setModifiable(row.getItem().getModifiable());
//					incidence.setAccount(row.getItem().getAccount());
//					incidence.setAccountingConcept(row.getItem().getAccountingConcept());
//				}
//			});
//			return row;
//		});
//		addRemoveButtonToTableIncidences(incidences);
		addRemoveButtonToTable(incidencesTable, "Code", listIncidences);
	}

	private void addIncidence() {
		try {
			validationIncidenceData();
			incidence = new Incidence();
			incidence.setCode(this.incidenceDto.getCode().get());
			incidence.setName(this.incidenceDto.getName().get());
			incidence.setModifiable(this.incidenceDto.getModifiable().get() ? "yes" : "no");
			incidence.setAccount(this.incidenceDto.getAccount().get());
			incidence.setAccountingConcept(this.incidenceDto.getAccountingConcept().get());
			switch (this.incidenceDto.getType().get()) {
			case "Retirada":
				incidence.setType("-");
				incidence.setUse("cashMovement");
				incidence.setAccountingConcept("R");
				break;
			case "Pago":
				incidence.setType("-");
				incidence.setUse("cashMovement");
				break;
			case "Cobro":
				incidence.setType("+");
				incidence.setUse("cashMovement");
				break;
			case "F. Cobro":
				incidence.setType("H");
				incidence.setUse("meanOfPayment");
				break;
			case "F. Cobro (No totalizable)":
				incidence.setType("D");
				incidence.setUse("meanOfPayment");
				break;
			}
			System.out.println(incidence);
			this.listIncidences.addItem(incidence);
			incidencesTable.refresh();
			initializeIncidencesTable(listIncidences);

			incidencesTable.getSelectionModel().select(incidence);
			incidencesTable.scrollTo(incidence);
			this.incidenceDto.setName("");
			this.incidenceDto.setCode("");
			incidenceCode.requestFocus();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al crear incidencia " + this.incidenceDto.getCode().get();
			showAlert(title, headerMsg, ex.getMessage());
		}
	}

	private void validationIncidenceData() {
		List<SimpleStringProperty> incidencesProperties = Arrays.asList(this.incidenceDto.getCode(),
				this.incidenceDto.getName(), this.incidenceDto.getType());
		System.out.println(this.incidenceDto);
		validateEmptyForm(incidencesProperties);
	}

	private void modifyIncidence(String oldIncidenceCode, Incidence newIncidence) {
		try {
			Incidence oldIncidence = listIncidences.findItem(oldIncidenceCode);
			System.out.println(oldIncidence);
			Incidence incidence = new Incidence();
			incidence.setCode(newIncidence.getCode());
			incidence.setName(newIncidence.getName());
			incidence.setType(newIncidence.getType());
			incidence.setUse(newIncidence.getUse());
			incidence.setModifiable(newIncidence.getModifiable());
			incidence.setAccount(newIncidence.getAccount());
			incidence.setAccountingConcept(newIncidence.getAccountingConcept());
			listIncidences.updateItem(oldIncidenceCode, incidence);
			System.out.println(newIncidence);

		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al modificar incidencia " + oldIncidenceCode;
			showAlert(title, headerMsg, ex.getMessage());
		}
		initializeIncidencesTable(listIncidences);

	}

//	private void addRemoveButtonToTableIncidences(IncidenceDao incidences) {
//		Callback<TableColumn<Incidence, Void>, TableCell<Incidence, Void>> cellFactory = new Callback<TableColumn<Incidence, Void>, TableCell<Incidence, Void>>() {
//			@Override
//			public TableCell<Incidence, Void> call(final TableColumn<Incidence, Void> param) {
//				final TableCell<Incidence, Void> cell = new TableCell<Incidence, Void>() {
//
//					private final Button btn = new Button("X");
//
//					{
//						btn.setOnAction((ActionEvent event) -> {
//							Incidence data = getTableView().getItems().get(getIndex());
//							removeIncidence(data);
//							System.out.println("selectedData: " + data);
//							System.out.println("selectedIndex: " + getIndex());
//						});
//					}
//
//					@Override
//					public void updateItem(Void item, boolean empty) {
//						super.updateItem(item, empty);
//						if (empty) {
//							setGraphic(null);
//						} else {
//							setGraphic(btn);
//						}
//					}
//				};
//				return cell;
//			}
//		};
//		TableColumn<Incidence, Void> incidencesColumnActions = new TableColumn<Incidence, Void>();
//		incidencesColumnActions.setCellFactory(cellFactory);
//		setMaxMinWidthActionsColumn(incidencesColumnActions);
//		if (incidencesTable.getColumns().stream().filter(c -> c.getText().length() == 0).findAny()
//				.orElse(null) == null) {
//			incidencesTable.getColumns().add(incidencesColumnActions);
//		}
//	}

//	private void removeIncidence(Incidence incidence) {
//		listIncidences.removeIncidence(incidence.getCode());
//		incidencesTable.refresh();
//		this.incidencesTable.getSelectionModel().clearSelection();
//	}

	private void initializeEmployeesInitForm(OpenposUnit openposUnit) {
		btnConfirmEmployee.setOnAction(event -> {
			createEmployees(openposUnit);
			System.out.println(openposUnit.getEmployees());
		});
		employeeDto = new EmployeeDto();

		if (openposUnit.getSiteInfo() != null) {
			if (openposUnit.getSiteInfo().getSiteCode() != null) {
				employeeDto = new EmployeeDto(openposUnit.getSiteInfo().getSiteCode());
			}
		}

		texfieldInitialize(employeePrefCode, employeeDto.getCodePrefix(), 3, true, false, true);
		texfieldInitialize(employeeInitNumber, employeeDto.getInitNumber(), 3, true, false, true);
		texfieldInitialize(employeeMaxNumber, employeeDto.getMaxNumber(), 2, true, false, true);
	}

	private void createEmployees(OpenposUnit openposUnit) {
		try {
			validateEmployeeForm();
			NumberFormat nf = new DecimalFormat("000");
			for (int i = 0; i < Integer.valueOf(employeeDto.getMaxNumber().get()); i++) {
				String employeeCode = nf.format(Integer.valueOf(employeeDto.getCodePrefix().get()))
						+ nf.format((Integer.valueOf(employeeDto.getInitNumber().get()) + i));

				if (listEmployees.findItem(employeeCode) == null) {
					String employeeName = "EMPLEADO " + (Integer.valueOf(employeeDto.getInitNumber().get()) + i);
					String employeeVatNumber = "0*";
					String employeeLeftHanded = "no";
					Employee employee = new Employee(employeeCode, employeeName, employeeVatNumber, employeeLeftHanded);
					System.out.println("A�adiendo empleado " + employeeCode);
					listEmployees.addItem(employee);
				}

			}
			openposUnit.setEmployees(listEmployees.getItems());
		} catch (Exception e) {
			String title = "Error";
			String headerMsg = "No se pueden dar de alta empleados";
			String msg = e.getMessage();
			showAlert(title, headerMsg, msg);
		}
		employeesTable.refresh();

	}

	private void validateEmployeeForm() {
		List<SimpleStringProperty> employeesPorperties = Arrays.asList(employeeDto.getCodePrefix(),
				employeeDto.getInitNumber(), employeeDto.getMaxNumber());
		validateEmptyForm(employeesPorperties);
	}

	Employee employee = new Employee("", "", "", "");
	String oldEmployeeId = null;

	private void initializeEmployeesTable(InterfaceDao<Employee> employees) {
		ObservableList<Employee> employeesObsList = FXCollections.observableList(employees.getItems());
		employeesTable.setItems(employeesObsList);
		employeesTable.setEditable(true);
		employeesTableCodeColumn
				.setCellValueFactory(new Callback<CellDataFeatures<Employee, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<Employee, String> employee) {

						return new SimpleStringProperty(employee.getValue().getId());
					}
				});
		employeesTableCodeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		employeesTableCodeColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			employee.setId(data.getNewValue());
			modifyEmployee(oldEmployeeId, employee);
		});

		employeesTableNameColumn
				.setCellValueFactory(new Callback<CellDataFeatures<Employee, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<Employee, String> employee) {

						return new SimpleStringProperty(employee.getValue().getName());
					}
				});
		employeesTableNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		employeesTableNameColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			employee.setName(data.getNewValue());
			modifyEmployee(oldEmployeeId, employee);
		});

		employeesTableVatColumn
				.setCellValueFactory(new Callback<CellDataFeatures<Employee, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<Employee, String> employee) {

						return new SimpleStringProperty(employee.getValue().getVatNumber());
					}
				});
		employeesTableVatColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		employeesTableVatColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			employee.setVatNumber(data.getNewValue());
			modifyEmployee(oldEmployeeId, employee);
		});

		employeesTableLeftHandedColumn
				.setCellValueFactory(new Callback<CellDataFeatures<Employee, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<Employee, String> employee) {

						return new SimpleStringProperty(employee.getValue().getLeftHanded());
					}
				});
		employeesTableLeftHandedColumn.setCellFactory(ComboBoxTableCell.forTableColumn("yes", "no"));
		employeesTableLeftHandedColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			employee.setLeftHanded(data.getNewValue());
			modifyEmployee(oldEmployeeId, employee);
		});
		employeesTable.setRowFactory(tv -> {
			TableRow<Employee> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
				if (!row.isEmpty()) {
					// employee = row.getItem();
					oldEmployeeId = row.getItem().getId();
					employee.setId(row.getItem().getId());
					employee.setName(row.getItem().getName());
					employee.setVatNumber(row.getItem().getVatNumber());
					employee.setLeftHanded(row.getItem().getLeftHanded());
				}
			});
			return row;
		});
		addRemoveButtonToTable(employeesTable, "Id", listEmployees);
//		addRemoveButtonToTableEmployees(employees);
	}

	private void modifyEmployee(String oldEmployeeId, Employee newEmployee) {
		try {
			Employee oldEmployee = listEmployees.findItem(oldEmployeeId);
			System.out.println(oldEmployee);
			Employee employee = new Employee();
			employee.setId(newEmployee.getId());
			employee.setName(newEmployee.getName());
			employee.setVatNumber(newEmployee.getVatNumber());
			employee.setLeftHanded(newEmployee.getLeftHanded());
			listEmployees.updateItem(oldEmployeeId, employee);
			System.out.println(newEmployee);

		} catch (Exception ex) {

			// System.out.println(employees.getEmployees().get(data.getTablePosition().getRow()));
			// System.out.println(employeesObsList.get(data.getTablePosition().getRow()));
			String title = "Error";
			String headerMsg = "Error al modificar empleado " + oldEmployeeId;
			showAlert(title, headerMsg, ex.getMessage());
		}
		initializeEmployeesTable(listEmployees);
	}

	private void checkEmptyValue(String value) throws IllegalArgumentException {
		if (value == null)
			throw new IllegalArgumentException("Campo vacio 1");
		if (value.trim().length() == 0)
			throw new IllegalArgumentException("Campo vacio 2");
	}

	private void checkStringLength(String value, int maxLength) throws IllegalArgumentException {
		if (value == null)
			throw new IllegalArgumentException("Valor largo nulo");
		if (value.length() > maxLength)
			throw new IllegalArgumentException("Valor largo");
	}

	private void checkStringParseInt(String value) throws NumberFormatException {
		if (value.length() > 0) {
			Integer.parseInt(value);
		}
	}

	private void checkStringParseDouble(String value) throws NumberFormatException {
		if (value.length() > 0) {
			Double.parseDouble(value);
		}
	}

	private void texfieldInitialize(TextField textfield, SimpleStringProperty property, int maxLength,
			boolean isInteger, boolean isDouble, boolean isObligatory) {
		Platform.runLater(() -> {
			textfield.setStyle(null);
		});

		textfield.textProperty().bindBidirectional(property);
		textfield.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
//					System.out.println("Cambia textfield");
					if (isObligatory) {
						checkEmptyValue(newValue);
					}
					checkStringLength(newValue, maxLength);
					if (isInteger) {
						checkStringParseInt(newValue);
					}
					if (isDouble) {
						checkStringParseDouble(newValue);
					}
					textfield.setStyle(null);
				} catch (IllegalArgumentException e) {
					String redColor = "#B22222"; // Dark color
					textfield.setStyle("-fx-text-box-border: " + redColor);
					System.out.println(e.getMessage());
					if (oldValue != null) {
						if (oldValue.trim().length() > 0 && newValue.trim().length() > 0) {
							textfield.setText(oldValue);
						} else {
							textfield.textProperty().unbind();
							property.set("");
							textfield.textProperty().bindBidirectional(property);
						}
					}
				} catch (Exception e) {
					String redColor = "#B22222"; // Dark color
					textfield.setStyle("-fx-text-box-border: " + redColor);
					e.printStackTrace();
					System.out.println(e.getMessage());
					if (oldValue != null) {
						if (oldValue.trim().length() > 0 && newValue.trim().length() > 0) {
							textfield.setText(oldValue);
						} else {
							textfield.textProperty().unbind();
							property.set("");
							textfield.textProperty().bindBidirectional(property);
						}
					}
				}
			}
		});
	}

	private void comboBoxStringInitialize(ComboBox<String> comboBox, SimpleStringProperty property,
			boolean isObligatory, String... items) {
		Platform.runLater(() -> {
			comboBox.setStyle(null);
		});
		comboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				comboBox.setStyle(null);

			}
		});
		comboBox.getItems().clear();
		comboBox.getItems().addAll(items);
		comboBox.valueProperty().bindBidirectional(property);
		comboBox.valueProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				try {
					if (isObligatory) {
						checkEmptyValue(newValue);
					}

					comboBox.setStyle(null);
				} catch (IllegalArgumentException e) {
					String redColor = "#B22222"; // Dark color
					comboBox.setStyle("-fx-border-color: " + redColor
							+ "; -fx-border-width: 1;-fx-font-size: 10px;-fx-border-radius:3px;");
				}
			}
		});
	}

	private void comboBoxIntegerInitialize(ComboBox<Integer> comboBox, ObjectProperty<Integer> property,
			boolean isObligatory, Integer... items) {
		comboBox.getItems().clear();
		comboBox.getItems().addAll(items);
		comboBox.valueProperty().bindBidirectional(property);
		comboBox.focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				comboBox.setStyle(null);

			}
		});
		comboBox.valueProperty().addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
				try {
					if (isObligatory) {
						checkEmptyValue(newValue + "");
					}

					comboBox.setStyle(null);
				} catch (IllegalArgumentException e) {
					String redColor = "#B22222"; // Dark color
					comboBox.setStyle("-fx-border-color: " + redColor
							+ "; -fx-border-width: 1;-fx-font-size: 10px;-fx-border-radius:3px;");
				}
			}
		});
	}

	private void initializeCompanyData(OpenposUnit openposUnit) {

		if (openposUnit.getCompanyInfo() != null) {
			companyDto = new CompanyInfoDto(openposUnit.getCompanyInfo());
		} else {
			companyDto = new CompanyInfoDto();
		}
//		System.out.println(companyDto.getCompanyCode().get());
		texfieldInitialize(companyCode, companyDto.getCompanyCode(), 3, true, false, true);
		texfieldInitialize(companyShortName, companyDto.getShortName(), 20, false, false, false);
		texfieldInitialize(companyLongName, companyDto.getLongName(), 40, false, false, true);
		texfieldInitialize(addressTypeCompany, companyDto.getAddressType(), 2, false, false, false);
		texfieldInitialize(addressCompany, companyDto.getAddress(), 40, false, false, false);
		texfieldInitialize(addressNumberCompany, companyDto.getAddressNumber(), 15, false, false, false);
		texfieldInitialize(cityCompany, companyDto.getCity(), 25, false, false, false);
		texfieldInitialize(postCodeCompany, companyDto.getPostCode(), 5, true, false, false);
		texfieldInitialize(phoneNumberCompany, companyDto.getPhoneNumber(), 20, false, false, false);

		texfieldInitialize(companyMainCurrencyCode, companyDto.getMainCurrencyCode(), 2, false, false, true);
		texfieldInitialize(companyMainCurrencyName, companyDto.getMainCurrencyName(), 10, false, false, true);
		texfieldInitialize(companyMainCurrencyDecimals, companyDto.getMainCurrencyDecimalPositions(), 2, true, false,
				true);

		texfieldInitialize(companySecondaryCurrencyCode, companyDto.getSecondaryCurrencyCode(), 2, false, false, true);
		texfieldInitialize(companySecondaryCurrencyName, companyDto.getSecondaryCurrencyName(), 10, false, false, true);
		texfieldInitialize(companySecondaryCurrencyDecimals, companyDto.getSecondaryCurrencyDecimalPositions(), 2, true,
				false, true);
		texfieldInitialize(companySecondaryCurrencyValue, companyDto.getSecondaryCurrencyValue(), 1, true, false, true);

		btnConfirmCompanyData.setOnAction(event -> {
			modifyPrincipalCompanyData(openposUnit);
		});

		btnConfirmCurrencyCompanyData.setOnAction(event -> {
			modifyCurrencyCompanyData(openposUnit);
		});

	}

	private void modifyCurrencyCompanyData(OpenposUnit openposUnit) {
		try {
			validateCurrencyCompanyData();
			openposUnit.getCompanyInfo().getCurrencyInfo().getMainCurrency()
					.setCode(companyDto.getMainCurrencyCode().get());
			openposUnit.getCompanyInfo().getCurrencyInfo().getMainCurrency()
					.setName(companyDto.getMainCurrencyName().get());
			openposUnit.getCompanyInfo().getCurrencyInfo().getMainCurrency()
					.setDecimalPositions(Integer.parseInt(companyDto.getMainCurrencyDecimalPositions().get()));

			openposUnit.getCompanyInfo().getCurrencyInfo().getSecondaryCurrency()
					.setCode(companyDto.getSecondaryCurrencyCode().get());
			openposUnit.getCompanyInfo().getCurrencyInfo().getSecondaryCurrency()
					.setName(companyDto.getSecondaryCurrencyName().get());
			openposUnit.getCompanyInfo().getCurrencyInfo().getSecondaryCurrency()
					.setDecimalPositions(Integer.parseInt(companyDto.getSecondaryCurrencyDecimalPositions().get()));
			openposUnit.getCompanyInfo().getCurrencyInfo().getSecondaryCurrency()
					.setValue(Integer.parseInt(companyDto.getSecondaryCurrencyValue().get()));
		} catch (Exception e) {
			String title = "ERROR";
			String headerMsg = "Error al a�adir configuraci�n de moneda";
			String msg = e.getMessage();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(openposUnit.getCompanyInfo());

	}

	private void validateCurrencyCompanyData() {
		List<SimpleStringProperty> currencyCompanyPropertiesList = Arrays.asList(companyDto.getMainCurrencyCode(),
				companyDto.getMainCurrencyName(), companyDto.getMainCurrencyDecimalPositions(),
				companyDto.getSecondaryCurrencyCode(), companyDto.getSecondaryCurrencyName(),
				companyDto.getSecondaryCurrencyDecimalPositions(), companyDto.getSecondaryCurrencyValue());
		validateEmptyForm(currencyCompanyPropertiesList);
	}

	private void modifyPrincipalCompanyData(OpenposUnit openposUnit) {
		try {
			validateCompanyData();
			NumberFormat nf = new DecimalFormat("000");
			companyDto.setCompanyCode(nf.format(Integer.parseInt(companyDto.getCompanyCode().get())));
			openposUnit.getCompanyInfo().setCompanyCode(companyDto.getCompanyCode().get());
			openposUnit.getCompanyInfo().setShortName(companyDto.getShortName().get());
			openposUnit.getCompanyInfo().setLongName(companyDto.getLongName().get());
			openposUnit.getCompanyInfo().setAddressType(companyDto.getAddressType().get());
			openposUnit.getCompanyInfo().setAddress(companyDto.getAddress().get());
			openposUnit.getCompanyInfo().setAddressNumber(companyDto.getAddressNumber().get());
			openposUnit.getCompanyInfo().setPostCode(companyDto.getPostCode().get());
			openposUnit.getCompanyInfo().setCity(companyDto.getCity().get());
			openposUnit.getCompanyInfo().setPhoneNumber(companyDto.getPhoneNumber().get());

		} catch (Exception e) {
			String title = "ERROR";
			String headerMsg = "Error al a�adir compa��a";
			String msg = e.getMessage();
			e.printStackTrace();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(openposUnit.getCompanyInfo());
	}

	private void validateCompanyData() {
		List<SimpleStringProperty> companyPropertiesList = Arrays.asList(companyDto.getCompanyCode(),
				companyDto.getLongName());
		validateEmptyForm(companyPropertiesList);
	}

	private void initializeSiteData(OpenposUnit openposUnit) {
		siteDto = new SiteInfoDto();
		if (openposUnit.getSiteInfo() != null) {
			siteDto = new SiteInfoDto(openposUnit.getSiteInfo());
		}

		texfieldInitialize(siteCode, siteDto.getCode(), 3, true, false, true);
		texfieldInitialize(siteVat, siteDto.getVatNumber(), 10, false, false, true);
		texfieldInitialize(siteName, siteDto.getName(), 40, false, false, true);
		texfieldInitialize(siteAddress, siteDto.getAddress(), 50, false, false, false);
		texfieldInitialize(sitePostCode, siteDto.getPostCode(), 5, true, false, false);
		texfieldInitialize(siteCity, siteDto.getCity(), 25, false, false, false);
		texfieldInitialize(siteProvince, siteDto.getProvince(), 25, false, false, false);
		texfieldInitialize(siteGreetingFirstLine, siteDto.getGreetingFirstLine(), 20, false, false, false);
		texfieldInitialize(siteGreetingSecondLine, siteDto.getGreetingSecondLine(), 20, false, false, false);
		texfieldInitialize(siteOfferFamily, siteDto.getOfferFamily(), 2, false, false, true);
		texfieldInitialize(siteServiceFamily, siteDto.getServiceFamily(), 2, false, false, true);
		texfieldInitialize(siteDiscountItem, siteDto.getDiscountItem(), 8, false, false, true);

		comboBoxStringInitialize(siteNumberOfShifts, siteDto.getNumberOfShifts(), true, "1", "2", "3", "4", "5", "6",
				"7", "8", "9");
		siteCommissionist.selectedProperty().bindBidirectional(siteDto.getCommissionist());

		btnConfirmSiteInfoEstablishment.setOnAction(event -> {
			modifySiteInfoEstablishmentData(openposUnit);
		});
		btnConfirmSiteInfoGeneralParameters.setOnAction(event -> {
			modifySiteInfoGeneralParametersData(openposUnit);
		});
		btnConfirmSiteInfoEstablishmentConfig.setOnAction(event -> {
			modifySiteInfoEstablishmentConfigData(openposUnit);
		});
	}

	private void modifySiteInfoEstablishmentConfigData(OpenposUnit openposUnit) {
		try {
			validateSiteInfoEstablishmentConfigForm();
			openposUnit.getSiteInfo().setNumberOfShifts(Integer.valueOf(siteDto.getNumberOfShifts().get()));
			openposUnit.getSiteInfo().setCommissionist(siteDto.getCommissionist().get() ? "yes" : "no");
			openposUnit.getSiteInfo().setOfferFamily(siteDto.getOfferFamily().get());
			openposUnit.getSiteInfo().setServiceFamily(siteDto.getServiceFamily().get());
			openposUnit.getSiteInfo().setDiscountItem(siteDto.getDiscountItem().get());
		} catch (Exception e) {
			String title = "ERROR";
			String headerMsg = "Error al a�adir configuraci�n de establecimiento";
			String msg = e.getMessage();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(openposUnit.getSiteInfo());

	}

	private void validateSiteInfoEstablishmentConfigForm() {
		List<SimpleStringProperty> sitePropertiesList = Arrays.asList(siteDto.getNumberOfShifts(),
				siteDto.getOfferFamily(), siteDto.getServiceFamily(), siteDto.getDiscountItem());
		validateEmptyForm(sitePropertiesList);
	}

	private void modifySiteInfoGeneralParametersData(OpenposUnit openposUnit) {

		try {
			validateSiteInfoGeneralParametersForm();
			openposUnit.getSiteInfo().setNumberOfShifts(Integer.parseInt(siteDto.getNumberOfShifts().get()));
			openposUnit.getSiteInfo().setCommissionist(siteDto.getCommissionist().get() ? "yes" : "no");
			openposUnit.getSiteInfo().setOfferFamily(siteDto.getOfferFamily().get());
			openposUnit.getSiteInfo().setServiceFamily(siteDto.getServiceFamily().get());
			openposUnit.getSiteInfo().setDiscountItem(siteDto.getDiscountItem().get());
		} catch (Exception e) {
			String title = "ERROR";
			String headerMsg = "Error al a�adir par�metros generales";
			String msg = e.getMessage();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(openposUnit.getSiteInfo());

	}

	private void validateSiteInfoGeneralParametersForm() {
		List<SimpleStringProperty> sitePropertiesList = Arrays.asList(siteDto.getGreetingFirstLine(),
				siteDto.getGreetingSecondLine());
		validateEmptyForm(sitePropertiesList);
	}

	private void modifySiteInfoEstablishmentData(OpenposUnit openposUnit) {
		try {
			validateSiteInfoEstablishmentForm();
			NumberFormat nf = new DecimalFormat("000");
			siteDto.getCode().set(nf.format(Integer.parseInt(siteDto.getCode().get())));
			openposUnit.getSiteInfo().setSiteCode(siteDto.getCode().get());
			openposUnit.getSiteInfo().setVatNumber(siteDto.getVatNumber().get());
			openposUnit.getSiteInfo().setSiteName(siteDto.getName().get());
			openposUnit.getSiteInfo().setAddress(siteDto.getAddress().get());
			openposUnit.getSiteInfo().setPostCode(siteDto.getPostCode().get());
			openposUnit.getSiteInfo().setCity(siteDto.getCity().get());
			openposUnit.getSiteInfo().setProvince(siteDto.getProvince().get());
		} catch (Exception e) {
			String title = "ERROR";
			String headerMsg = "Error al a�adir compa��a";
			String msg = e.getMessage();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(openposUnit.getSiteInfo());

	}

	private void validateSiteInfoEstablishmentForm() {
		List<SimpleStringProperty> sitePropertiesList = Arrays.asList(siteDto.getCode(), siteDto.getVatNumber(),
				siteDto.getName());
		validateEmptyForm(sitePropertiesList);
	}

	SimpleStringProperty obsTaxInfoId;
	SimpleStringProperty obsTaxInfoName;
	SimpleStringProperty obsTaxInfoValue;

	private void initializeTaxInfoInitForm(OpenposUnit openposUnit) {
		btnConfirmTaxInfo.setOnAction(event -> {
			if (taxInfoType.getSelectionModel().getSelectedItem().equals("Manual")) {
				addTaxInfoManual();
			} else {
				addTaxInfoAutomatic();
			}
		});
//		btnClearTaxInfo.setOnAction(event -> {
//			clearTxInfoForm();
//
//		});

		taxInfoType.getItems().clear();
		taxInfoType.getItems().addAll("Manual", "Automatica");
		taxInfoType.getSelectionModel().clearAndSelect(1);
		if (openposUnit.getTaxInfo().isEmpty()) {
			taxInfoIVA.setSelected(true);
		}
		showTaxInfoFormContainers(false);
		taxInfoType.setOnAction(event -> {
			if (taxInfoType.getValue() != null) {
				boolean value = taxInfoType.getValue().equals("Manual");
				showTaxInfoFormContainers(value);
			}
		});

		obsTaxInfoId = new SimpleStringProperty("");
		texfieldInitialize(taxInfoId, obsTaxInfoId, 2, true, false, true);

		obsTaxInfoName = new SimpleStringProperty("");
		texfieldInitialize(taxInfoName, obsTaxInfoName, 30, false, false, true);

		obsTaxInfoValue = new SimpleStringProperty("");
		texfieldInitialize(taxInfoValue, obsTaxInfoValue, 5, false, true, true);
	}

	private void showTaxInfoFormContainers(boolean value) {
		taxInfoIdContainer.setVisible(value);
		taxInfoIdContainer.setManaged(value);
		taxInfoNameContainer.setVisible(value);
		taxInfoNameContainer.setManaged(value);
		taxInfoValueContainer.setVisible(value);
		taxInfoValueContainer.setManaged(value);
		taxInfoChecBoxesContainer.setVisible(!value);
		taxInfoChecBoxesContainer.setManaged(!value);

	}

	TaxInfo tax = new TaxInfo("", "", 0.0);
	String oldTaxInfoId = null;

	private void initializeTaxTable(InterfaceDao<TaxInfo> listTaxInfo) {
		ObservableList<TaxInfo> taxObsList = FXCollections.observableList(listTaxInfo.getItems());
		taxTable.setItems(taxObsList);
		taxTable.setEditable(true);
		taxIdColumn.setCellValueFactory(new Callback<CellDataFeatures<TaxInfo, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TaxInfo, String> tax) {
				return new SimpleStringProperty(tax.getValue().getTaxId());
			}
		});
		taxIdColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		taxIdColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			tax.setTaxId(data.getNewValue());
			modifyTaxInfo(oldTaxInfoId, tax);
		});

		taxNameColumn.setCellValueFactory(new Callback<CellDataFeatures<TaxInfo, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TaxInfo, String> tax) {
				return new SimpleStringProperty(tax.getValue().getName());
			}
		});
		taxNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		taxNameColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			tax.setName(data.getNewValue());
			modifyTaxInfo(oldTaxInfoId, tax);
		});

		taxValueColumn.setCellValueFactory(new Callback<CellDataFeatures<TaxInfo, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<TaxInfo, String> tax) {

				return new SimpleStringProperty(Double.toString(tax.getValue().getValue()));
			}
		});
		taxValueColumn.setCellFactory(TextFieldTableCell.forTableColumn());

		taxValueColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			tax.setValue(Double.valueOf(data.getNewValue()));
			modifyTaxInfo(oldTaxInfoId, tax);
		});

		taxTable.setRowFactory(tv -> {
			TableRow<TaxInfo> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
				if (!row.isEmpty()) {
					// tax = row.getItem();
					oldTaxInfoId = row.getItem().getTaxId();
					tax = new TaxInfo();
					tax.setTaxId(row.getItem().getTaxId());
					tax.setName(row.getItem().getName());
					tax.setValue(row.getItem().getValue());
				}
			});
			return row;
		});
		addRemoveButtonToTable(taxTable, "TaxId", listTaxInfo);
	}

	private void modifyTaxInfo(String oldTaxInfoId, TaxInfo newTax) {
		try {
			TaxInfo oldTaxInfo = listTaxInfo.findItem(oldTaxInfoId);
			System.out.println(oldTaxInfo);
			TaxInfo tax = new TaxInfo();
			tax.setTaxId(newTax.getTaxId());
			tax.setName(newTax.getName());
			tax.setValue(newTax.getValue());
			listTaxInfo.updateItem(oldTaxInfoId, tax);
			System.out.println(newTax);

		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al modificar impuesto " + oldTaxInfoId;
			showAlert(title, headerMsg, ex.getMessage());
		}
		initializeTaxTable(listTaxInfo);
	}

	private <S, T> void setMaxMinWidthActionsColumn(TableColumn<S, T> tableColumn) {
		Double value = 30.0;
		tableColumn.setMaxWidth(value);
		tableColumn.setMinWidth(value);
	}

	private void addTaxInfoManual() {
		try {
			validationTaxInfo();
			setTaxInfoPropertiesFromObserver();
			this.listTaxInfo.addItem(tax);
			initializeTaxTable(listTaxInfo);
			taxTable.refresh();
			// clearCardTypeData();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede dar de alta impuesto " + tax.getTaxId();
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
	}

	private void addTaxInfoAutomatic() {
		try {
			if (taxInfoIVA.isSelected()) {
				addTaxes("IVA");
			}
			if (taxInfoIGIC.isSelected()) {
				addTaxes("IGIC");
			}
			initializeTaxTable(listTaxInfo);
			taxTable.refresh();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede dar de alta impuesto " + tax.getTaxId();
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
	}

	private void addTaxes(String type) {
		for (TaxInfo tax : TaxInfo.GENERIC_TAX_TYPES.get(type)) {
			if (this.listTaxInfo.findItem(tax.getTaxId()) != null) {
				throw new IllegalArgumentException("No se pueden dar de alta impuestos de " + type
						+ " automaticos porque existe impuesto con c�digo " + tax.getTaxId());
			}
		}
		for (TaxInfo tax : TaxInfo.GENERIC_TAX_TYPES.get(type)) {
			this.listTaxInfo.addItem(tax);
		}
	}

	private void setTaxInfoPropertiesFromObserver() {
		tax = new TaxInfo();
		tax.setTaxId(obsTaxInfoId.get());
		tax.setName(obsTaxInfoName.get());
		tax.setValue(Double.parseDouble(obsTaxInfoValue.get()));
	}

	private void validationTaxInfo() {
		List<SimpleStringProperty> taxProperties = Arrays.asList(obsTaxInfoId, obsTaxInfoName, obsTaxInfoValue);
		validateEmptyForm(taxProperties);
	}

	CardType cardType = new CardType("", "", "", 0, 0, "", "no", "", "", "");

	private void initializeCardTypesData(InterfaceDao<CardType> cardTypes) {
		cardTypeAutomatic.getItems().clear();
		cardTypeAutomatic.getItems()
				.addAll(CardType.GENERIC_CARTYPES.keySet().stream().sorted().collect(Collectors.toList()));
		cardTypeAutomatic.setOnAction(event -> {
			// System.out.println(CardType.GENERIC_CARTYPES.get(cardTypeAutomatic.getSelectionModel().getSelectedItem()));
			if (cardTypeAutomatic.getSelectionModel().getSelectedItem() != null) {
				cardType = CardType
						.clone(CardType.GENERIC_CARTYPES.get(cardTypeAutomatic.getSelectionModel().getSelectedItem()));
				loadCardType();
			}
		});
		ObservableList<CardType> cardTypesObsList = FXCollections.observableList(cardTypes.getItems());
		cardTypesTable.setItems(cardTypesObsList);
		// cardTypesTable.setEditable(true);

		cardTypesColumnId
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getId());
					}
				});
		cardTypesColumnName
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getName());
					}
				});
		cardTypesColumnFunction
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getFunction());
					}
				});

		cardTypesColumnUser
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getPosUser() + "");
					}
				});

		cardTypesColumnVehicle
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getPosVehicle() + "");
					}
				});

		cardTypesColumnAuthorisation
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getAuthorisation());
					}
				});

		cardTypesColumnCenterType
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getCenterType());
					}
				});

		cardTypesColumnPointsIncidence
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(cardType.getValue().getPointsIncidence());
					}
				});

		cardTypesColumnBines
				.setCellValueFactory(new Callback<CellDataFeatures<CardType, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<CardType, String> cardType) {
						return new SimpleStringProperty(
								cardType.getValue().getCardBins().toString().replace("[", "").replace("]", ""));
					}
				});

		cardTypesTable.setRowFactory(tv -> {
			TableRow<CardType> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
				if (!row.isEmpty()) {
					cardType = row.getItem();
					cardTypeAutomatic.getSelectionModel().clearSelection();
					loadCardType();
				}
			});
			return row;
		});
		loadCardType();
		addRemoveButtonToTable(cardTypesTable, "Id", listCardTypes);
	}

	SimpleStringProperty obsCardTypeId;
	SimpleStringProperty obsCardTypeName;
	SimpleStringProperty obsCardTypeFunction;
	SimpleBooleanProperty obsCardTypeAuthorisation;
	SimpleStringProperty obsCardTypeCenterType;
	SimpleStringProperty obsCardTypeUser;
	SimpleStringProperty obsCardTypeVehicle;
	SimpleStringProperty obsCardTypeManageType;
	SimpleStringProperty obsCardTypePointsIncidence;
	SimpleStringProperty obsCardTypeActivateOperative;
	SimpleBooleanProperty obsCardTypeOdometer;
	SimpleBooleanProperty obsCardTypeLicense;
	SimpleStringProperty obsCardTypeBine;

	private String oldCardType = null;

	private void loadCardType() {

		btnClearCardType.setOnAction(event -> {
			clearCardTypeData();
		});
		btnAddBine.setOnAction(event -> {
			addBine();

		});
		if (cardTypesTable.getSelectionModel().getSelectedItem() == null) {
			btnConfirmCardType.setOnAction(event -> {
				addCardTypeData();
			});
		} else {
			btnConfirmCardType.setOnAction(event -> {
				oldCardType = cardTypesTable.getSelectionModel().getSelectedItem().getId();
				modifyCardTypeData();
			});
		}

		obsCardTypeId = new SimpleStringProperty(cardType.getId() == null ? "" : cardType.getId());
		texfieldInitialize(cardTypeId, obsCardTypeId, 10, false, false, true);

		obsCardTypeName = new SimpleStringProperty(cardType.getName());
		texfieldInitialize(cardTypeName, obsCardTypeName, 40, false, false, true);

		obsCardTypeFunction = new SimpleStringProperty("");
		if (cardType.getFunction().length() > 0) {
			obsCardTypeFunction = new SimpleStringProperty(
					CardType.CARDTYPE_FUNCTION[Integer.valueOf(cardType.getFunction())]);
		}

		comboBoxStringInitialize(cardTypeFunction, obsCardTypeFunction, true, CardType.CARDTYPE_FUNCTION);

		obsCardTypeAuthorisation = new SimpleBooleanProperty(cardType.getAuthorisation().equals("yes") ? true : false);
		cardTypeAuthorisation.selectedProperty().bindBidirectional(obsCardTypeAuthorisation);

		obsCardTypeCenterType = new SimpleStringProperty(cardType.getCenterType());
		texfieldInitialize(cardTypeCenterType, obsCardTypeCenterType, 2, false, false, true);

		obsCardTypeManageType = new SimpleStringProperty(
				cardType.getManageType() == null ? "" : cardType.getManageType());
		texfieldInitialize(cardTypeManageType, obsCardTypeManageType, 4, false, false, false);

		obsCardTypeUser = new SimpleStringProperty(cardType.getPosUser() + "");
		texfieldInitialize(cardTypeUser, obsCardTypeUser, 4, true, false, true);

		obsCardTypeVehicle = new SimpleStringProperty(cardType.getPosVehicle() + "");
		texfieldInitialize(cardTypeVehicle, obsCardTypeVehicle, 4, true, false, true);

		obsCardTypeActivateOperative = new SimpleStringProperty(
				cardType.getActivateOperative() == null ? "" : cardType.getActivateOperative());
		texfieldInitialize(cardTypeActivateOperative, obsCardTypeActivateOperative, 20, false, false, false);

		obsCardTypePointsIncidence = new SimpleStringProperty(
				cardType.getPointsIncidence() == null ? "" : cardType.getPointsIncidence());
		texfieldInitialize(cardTypeIncidencePoint, obsCardTypePointsIncidence, 2, false, false, false);

		obsCardTypeOdometer = new SimpleBooleanProperty(
				cardType.getDataListItem(CardTypeDataList.TYPE_ODOMETER) != null ? true : false);
		cardTypeOdometer.selectedProperty().bindBidirectional(obsCardTypeOdometer);

		obsCardTypeLicense = new SimpleBooleanProperty(
				cardType.getDataListItem(CardTypeDataList.TYPE_LICENSE) != null ? true : false);
		cardTypeLicense.selectedProperty().bindBidirectional(obsCardTypeLicense);

		obsCardTypeBine = new SimpleStringProperty("");
		texfieldInitialize(cardTypeBine, obsCardTypeBine, 30, true, false, true);

		loadCardTypeBines();
	}

	private void addBine() {
		try {
			validateValidBine();
			cardType.getCardBins().add(obsCardTypeBine.get());
			cardTypeBine.clear();
			cardTypeBine.requestFocus();
			loadCardType();
			cardTypesTable.refresh();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede a�adir bin " + obsCardTypeBine.get();
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
	}

	private void validateValidBine() {

		if (obsCardTypeBine.get().trim().length() == 0) {
			throw new IllegalArgumentException("En necesario indicar un Bin");
		}
		checkExistBine(obsCardTypeBine.get());
	}

	private void checkExistBine(String bine) {
		for (CardType cardType : listCardTypes.getItems()) {
			for (String cardTypeBine : cardType.getCardBins()) {
				if (cardTypeBine.equals(bine)) {
					if (oldCardType != null) {
						if (!oldCardType.equals(cardType.getId())) {
							throw new IllegalArgumentException(
									"El Bin " + bine + " ya existe en tipo de tarjeta " + cardType.getId());
						}
					} else {
						throw new IllegalArgumentException(
								"El Bin " + bine + " ya existe en tipo de tarjeta " + cardType.getId());
					}
				}
			}
		}
	}

	private void loadCardTypeBines() {
		ObservableList<String> obsBinesList = FXCollections.observableList(cardType.getCardBins());
		cardTypeBinesTable.setItems(obsBinesList);
		cardTypeBinesTableBinColumn.setCellValueFactory(data -> new SimpleStringProperty(data.getValue()));
		addRemoveButtonToTableCardTypesBines();
	}

	private void addRemoveButtonToTableCardTypesBines() {
		Callback<TableColumn<String, Void>, TableCell<String, Void>> cellFactory = new Callback<TableColumn<String, Void>, TableCell<String, Void>>() {
			@Override
			public TableCell<String, Void> call(final TableColumn<String, Void> param) {
				final TableCell<String, Void> cell = new TableCell<String, Void>() {

					private final Button btn = new Button("X");

					{
						btn.setOnAction((ActionEvent event) -> {
							String data = getTableView().getItems().get(getIndex());
							cardType.getCardBins().remove(data);
							cardTypeBinesTable.refresh();
							cardTypesTable.refresh();
							System.out.println("selectedData: " + data);
							System.out.println("selectedIndex: " + getIndex());
						});
					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};
		TableColumn<String, Void> cardTypesBinesColumnActions = new TableColumn<String, Void>();
		cardTypesBinesColumnActions.setCellFactory(cellFactory);
		setMaxMinWidthActionsColumn(cardTypesBinesColumnActions);
		if (cardTypeBinesTable.getColumns().stream().filter(c -> c.getText().length() == 0).findAny()
				.orElse(null) == null) {
			cardTypeBinesTable.getColumns().add(cardTypesBinesColumnActions);
		}
	}

	private void clearCardTypeData() {
		this.cardTypeAutomatic.getSelectionModel().clearSelection();
		this.cardTypesTable.getSelectionModel().clearSelection();
		this.oldCardType = null;
		cardType = new CardType("", "", "", 0, 0, "", "no", "", "", "");
		loadCardType();
	}

	private void setCardTypePropertiesFromObserver() {
		List<String> bines = cardType.getCardBins();
		List<CardTypeDataList> dataList = cardType.getDataList();
		cardType = new CardType();
		cardType.setId(obsCardTypeId.get());
		cardType.setName(obsCardTypeName.get());
		cardType.setFunction(Arrays.asList(CardType.CARDTYPE_FUNCTION).indexOf(obsCardTypeFunction.get()) + "");
		cardType.setAuthorisation(obsCardTypeAuthorisation.get() ? "yes" : "no");
		cardType.setCenterType(obsCardTypeCenterType.get());
		cardType.setManageType(obsCardTypeManageType.get());
		cardType.setPosUser(Integer.valueOf(obsCardTypeUser.get()));
		cardType.setPosVehicle(Integer.valueOf(obsCardTypeVehicle.get()));
		cardType.setActivateOperative(obsCardTypeActivateOperative.get());
		cardType.setPointsIncidence(obsCardTypePointsIncidence.get());
		cardType.setCardBins(bines);
		cardType.setDataList(dataList);
		if (!obsCardTypeOdometer.get()) {
			cardType.removeDataListItem(CardTypeDataList.TYPE_ODOMETER);
		} else {
			if (cardType.getDataListItem(CardTypeDataList.TYPE_ODOMETER) == null) {
				cardType.getDataList()
						.add(new CardTypeDataList(CardTypeDataList.TYPE_ODOMETER, CardTypeDataList.PROMPT_ODOMETER));
			}
		}
		if (!obsCardTypeLicense.get()) {
			cardType.removeDataListItem(CardTypeDataList.TYPE_LICENSE);
		} else {
			if (cardType.getDataListItem(CardTypeDataList.TYPE_LICENSE) == null) {
				cardType.getDataList()
						.add(new CardTypeDataList(CardTypeDataList.TYPE_LICENSE, CardTypeDataList.PROMPT_LICENSE));
			}
		}
	}

	private void selectTableRow() {
		cardTypesTable.getSelectionModel().select(cardType);
		cardTypesTable.scrollTo(cardType);
		oldCardType = cardTypesTable.getSelectionModel().getSelectedItem().getId();
	}

	private void modifyCardTypeData() {
		try {
			validationCardTypeData();
			setCardTypePropertiesFromObserver();
			for (String bine : cardType.getCardBins()) {
				checkExistBine(bine);
			}
			this.listCardTypes.updateItem(oldCardType, cardType);
			initializeCardTypesData(listCardTypes);
			cardTypesTable.refresh();
			selectTableRow();
			loadCardType();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede modificar tipo de tarjeta " + oldCardType;
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
	}

	private void addCardTypeData() {

		try {
			validationCardTypeData();
			setCardTypePropertiesFromObserver();
			for (String bine : cardType.getCardBins()) {
				checkExistBine(bine);
			}
			this.listCardTypes.addItem(cardType);
			initializeCardTypesData(listCardTypes);
			cardTypesTable.refresh();
			selectTableRow();
			// loadCardType();
			clearCardTypeData();
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede dar de alta tipo de tarjeta " + cardType.getId();
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
	}

	private void validationCardTypeData() {
		List<SimpleStringProperty> cardTypeProperties = Arrays.asList(obsCardTypeId, obsCardTypeFunction,
				obsCardTypeName, obsCardTypeCenterType, obsCardTypeUser, obsCardTypeVehicle);
		validateEmptyForm(cardTypeProperties);
	}

	private void showAlert(String title, String headerMsg, String msg) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner((Stage) mainAnchorPane.getScene().getWindow());
		alert.setTitle(title);
		alert.setHeaderText(headerMsg);
		alert.setContentText(msg);
		alert.show();
	}

	private void showStartAlert() {
		ButtonType btnNewProyect = new ButtonType("Archivo nuevo ...");
		ButtonType btnLoadProyect = new ButtonType("Cargar archivo ...");
		Alert alert = new Alert(AlertType.CONFIRMATION, "Elige una opci�n:", btnNewProyect, btnLoadProyect);
		alert.initOwner(this.stage);

		alert.setTitle("Inicio");
		alert.setHeaderText("Bienvenido a OpenPOS Maker");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == btnNewProyect) {
			System.out.println("Nuevo");
			createNewFile();
		}
		if (result.get() == btnLoadProyect) {
			System.out.println("Abrir fichero");
			openFile();
		}
	}

	PosTerminal posTerminal = new PosTerminal("", "", "");
	String oldPosTerminalCode = null;

	private void initializePosTerminalConfiguratorInitForm(OpenposUnit openposUnit) {
		btnConfirmPosConfiguration.setOnAction(event -> {
			createPosTerminals(openposUnit);
		});

		posTerminalDto = new PosTerminalDto();
		if (openposUnit.getPosTerminals() != null) {
			String initialCode = openposUnit.getPosTerminals().stream().map(pT -> pT.getPosCode()).max((a, b) -> {
				int value = a.compareTo(b);
				if (b.equals("050")) {
					value = 1;
				}
				return value;
			}).orElse(null);
			if (initialCode != null) {
				NumberFormat nf = new DecimalFormat("000");
				initialCode = nf.format((Integer.parseInt(initialCode) + 1));
			} else {
				initialCode = "001";
			}
			posTerminalDto.getInitialCode().set(initialCode);

		}
		texfieldInitialize(posConfigurationInitialCode, posTerminalDto.getInitialCode(), 3, true, false, true);
		texfieldInitialize(posConfigurationMaxNumber, posTerminalDto.getMaxNumber(), 2, true, false, true);
		texfieldInitialize(posConfigurationInitialIP, posTerminalDto.getInitialIP(), 3, true, false, true);
		comboBoxStringInitialize(posConfigurationPrinterProcess, posTerminalDto.getPrinterProcess(), true,
				PosTerminalDto.LIST_PRINTER_PROCESS);
		comboBoxStringInitialize(posConfigurationInvoiceFormat, posTerminalDto.getInvoiceFormat(), true,
				PosTerminalDto.LIST_INVOICE_FORMATS);
		comboBoxStringInitialize(posConfigurationCloseDocFormat, posTerminalDto.getCloseDocFormat(), true,
				PosTerminalDto.LIST_INVOICE_FORMATS);
		posConfigurationControlPumps.selectedProperty().bindBidirectional(posTerminalDto.getControlPumps());
		posConfigurationControlAuth.selectedProperty().bindBidirectional(posTerminalDto.getControlAuth());
		posConfigurationDrawer.selectedProperty().bindBidirectional(posTerminalDto.getDrawer());
		posConfigurationVisor.selectedProperty().bindBidirectional(posTerminalDto.getVisor());
		Platform.runLater(() -> {
			posConfigurationInitialCode.requestFocus();
		});
	}

	private void initializePosTerminalsTab(OpenposUnit openposUnit) {
		if (listPosTerminals == null) {
			listPosTerminals = new PosTerminalDao();
			listPosTerminalGeneralParameters = new PosTerminalGeneralParameterDao();
		}
		if (openposUnit != null) {
			if (openposUnit.getPosTerminals() != null) {
				listPosTerminals.setItems(openposUnit.getPosTerminals());
				if (openposUnit.getAdditionalParameters() != null) {
					for (AdditionalParameter parameter : openposUnit.getAdditionalParameters()) {
						if (parameter.getFile().contains("TPV.INI")) {
							listPosTerminalGeneralParameters
									.setSectionParams(parameter.getSections().get(0).getParams());

						}
					}
				}
//				openposUnit.setPosTerminals(this.listPosTerminals.getPosTerminals());
			}
		}

		initializePosTerminalConfigurationTab();
		initializePosTerminalDocumentsTab();
		initializePosTerminalPeripheralsTab();
		initializePosTerminalParamsTab();

	}

	private void initializePosTerminalGeneralParamsTab() {
		if (this.tabPosTerminalsGeneralParameters.isSelected()) {
			System.out.println("Seleccionada Tab TPV.INI > Bloque General");
			initializePosTerminalsGeneralParametersTable(listPosTerminalGeneralParameters);
			initializePosTerminalsGeneralParametersInitForm();
		}
		this.tabPosTerminalsGeneralParameters.setOnSelectionChanged(event -> {
			if (this.tabPosTerminalsGeneralParameters.isSelected()) {
				System.out.println("Seleccionada Tab TPV.INI > Bloque General");
				initializePosTerminalsGeneralParametersTable(listPosTerminalGeneralParameters);
				initializePosTerminalsGeneralParametersInitForm();
			}
		});
	}

	private void initializePosTerminalAdditionalParametersTab() {
		if (this.tabPosTerminalsAdditionalParameters.isSelected()) {
			System.out.println("Seleccionada Tab TPV.INI > Bloque TPV");
			initializePosTerminalsAdditionalParametersInitForm(openposUnit);
			initializePosTerminalsAdditionalParametersTable(listPosTerminals);
		}
		this.tabPosTerminalsAdditionalParameters.setOnSelectionChanged(event -> {
			if (this.tabPosTerminalsAdditionalParameters.isSelected()) {
				System.out.println("Seleccionada Tab TPV.INI > Bloque TPV");
				initializePosTerminalsAdditionalParametersInitForm(openposUnit);
				initializePosTerminalsAdditionalParametersTable(listPosTerminals);
			}
		});
	}

	private void initializePosTerminalParamsTab() {
		if (this.tabPosTPVINI.isSelected()) {
			initializePosTerminalGeneralParamsTab();
			initializePosTerminalAdditionalParametersTab();
		}
		this.tabPosTPVINI.setOnSelectionChanged(event -> {
			if (this.tabPosTPVINI.isSelected()) {
				initializePosTerminalGeneralParamsTab();
				initializePosTerminalAdditionalParametersTab();
			}
		});
	}

	private void initializePosTerminalPeripheralsTab() {
		if (this.tabPosTerminalsPeripherals.isSelected()) {
			System.out.println("Seleccionada Tab TPV's > Perif�ricos");
			initializePosTerminalsPeripheralsInitForm(openposUnit);
			initializePosTerminalsPeripheralsTable(listPosTerminals);
		}
		this.tabPosTerminalsPeripherals.setOnSelectionChanged(event -> {
			if (this.tabPosTerminalsPeripherals.isSelected()) {
				System.out.println("Seleccionada Tab TPV's > Perif�ricos");
				initializePosTerminalsPeripheralsInitForm(openposUnit);
				initializePosTerminalsPeripheralsTable(listPosTerminals);
			}
		});
	}

	private void initializePosTerminalDocumentsTab() {
		if (this.tabPosTerminalsDocuments.isSelected()) {
			System.out.println("Seleccionada Tab TPV's > Tipos de documentos");
			initializePosTerminalsDocumentsInitForm();
			initializePosTerminalsDocumentsTable(listPosTerminals);
		}
		this.tabPosTerminalsDocuments.setOnSelectionChanged(event -> {
			if (this.tabPosTerminalsDocuments.isSelected()) {
				System.out.println("Seleccionada Tab TPV's > Tipos de documentos");
				initializePosTerminalsDocumentsInitForm();
				initializePosTerminalsDocumentsTable(listPosTerminals);
			}
		});
	}

	private void initializePosTerminalConfigurationTab() {
		if (this.tabPosTerminalsConfiguration.isSelected()) {
			System.out.println("Seleccionada Tab TPV's > Configuraci�n");
			initializePosTerminalConfiguratorInitForm(openposUnit);
			initializePosTerminalsConfiguratorTable(listPosTerminals);
		}
		this.tabPosTerminalsConfiguration.setOnSelectionChanged(event -> {
			if (this.tabPosTerminalsConfiguration.isSelected()) {
				System.out.println("Seleccionada Tab TPV's > Configuraci�n");
				initializePosTerminalConfiguratorInitForm(openposUnit);
				initializePosTerminalsConfiguratorTable(listPosTerminals);
			}
		});
	}

	private void checkPosTerminalCode(int posCode) {
		if (!this.listPosTerminals.getItems().isEmpty()) {
			List<PosTerminal> posTerminals = this.listPosTerminals.getItems();
			for (PosTerminal posTerminal : posTerminals) {
				if (Integer.valueOf(posTerminal.getPosCode()) == posCode) {
					NumberFormat nf = new DecimalFormat("000");
					throw new IllegalArgumentException("Existe TPV con c�digo " + nf.format(posCode));
				}
			}
		}
	}

	private void createPosTerminals(OpenposUnit openposUnit) {
		try {
			NumberFormat nf = new DecimalFormat("000");
			int maxPosNumber = Integer.parseInt(posTerminalDto.getMaxNumber().get());
			int posCodeNumber = -1;
			for (int i = 0; i < maxPosNumber; i++) {
				posCodeNumber = Integer.parseInt(posTerminalDto.getInitialCode().get()) + i;
				checkPosTerminalCode(posCodeNumber);
			}
			for (int i = 0; i < maxPosNumber; i++) {
				posCodeNumber = Integer.parseInt(posTerminalDto.getInitialCode().get()) + i;
				String posCode = nf.format(posCodeNumber);
				String controlDump = posTerminalDto.getControlPumps().get() ? "yes" : "no";
				String controlAuth = posTerminalDto.getControlAuth().get() ? "auto" : "no";
				List<Document> documents = new ArrayList<>();
				String tickSerie = "T" + openposUnit.getCompanyInfo().getCompanyCode();
				String tickForm = "TICKIVAO";
				String factSerie = "FI" + openposUnit.getCompanyInfo().getCompanyCode();
				String factMenuSerie = "FD" + openposUnit.getCompanyInfo().getCompanyCode();
				String factForm = "FACTPVT";
				String albSerie = "AL" + openposUnit.getCompanyInfo().getCompanyCode();
				if (posTerminalDto.getInvoiceFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[1])) {
					factForm = "FACTPVC";
				}
				if (posTerminalDto.getInvoiceFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
					factForm = "FACTPVF";
				}
				List<String> observations = Arrays.asList("******************************",
						"*** GRACIAS POR  SU VISITA ***", "*****     FELIZ VIAJE    *****",
						"******************************");
				documents.add(new Document("0", tickSerie, tickForm, 1, "TIRA", observations, tickSerie, tickForm, 1,
						"TIRA", observations));
				documents.add(new Document("3", tickSerie, tickForm, 1, "TIRA", observations, tickSerie, tickForm, 1,
						"TIRA", observations));
				documents.add(new Document("5", tickSerie, tickForm, 1, "TIRA", observations, tickSerie, tickForm, 1,
						"TIRA", observations));
				documents.add(new Document("20", tickSerie, tickForm, 1, "TIRA", observations, tickSerie, tickForm, 1,
						"TIRA", observations));
				documents.add(new Document("23", tickSerie, tickForm, 1, "TIRA", observations, tickSerie, tickForm, 1,
						"TIRA", observations));
				documents.add(new Document("1", factSerie, factForm, 1, "TIRAFACE", observations, factSerie, factForm,
						1, "TIRAFACE", observations));
				documents.add(new Document("2", factSerie, factForm, 1, "TIRAFACC", observations, factSerie, factForm,
						1, "TIRAFACC", observations));
				documents.add(new Document("6", "INCI", "INCI", 1, "TIRA", observations, "INCI", "INCI", 0, "TIRA",
						observations));
				documents.add(new Document("15", factMenuSerie, "FACMENUD", 1, "", observations, "", "", 0, "",
						observations));
				documents.add(new Document("16", factMenuSerie, "FACMENUD", 1, "", observations, "", "", 0, "",
						observations));
				documents.add(new Document("E", "CAESSE", "DOCU", 1, "", Arrays.asList(""), "", "", 0, "",
						Arrays.asList("")));
				documents.add(
						new Document("4", "", "EVENTO", 0, "", Arrays.asList(""), "", "", 0, "", Arrays.asList("")));
				documents.add(
						new Document("7", "", "MOVAL", 1, "", Arrays.asList(""), "", "", 0, "", Arrays.asList("")));
				documents.add(new Document("ZZ", albSerie, "ALBTPVT", 1, "TIRAALB", Arrays.asList(""), albSerie,
						"ALBTPVT", 1, "TIRAALB", Arrays.asList("")));
				List<Peripheral> peripherals = new ArrayList<>();
				String port = "+" + Integer.toString((Integer.parseInt(posTerminalDto.getInitialIP().get()) + i));
				String portFact = port;
				String typeTicket = "TM5000T";
				String driverTicket = "ticket5";
				String typeFact = "TM5000S";
				String driverFact = "bandeja5";
				String tipoImpCup = "5";
				String impCup = port;
				String typeDisplay = "TM5000V";
				String typeDrawer = "TM5000C";
				String driverDisplay = "visor";
				String driverDrawer = "cajon";
				if (posTerminalDto.getPrinterProcess().get().equals(PosTerminalDto.LIST_PRINTER_PROCESS[1])) {
					port = "OP" + port;
					portFact = port;
					tipoImpCup = "O";
					impCup = port;
					typeTicket = "OPOSTICK";
					driverTicket = "opostick";
					typeFact = "OPOSSLIP";
					driverFact = "oposslip";
					typeDisplay = "OPOSVIS";
					driverDisplay = "oposvis";
					typeDrawer = "OPOSCAJ";
					driverDrawer = "oposcaj";
				}
				if (posTerminalDto.getInvoiceFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
					typeFact = "TM5000T";
					driverFact = "ticket5";
					if (posTerminalDto.getPrinterProcess().get().equals(PosTerminalDto.LIST_PRINTER_PROCESS[1])) {
						typeFact = "OPOSTICK";
						driverFact = "opostick";
					}
				}
				if (posTerminalDto.getInvoiceFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
					typeFact = "LASER";
					driverFact = "winno";
					portFact = "!" + typeFact;
				}
				peripherals.add(new Peripheral("simplifiedInvoices", typeTicket, driverTicket, port));
				peripherals.add(new Peripheral("deliveryNotes", typeTicket, driverTicket, port));
				peripherals.add(new Peripheral("invoices", typeFact, driverFact, portFact));
				peripherals.add(new Peripheral("petrolInvoices", typeFact, driverFact, portFact));
				if (posTerminalDto.getCloseDocFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
					typeFact = "TM5000T";
					driverFact = "ticket5";
					if (posTerminalDto.getPrinterProcess().get().equals(PosTerminalDto.LIST_PRINTER_PROCESS[1])) {
						typeFact = "OPOSTICK";
						driverFact = "opostick";
					}
					Document liqTicketDocument = new Document("27", "", "LIQTICKET", 1, "", new ArrayList<String>(), "",
							"", 0, "", new ArrayList<String>());
					documents.add(liqTicketDocument);
				}
				if (posTerminalDto.getCloseDocFormat().get().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
					typeFact = "LASER";
					driverFact = "winno";
					portFact = "!" + typeFact;
				}
				peripherals.add(new Peripheral("other", typeFact, driverFact, portFact));

				if (posConfigurationDrawer.isSelected()) {
					peripherals.add(new Peripheral("drawer", typeDrawer, driverDrawer, port));
				}
				if (posConfigurationVisor.isSelected()) {
					peripherals.add(new Peripheral("display", typeDisplay, driverDisplay, port));
				}
				List<OpenShiftTimeSlot> openShiftTimeSlots = new ArrayList<>();
				List<PosTerminalAdditionalParameter> additionalParameters = new ArrayList<>();
				for (String valor : PosTerminalAdditionalParameter.LIST_DEFAULT_ADDITIONAL_PARAMETERS.keySet()) {
					additionalParameters.add(new PosTerminalAdditionalParameter(valor,
							PosTerminalAdditionalParameter.LIST_DEFAULT_ADDITIONAL_PARAMETERS.get(valor)));
				}
				additionalParameters.add(new PosTerminalAdditionalParameter("TipoImpCup", tipoImpCup));
				additionalParameters.add(new PosTerminalAdditionalParameter("ImpCup", impCup));
				PosTerminal pos = new PosTerminal(posCode, controlDump, controlAuth, documents, peripherals,
						openShiftTimeSlots, additionalParameters);
				this.listPosTerminals.getItems().add(pos);
			}
			initializePosTerminalConfiguratorInitForm(openposUnit);
		} catch (IllegalArgumentException ex) {
			String title = "ERROR";
			String headerMsg = "Error al crear TPV's";
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}
		System.out.println(this.listPosTerminals.getItems());
		initializePosTerminalsConfiguratorTable(this.listPosTerminals);
	}

	private void initializePosTerminalsConfiguratorTable(InterfaceDao<PosTerminal> posTerminals) {

		ObservableList<PosTerminal> posTerminalsConfiguratorObsList = FXCollections
				.observableList(posTerminals.getItems());
		posTerminalConfiguratorTable.setItems(posTerminalsConfiguratorObsList);
		posTerminalConfiguratorTable.refresh();
		posTerminalConfiguratorTable.setEditable(true);
		posTerminalConfiguratorTableCodeColumn.setEditable(true);
		posTerminalConfiguratorTableCodeColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {

						return new SimpleStringProperty(posTerminal.getValue().getPosCode());
					}
				});
		posTerminalConfiguratorTableCodeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		posTerminalConfiguratorTableCodeColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			posTerminal.setPosCode(data.getNewValue());
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTablePortColumn.setEditable(true);
		posTerminalConfiguratorTablePortColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						Peripheral ticketPeripheral = posTerminal.getValue().getPeripherals().stream()
								.filter(p -> p.getId().equals("simplifiedInvoices")).findAny().orElse(null);
						String port = "Desconocido";
						if (ticketPeripheral != null) {
							port = ticketPeripheral.getPort();
						}
						return new SimpleStringProperty(port);
					}
				});
		posTerminalConfiguratorTablePortColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		posTerminalConfiguratorTablePortColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());
			System.out.println(posTerminal.getPeripherals().size());
			String oldPort = data.getOldValue();
			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				if (peripheral.getPort().equals(oldPort)) {
					peripheral.setPort(data.getNewValue());
				}
			}
			for (PosTerminalAdditionalParameter addParam : posTerminal.getAdditionalParameters()) {
				if (addParam.getValue().equals(oldPort)) {
					addParam.setValue(data.getNewValue());
				}
			}
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTablePrinterProcessColumn.setEditable(true);
		posTerminalConfiguratorTablePrinterProcessColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						Peripheral ticketPeripheral = posTerminal.getValue().getPeripherals().stream()
								.filter(p -> p.getId().equals("simplifiedInvoices")).findAny().orElse(null);
						String printerProcess = "Desconocido";
						if (ticketPeripheral != null) {
							printerProcess = ticketPeripheral.getPort().contains("OP+")
									? PosTerminalDto.LIST_PRINTER_PROCESS[1]
									: PosTerminalDto.LIST_PRINTER_PROCESS[0];
						}
						return new SimpleStringProperty(printerProcess);
					}
				});
		posTerminalConfiguratorTablePrinterProcessColumn
				.setCellFactory(ComboBoxTableCell.forTableColumn(PosTerminalDto.LIST_PRINTER_PROCESS));
		posTerminalConfiguratorTablePrinterProcessColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				String type = peripheral.getType();
				String driver = peripheral.getDriver();
				String port = peripheral.getPort();
				if (data.getNewValue().equals(PosTerminalDto.LIST_PRINTER_PROCESS[0])) {
					System.out.println(data.getNewValue());
					type = type.replace("OPOSTICK", "TM5000T");
					driver = driver.replace("opostick", "ticket5");
					type = type.replace("OPOSSLIP", "TM5000S");
					driver = driver.replace("oposslip", "bandeja5");
					type = type.replace("OPOSCAJ", "TM5000C");
					driver = driver.replace("oposcaj", "cajon");
					type = type.replace("OPOSVIS", "TM5000V");
					driver = driver.replace("oposvis", "visor");
					port = port.replace("OP+", "+");
				}
				if (data.getNewValue().equals(PosTerminalDto.LIST_PRINTER_PROCESS[1])) {
					type = type.replace("TM5000T", "OPOSTICK");
					driver = driver.replace("ticket5", "opostick");
					type = type.replace("TM5000S", "OPOSSLIP");
					driver = driver.replace("bandeja5", "oposslip");
					type = type.replace("TM5000C", "OPOSCAJ");
					driver = driver.replace("cajon", "oposcaj");
					type = type.replace("TM5000V", "OPOSVIS");
					driver = driver.replace("visor", "oposvis");
					port = port.replace("+", "OP+");
				}
				peripheral.setType(type);
				peripheral.setDriver(driver);
				peripheral.setPort(port);
			}
			for (PosTerminalAdditionalParameter addParam : posTerminal.getAdditionalParameters()) {
				String paramName = addParam.getName();
				String paramValue = addParam.getValue();
				if (data.getNewValue().equals(PosTerminalDto.LIST_PRINTER_PROCESS[0])) {
					if (paramName.equals("TipoImpCup")) {
						paramValue = paramValue.replace("O", "5");
					} else {
						paramValue = paramValue.replace("OP+", "+");
					}
				}
				if (data.getNewValue().equals(PosTerminalDto.LIST_PRINTER_PROCESS[1])) {
					if (paramName.equals("TipoImpCup")) {
						paramValue = paramValue.replace("5", "O");
					} else {
						paramValue = paramValue.replace("+", "OP+");
					}
				}
				addParam.setValue(paramValue);
			}
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTableInvoiceFormatColumn.setEditable(true);
		posTerminalConfiguratorTableInvoiceFormatColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						Peripheral invoicePeripheral = posTerminal.getValue().getPeripherals().stream()
								.filter(p -> p.getId().equals("invoices")).findAny().orElse(null);
						String invoiceFormat = "Desconocido";
						if (invoicePeripheral != null) {
							invoiceFormat = (invoicePeripheral.getDriver().contains("oposslip")
									|| invoicePeripheral.getDriver().contains("bandeja")
											? "Bandeja"
											: invoicePeripheral.getDriver().contains("tick") ? "Ticket"
													: invoicePeripheral.getDriver().contains("winno") ? "L�ser"
															: invoicePeripheral.getDriver());
						}
						return new SimpleStringProperty(invoiceFormat);

					}
				});
		posTerminalConfiguratorTableInvoiceFormatColumn
				.setCellFactory(ComboBoxTableCell.forTableColumn(PosTerminalDto.LIST_INVOICE_FORMATS));
		posTerminalConfiguratorTableInvoiceFormatColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			String printerProcessPort = "";
			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				if (peripheral.getId().equals("simplifiedInvoices")) {
					printerProcessPort = peripheral.getPort();
				}
			}
			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				String type = peripheral.getType();
				String driver = peripheral.getDriver();
				String port = peripheral.getPort();
				if (peripheral.getId().equals("invoices") || peripheral.getId().equals("petrolInvoices")) {
					// TICKET
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
						// si JavaPOS
						if (printerProcessPort.startsWith("OP+")) {
							type = "OPOSTICK";
							driver = "opostick";
						}
						// si LpCli
						if (printerProcessPort.startsWith("+")) {
							type = "TM5000T";
							driver = "ticket5";
						}
					}
					// BANDEJA
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[1])) {
						// si JavaPOS
						if (printerProcessPort.startsWith("OP+")) {
							type = "OPOSSLIP";
							driver = "oposslip";
						}
						// si LpCli
						if (printerProcessPort.startsWith("+")) {
							type = "TM5000S";
							driver = "bandeja5";
						}
					}
					port = printerProcessPort;
					// LASER
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
						type = "LASER";
						driver = "winno";
						port = "!LASER";
					}
					peripheral.setType(type);
					peripheral.setDriver(driver);
					peripheral.setPort(port);
				}
			}
			for (Document document : posTerminal.getDocuments()) {
				String form = document.getForm();
				String refundForm = document.getRefundForm();
				if (document.getId().equals("1") || document.getId().equals("2")) {
					// TICKET
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
						form = form.replace("FACTPVC", "FACTPVT");
						form = form.replace("FACTPVF", "FACTPVT");
						refundForm = refundForm.replace("FACTPVC", "FACTPVT");
						refundForm = refundForm.replace("FACTPVF", "FACTPVT");
					}
					// BANDEJA
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[1])) {
						form = form.replace("FACTPVT", "FACTPVC");
						form = form.replace("FACTPVF", "FACTPVC");
						refundForm = refundForm.replace("FACTPVT", "FACTPVC");
						refundForm = refundForm.replace("FACTPVF", "FACTPVC");
					}
					// LASER
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
						form = form.replace("FACTPVC", "FACTPVF");
						form = form.replace("FACTPVT", "FACTPVF");
						refundForm = refundForm.replace("FACTPVC", "FACTPVF");
						refundForm = refundForm.replace("FACTPVT", "FACTPVF");
					}
					document.setForm(form);
					document.setRefundForm(refundForm);
				}
			}
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTableCloseDocFormatColumn.setEditable(true);
		posTerminalConfiguratorTableCloseDocFormatColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						Peripheral otherPeripheral = posTerminal.getValue().getPeripherals().stream()
								.filter(p -> p.getId().equals("other")).findAny().orElse(null);
						String closeDocFormat = "Desconocido";
						if (otherPeripheral != null) {
							closeDocFormat = (otherPeripheral.getDriver().contains("oposslip")
									|| otherPeripheral.getDriver().contains("bandeja")
											? "Bandeja"
											: otherPeripheral.getDriver().contains("tick") ? "Ticket"
													: otherPeripheral.getDriver().contains("winno") ? "L�ser"
															: otherPeripheral.getDriver());
						}
						return new SimpleStringProperty(closeDocFormat);

					}
				});
		posTerminalConfiguratorTableCloseDocFormatColumn
				.setCellFactory(ComboBoxTableCell.forTableColumn(PosTerminalDto.LIST_INVOICE_FORMATS));
		posTerminalConfiguratorTableCloseDocFormatColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			String printerProcessPort = "";
			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				if (peripheral.getId().equals("simplifiedInvoices")) {
					printerProcessPort = peripheral.getPort();
				}
			}
			for (Peripheral peripheral : posTerminal.getPeripherals()) {
				String type = peripheral.getType();
				String driver = peripheral.getDriver();
				String port = peripheral.getPort();
				if (peripheral.getId().equals("other")) {
					// TICKET
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
						// si JavaPOS
						if (printerProcessPort.startsWith("OP+")) {
							type = "OPOSTICK";
							driver = "opostick";
						}
						// si LpCli
						if (printerProcessPort.startsWith("+")) {
							type = "TM5000T";
							driver = "ticket5";
						}
					}
					// BANDEJA
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[1])) {
						// si JavaPOS
						if (printerProcessPort.startsWith("OP+")) {
							type = "OPOSSLIP";
							driver = "oposslip";
						}
						// si LpCli
						if (printerProcessPort.startsWith("+")) {
							type = "TM5000S";
							driver = "bandeja5";
						}
					}
					port = printerProcessPort;
					// LASER
					if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[2])) {
						type = "LASER";
						driver = "winno";
						port = "!LASER";
					}
					peripheral.setType(type);
					peripheral.setDriver(driver);
					peripheral.setPort(port);
				}
			}
			// TICKET
			if (data.getNewValue().equals(PosTerminalDto.LIST_INVOICE_FORMATS[0])) {
				boolean existLiqTicket = false;
				for (Document document : posTerminal.getDocuments()) {
					if (document.getId().equals("27")) {
						existLiqTicket = true;
					}
				}
				if (!existLiqTicket) {
					Document liqTicketDocument = new Document("27", "", "LIQTICKET", 1, "", new ArrayList<String>(), "",
							"", 0, "", new ArrayList<String>());
					posTerminal.getDocuments().add(liqTicketDocument);
				}
			}
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTableControlPumpColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						return new SimpleStringProperty(posTerminal.getValue().getControlPumps());
					}
				});
		posTerminalConfiguratorTableControlPumpColumn.setCellFactory(ComboBoxTableCell.forTableColumn("yes", "no"));
		posTerminalConfiguratorTableControlPumpColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			posTerminal.setControlPumps(data.getNewValue());
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTableControlAuthColumn
				.setCellValueFactory(new Callback<CellDataFeatures<PosTerminal, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<PosTerminal, String> posTerminal) {
						return new SimpleStringProperty(posTerminal.getValue().getControlAuth());
					}
				});
		posTerminalConfiguratorTableControlAuthColumn
				.setCellFactory(ComboBoxTableCell.forTableColumn("auto", "no", "yes"));
		posTerminalConfiguratorTableControlAuthColumn.setOnEditCommit(data -> {
			System.out.println("Nuevo dato: " + data.getNewValue());
			System.out.println("Antiguo dato: " + data.getOldValue());

			posTerminal.setControlAuth(data.getNewValue());
			modifyPosTerminal(oldPosTerminalCode, posTerminal);
		});

		posTerminalConfiguratorTable.setRowFactory(tv -> {
			TableRow<PosTerminal> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
				if (!row.isEmpty()) {
					// posTerminal = row.getItem();
					oldPosTerminalCode = row.getItem().getPosCode();
					posTerminal = row.getItem().clone();
				}
			});
			return row;
		});
		addRemoveButtonToTable(posTerminalConfiguratorTable, "PosCode", posTerminals);
	}

	private void modifyPosTerminal(String oldPosTerminalCode, PosTerminal newPosTerminal) {
		try {
			PosTerminal oldPosTerminal = listPosTerminals.findItem(oldPosTerminalCode);
			System.out.println(oldPosTerminal);
			PosTerminal posTerminal = newPosTerminal.clone();
			listPosTerminals.updateItem(oldPosTerminalCode, posTerminal);
			System.out.println(newPosTerminal);

		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al modificar TPV " + oldPosTerminalCode;
			showAlert(title, headerMsg, ex.getMessage());
		}
		initializePosTerminalsConfiguratorTable(listPosTerminals);

	}

	SimpleStringProperty obsPosTerminalsDocumentsSelector;

	private void initializePosTerminalsDocumentsInitForm() {
		obsPosTerminalsDocumentsSelector = new SimpleStringProperty("");
		posDocumentsTpvSelector.getItems().clear();
		posDocumentsTpvSelector.getItems()
				.addAll(listPosTerminals.getItems().stream().map(pos -> pos.getPosCode()).collect(Collectors.toList()));
		if (!posDocumentsTpvSelector.getItems().isEmpty() && posDocumentsTpvSelector.getItems().size() == 1) {
			Platform.runLater(() -> {
				posDocumentsTpvSelector.getSelectionModel().select(0);
			});
		}
		posDocumentsTpvSelector.valueProperty().bindBidirectional(obsPosTerminalsDocumentsSelector);
		posDocumentsTpvSelector.setOnAction(event -> {
			if (posDocumentsTpvSelector.getValue() != null) {
				if (posDocumentsTpvSelector.getValue().length() > 0) {
					initializePosTerminalsDocumentsTable(listPosTerminals);
				}
			}
		});
	}

	private <E> void tableColumnTextfieldInitialize(TableView<E> tableView, TableColumn<E, String> tableColumn,
			String keyName, String fieldName, boolean editable, SimpleStringProperty oldKeyItem, E item,
			InterfaceDao<E> listItems) {
		tableColumn.setEditable(editable);
		tableColumn.setCellValueFactory(new Callback<CellDataFeatures<E, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<E, String> cellValue) {
				String data = "";
				try {
					E item = cellValue.getValue();
					Method method = item.getClass().getMethod("get" + fieldName);
					if (method.invoke(item) != null) {
						data = method.invoke(item).toString();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return new SimpleStringProperty(data);
			}
		});
		if (editable) {
			tableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
			tableColumn.setOnEditCommit(data -> {
				System.out.println("Nuevo dato: " + data.getNewValue());
				System.out.println("Antiguo dato: " + data.getOldValue());
				try {
					String newValue = data.getNewValue();
					E oldItem = listItems.findItem(oldKeyItem.get());
					System.out.println("Entidad original: " + oldItem);
					Method method = oldItem.getClass().getMethod("clone");
					E cloneItem = (E) method.invoke(oldItem);
					Object[] args = { newValue };
					Statement statement = new Statement(cloneItem, "set" + fieldName, args);
					statement.execute();
					System.out.println("Entidad modificada: " + cloneItem);
					modifyItem(oldKeyItem.get(), cloneItem, listItems);
//					List<E> itemArg = Arrays.asList(item);
					initializeTable(tableView, listItems, item, oldKeyItem, keyName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	private <E> void tableColumnComboBoxIntegerInitialize(TableView<E> tableView, TableColumn<E, String> tableColumn,
			String keyName, String fieldName, boolean editable, SimpleStringProperty oldKeyItem, E item,
			InterfaceDao<E> listItems, String... comboBoxValues) {
		tableColumn.setEditable(editable);
		tableColumn.setCellValueFactory(new Callback<CellDataFeatures<E, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<E, String> cellValue) {
				String data = "";
				try {
					E item = cellValue.getValue();
					Method method = item.getClass().getMethod("get" + fieldName);
					if (method.invoke(item) != null) {
						data = method.invoke(item).toString();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return new SimpleStringProperty(data);
			}
		});
		if (editable) {
			tableColumn.setCellFactory(ComboBoxTableCell.forTableColumn(comboBoxValues));
			tableColumn.setOnEditCommit(data -> {
				System.out.println("Nuevo dato: " + data.getNewValue());
				System.out.println("Antiguo dato: " + data.getOldValue());
				try {
					String newValue = data.getNewValue();
					E oldItem = listItems.findItem(oldKeyItem.get());
					System.out.println("Entidad original: " + oldItem);
					Method method = oldItem.getClass().getMethod("clone");
					E cloneItem = (E) method.invoke(oldItem);
					Object[] args = { Integer.parseInt(newValue) };
					Statement statement = new Statement(cloneItem, "set" + fieldName, args);
					statement.execute();
					System.out.println("Entidad modificada: " + cloneItem);
					modifyItem(oldKeyItem.get(), cloneItem, listItems);
//					List<E> itemArg = Arrays.asList(item);
					initializeTable(tableView, listItems, item, oldKeyItem, keyName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	private <E> void tableColumnComboBoxStringInitialize(TableView<E> tableView, TableColumn<E, String> tableColumn,
			String keyName, String fieldName, boolean editable, SimpleStringProperty oldKeyItem, E item,
			InterfaceDao<E> listItems, String... comboBoxValues) {
		tableColumn.setEditable(editable);
		tableColumn.setCellValueFactory(new Callback<CellDataFeatures<E, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<E, String> cellValue) {
				String data = "";
				try {
					E item = cellValue.getValue();
					Method method = item.getClass().getMethod("get" + fieldName);
					if (method.invoke(item) != null) {
						data = method.invoke(item).toString();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return new SimpleStringProperty(data);
			}
		});
		if (editable) {
			tableColumn.setCellFactory(ComboBoxTableCell.forTableColumn(comboBoxValues));
			tableColumn.setOnEditCommit(data -> {
				System.out.println("Nuevo dato: " + data.getNewValue());
				System.out.println("Antiguo dato: " + data.getOldValue());
				try {
					String newValue = data.getNewValue();
					E oldItem = listItems.findItem(oldKeyItem.get());
					System.out.println("Entidad original: " + oldItem);
					Method method = oldItem.getClass().getMethod("clone");
					E cloneItem = (E) method.invoke(oldItem);
					Object[] args = { newValue };
					Statement statement = new Statement(cloneItem, "set" + fieldName, args);
					statement.execute();
					System.out.println("Entidad modificada: " + cloneItem);
					modifyItem(oldKeyItem.get(), cloneItem, listItems);
//					List<E> itemArg = Arrays.asList(item);
					initializeTable(tableView, listItems, item, oldKeyItem, keyName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	private <E> void modifyItem(String oldKeyItem, E newItem, InterfaceDao<E> listItems) {
		try {
			listItems.updateItem(oldKeyItem, newItem);
		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al modificar elemento " + oldKeyItem;
			showAlert(title, headerMsg, ex.getMessage());
		}
	}

	private <E> void initializeTable(TableView<E> tableView, InterfaceDao<E> listItems, E item,
			SimpleStringProperty oldKeyItem, String keyName) {
		 List<E> itemArg = Arrays.asList(item);;
		ObservableList<E> obsList = FXCollections.observableList(listItems.getItems());
		tableView.setItems(obsList);
		tableView.refresh();
		tableView.setEditable(true);
		tableView.setRowFactory(tv -> {
			TableRow<E> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
				if (!row.isEmpty()) {
					E oldItem = (E) row.getItem();
					try {
						Method method = oldItem.getClass().getMethod("get" + keyName);
						Statement statement = new Statement(oldItem, "get" + keyName, null);
						String keyValue = (String) method.invoke(oldItem);
						System.out.println("keyValue " + keyValue);
						E findItem = itemArg.get(0);
						findItem = listItems.findItem(keyValue);
						statement.execute();
						System.out.println(oldItem);
						oldKeyItem.set((String) method.invoke(oldItem));
						statement = new Statement(findItem, "clone", null);
						statement.execute();
						findItem = (E) statement.getTarget();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			return row;
		});
	}

	Document document = new Document("", "", "", 1, "", new ArrayList<String>(), "", "", 1, "",
			new ArrayList<String>());
	SimpleStringProperty oldPosTerminalDocumentCode = new SimpleStringProperty();

	private void initializePosTerminalsDocumentsTable(InterfaceDao<PosTerminal> posTerminals) {
		posTerminalDocumentsTable.setItems(null);
		if (posDocumentsTpvSelector.getValue().length() > 0) {
			if (documents == null) {
				documents = new DocumentDao();
			}
			documents.setItems(posTerminals.findItem(posDocumentsTpvSelector.getValue()).getDocuments());
			System.out.println(documents.getItems());

//			List<Document> listDocuments = documents.getItems();
			List<Document> itemArg = Arrays.asList(document);
			initializeTable(posTerminalDocumentsTable, documents, document, oldPosTerminalDocumentCode, "Id");
//			posTerminalDocumentsTable.setItems(posTerminalsDocumentsObsList);
//			posTerminalDocumentsTable.refresh();
//			posTerminalDocumentsTable.setEditable(true);
//			posTerminalDocumentsTable.setRowFactory(tv -> {
//				TableRow<Document> row = new TableRow<>();
//				row.setOnMouseClicked(event -> {
//					// if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
//					if (!row.isEmpty()) {
//						oldPosTerminalDocumentCode.set(row.getItem().getId());
//						document = row.getItem().clone();
//					}
//				});
//				return row;
//			});
//			System.out.println("Valor old" + oldPosTerminalDocumentCode);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableIdColumn, "Id", "Id", true,
					oldPosTerminalDocumentCode, document, documents);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableSerieColumn, "Id", "Serie", true,
					oldPosTerminalDocumentCode, document, documents);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableFormColumn, "Id", "Form", true,
					oldPosTerminalDocumentCode, document, documents);
			String[] numCopiesValues = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
			tableColumnComboBoxIntegerInitialize(posTerminalDocumentsTable, posDocumentsTableCopiesColumn, "Id",
					"NumCopies", true, oldPosTerminalDocumentCode, document, documents, numCopiesValues);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableRegColumn, "Id",
					"RegistrationForm", true, oldPosTerminalDocumentCode, document, documents);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableRefundSerieColumn, "Id",
					"RefundSerie", true, oldPosTerminalDocumentCode, document, documents);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableRefundFormColumn, "Id",
					"RefundForm", true, oldPosTerminalDocumentCode, document, documents);
			tableColumnComboBoxIntegerInitialize(posTerminalDocumentsTable, posDocumentsTableRefundCopiesColumn, "Id",
					"RefundNumCopies", true, oldPosTerminalDocumentCode, document, documents, numCopiesValues);
			tableColumnTextfieldInitialize(posTerminalDocumentsTable, posDocumentsTableRefundRegColumn, "Id",
					"RefundRegistrationForm", true, oldPosTerminalDocumentCode, document, documents);
//			posDocumentsTableIdColumn.setEditable(true);
//			posDocumentsTableIdColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getId());
//						}
//					});

//			posDocumentsTableSerieColumn.setEditable(true);
//			posDocumentsTableSerieColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getSerie());
//						}
//					});

//			posDocumentsTableFormColumn.setEditable(true);
//			posDocumentsTableFormColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getForm());
//						}
//					});
//			posDocumentsTableFormColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//			posDocumentsTableFormColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setForm(data.getNewValue());
//				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

//			posDocumentsTableCopiesColumn.setEditable(true);
//			posDocumentsTableCopiesColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getNumCopies() + "");
//						}
//					});
//			posDocumentsTableCopiesColumn
//					.setCellFactory(ComboBoxTableCell.forTableColumn("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
//			posDocumentsTableCopiesColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setNumCopies(Integer.parseInt(data.getNewValue()));
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});
////
//			posDocumentsTableRegColumn.setEditable(true);
//			posDocumentsTableRegColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getRegistrationForm());
//						}
//					});
//			posDocumentsTableRegColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//			posDocumentsTableRegColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setRegistrationForm(data.getNewValue());
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

//			posDocumentsTableRefundSerieColumn.setEditable(true);
//			posDocumentsTableRefundSerieColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getRefundSerie());
//						}
//					});
//			posDocumentsTableRefundSerieColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//			posDocumentsTableRefundSerieColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setRefundSerie(data.getNewValue());
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

//			posDocumentsTableRefundFormColumn.setEditable(true);
//			posDocumentsTableRefundFormColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getRefundForm());
//						}
//					});
//			posDocumentsTableRefundFormColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//			posDocumentsTableRefundFormColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setRefundForm(data.getNewValue());
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

//			posDocumentsTableRefundCopiesColumn.setEditable(true);
//			posDocumentsTableRefundCopiesColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getRefundNumCopies() + "");
//						}
//					});
//			posDocumentsTableRefundCopiesColumn
//					.setCellFactory(ComboBoxTableCell.forTableColumn("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"));
//			posDocumentsTableRefundCopiesColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setRefundNumCopies(Integer.parseInt(data.getNewValue()));
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

//			posDocumentsTableRefundRegColumn.setEditable(true);
//			posDocumentsTableRefundRegColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Document, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Document, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getRefundRegistrationForm());
//						}
//					});
//			posDocumentsTableRefundRegColumn.setCellFactory(TextFieldTableCell.forTableColumn());
//			posDocumentsTableRefundRegColumn.setOnEditCommit(data -> {
//				System.out.println("Nuevo dato: " + data.getNewValue());
//				System.out.println("Antiguo dato: " + data.getOldValue());
//
//				document.setRefundRegistrationForm(data.getNewValue());
////				modifyPosTerminalDocument(oldPosTerminalDocumentCode, document);
//			});

			addRemoveButtonToTable(posTerminalDocumentsTable, "Id", documents);
		}
	}

	private void modifyPosTerminalDocument(String oldPosTerminalDocumentCode, Document newPosTerminalDocument) {
		try {
			Document oldPosTerminalDocument = documents.findItem(oldPosTerminalDocumentCode);
			System.out.println(oldPosTerminalDocument);
			Document posTerminalDocument = newPosTerminalDocument.clone();
			documents.updateItem(oldPosTerminalDocumentCode, posTerminalDocument);
			System.out.println(newPosTerminalDocument);

		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "Error al modificar tipo de documento " + newPosTerminalDocument;
			showAlert(title, headerMsg, ex.getMessage());
		}
		initializePosTerminalsDocumentsTable(listPosTerminals);

	}

	SimpleStringProperty obsPosTerminalsPeripheralsSelector;

	private void initializePosTerminalsPeripheralsInitForm(OpenposUnit openposUnit) {
		obsPosTerminalsPeripheralsSelector = new SimpleStringProperty("");
		posPeripheralsTpvSelector.getItems().clear();
		posPeripheralsTpvSelector.getItems()
				.addAll(listPosTerminals.getItems().stream().map(pos -> pos.getPosCode()).collect(Collectors.toList()));
		if (!posPeripheralsTpvSelector.getItems().isEmpty() && posPeripheralsTpvSelector.getItems().size() == 1) {
			Platform.runLater(() -> {
				posPeripheralsTpvSelector.getSelectionModel().select(0);
			});
		}
		posPeripheralsTpvSelector.valueProperty().bindBidirectional(obsPosTerminalsPeripheralsSelector);
		posPeripheralsTpvSelector.setOnAction(event -> {
			if (posPeripheralsTpvSelector.getValue() != null) {
				if (posPeripheralsTpvSelector.getValue().length() > 0) {
					initializePosTerminalsPeripheralsTable(listPosTerminals);
				}
			}
		});
	}

	Peripheral peripheral = new Peripheral();
	SimpleStringProperty oldPosTerminalPeripheralId = new SimpleStringProperty();

	private void initializePosTerminalsPeripheralsTable(InterfaceDao<PosTerminal> posTerminals) {
		posTerminalPeripheralsTable.setItems(null);
		if (posPeripheralsTpvSelector.getValue().length() > 0) {
			if (peripherals == null) {
				peripherals = new PeripheralDao();

			}
			peripherals.setItems(posTerminals.findItem(posPeripheralsTpvSelector.getValue()).getPeripherals());
			initializeTable(posTerminalPeripheralsTable, peripherals, peripheral, oldPosTerminalPeripheralId, "Id");
			tableColumnTextfieldInitialize(posTerminalPeripheralsTable, posPeripheralsTableIdColumn, "Id", "Id", false,
					oldPosTerminalPeripheralId, peripheral, peripherals);
			tableColumnTextfieldInitialize(posTerminalPeripheralsTable, posPeripheralsTableTypeColumn, "Id", "Type",
					true, oldPosTerminalPeripheralId, peripheral, peripherals);
			tableColumnTextfieldInitialize(posTerminalPeripheralsTable, posPeripheralsTableDriverColumn, "Id", "Driver",
					true, oldPosTerminalPeripheralId, peripheral, peripherals);
			tableColumnTextfieldInitialize(posTerminalPeripheralsTable, posPeripheralsTablePortColumn, "Id", "Port",
					true, oldPosTerminalPeripheralId, peripheral, peripherals);
//			ObservableList<Peripheral> posTerminalsPeripheralsObsList = FXCollections
//					.observableList(peripherals.getItems());
//			posTerminalPeripheralsTable.setItems(posTerminalsPeripheralsObsList);
//			posTerminalPeripheralsTable.refresh();
//			posPeripheralsTableIdColumn.setEditable(true);
//			posPeripheralsTableIdColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Peripheral, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Peripheral, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getId());
//						}
//					});

//			posPeripheralsTableTypeColumn.setEditable(true);
//			posPeripheralsTableTypeColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Peripheral, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Peripheral, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getType());
//						}
//					});

//			posPeripheralsTableDriverColumn.setEditable(true);
//			posPeripheralsTableDriverColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Peripheral, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Peripheral, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getDriver());
//						}
//					});

//			posPeripheralsTablePortColumn.setEditable(true);
//			posPeripheralsTablePortColumn
//					.setCellValueFactory(new Callback<CellDataFeatures<Peripheral, String>, ObservableValue<String>>() {
//						public ObservableValue<String> call(CellDataFeatures<Peripheral, String> posTerminal) {
//
//							return new SimpleStringProperty(posTerminal.getValue().getPort());
//						}
//					});
			addRemoveButtonToTable(posTerminalPeripheralsTable, "Id", peripherals);
		}
	}

	SimpleStringProperty obsPosTerminalsAdditionalParametersSelector;

	private void initializePosTerminalsAdditionalParametersInitForm(OpenposUnit openposUnit) {
		obsPosTerminalsAdditionalParametersSelector = new SimpleStringProperty("");
		posAdditionalParametersTpvSelector.getItems().clear();
		posAdditionalParametersTpvSelector.getItems()
				.addAll(listPosTerminals.getItems().stream().map(pos -> pos.getPosCode()).collect(Collectors.toList()));
		if (!posAdditionalParametersTpvSelector.getItems().isEmpty()
				&& posAdditionalParametersTpvSelector.getItems().size() == 1) {
			Platform.runLater(() -> {
				posAdditionalParametersTpvSelector.getSelectionModel().select(0);
			});
		}
		posAdditionalParametersTpvSelector.valueProperty()
				.bindBidirectional(obsPosTerminalsAdditionalParametersSelector);
		posAdditionalParametersTpvSelector.setOnAction(event -> {
			if (posAdditionalParametersTpvSelector.getValue() != null) {
				if (posAdditionalParametersTpvSelector.getValue().length() > 0) {
					initializePosTerminalsAdditionalParametersTable(listPosTerminals);
				}
			}
		});
	}

	private void initializePosTerminalsAdditionalParametersTable(InterfaceDao<PosTerminal> posTerminals) {
		posTerminalAdditionalParametersTable.setItems(null);
		if (posAdditionalParametersTpvSelector.getValue().length() > 0) {
			ObservableList<PosTerminalAdditionalParameter> additionalParametersAdditionalParametersObsList = FXCollections
					.observableList(posTerminals.findItem(posAdditionalParametersTpvSelector.getValue())
							.getAdditionalParameters());
			posTerminalAdditionalParametersTable.setItems(additionalParametersAdditionalParametersObsList);
			posTerminalAdditionalParametersTable.refresh();
			posAdditionalParametersTableNameColumn.setEditable(true);
			posAdditionalParametersTableNameColumn.setCellValueFactory(
					new Callback<CellDataFeatures<PosTerminalAdditionalParameter, String>, ObservableValue<String>>() {
						public ObservableValue<String> call(
								CellDataFeatures<PosTerminalAdditionalParameter, String> additionalParameter) {
							return new SimpleStringProperty(additionalParameter.getValue().getName());
						}
					});

			posAdditionalParametersTableValueColumn.setEditable(true);
			posAdditionalParametersTableValueColumn.setCellValueFactory(
					new Callback<CellDataFeatures<PosTerminalAdditionalParameter, String>, ObservableValue<String>>() {
						public ObservableValue<String> call(
								CellDataFeatures<PosTerminalAdditionalParameter, String> additionalParameter) {
							return new SimpleStringProperty(additionalParameter.getValue().getValue());
						}
					});

		}

	}

	SimpleStringProperty obsPosTerminalsGeneralParameterName;
	SimpleStringProperty obsPosTerminalsGeneralParameterValue;

	private void initializePosTerminalsGeneralParametersInitForm() {
		btnConfirmTPVINIGeneralParam.setOnAction(event -> {
			addTPVINIGeneralParam();
		});
		obsPosTerminalsGeneralParameterName = new SimpleStringProperty("");
		obsPosTerminalsGeneralParameterValue = new SimpleStringProperty("");
		texfieldInitialize(TPVINIGeneralParamName, obsPosTerminalsGeneralParameterName, 80, false, false, true);
		texfieldInitialize(TPVINIGeneralParamValue, obsPosTerminalsGeneralParameterValue, 500, false, false, true);
		Platform.runLater(() -> TPVINIGeneralParamName.requestFocus());
//		TPVINIGeneralParamName.requestFocus();
//		System.out.println(TPVINIGeneralParamName.isFocused());
	}

	private void addTPVINIGeneralParam() {
		try {
			this.validatePosTerminalsGeneralParametersForm();
			SectionParam param = new SectionParam(obsPosTerminalsGeneralParameterName.get(),
					obsPosTerminalsGeneralParameterValue.get());
			this.listPosTerminalGeneralParameters.addSectionParam(param);
			this.initializePosTerminalsGeneralParametersTable(listPosTerminalGeneralParameters);
			System.out.println("** " + listPosTerminalGeneralParameters.getSectionParams());
			this.posTerminalGeneralParametersTable.getSelectionModel().select(param);
			this.posTerminalGeneralParametersTable.scrollTo(param);
			this.initializePosTerminalsGeneralParametersInitForm();
//		} catch (IllegalArgumentException e) {
//			// TODO: handle exception

		} catch (Exception ex) {
			String title = "Error";
			String headerMsg = "No se puede a�adir par�metro general " + obsPosTerminalsGeneralParameterName.get();
			String msg = ex.getMessage();
			showAlert(title, headerMsg, msg);
		}

	}

	private void validatePosTerminalsGeneralParametersForm() {
		List<SimpleStringProperty> list = Arrays.asList(obsPosTerminalsGeneralParameterName,
				obsPosTerminalsGeneralParameterValue);
		validateEmptyForm(list);
	}

	private void validateEmptyForm(List<SimpleStringProperty> propertiesList) {
		for (SimpleStringProperty propertie : propertiesList) {
			if (propertie.get() == null) {
				propertie.set(" ");
				propertie.set("");
			}
			if (propertie.get().trim().length() == 0) {
				propertie.set(" ");
				propertie.set("");
			}

		}
		for (SimpleStringProperty propertie : propertiesList) {
			if (propertie.get().trim().length() == 0) {
				throw new IllegalArgumentException("Campos obligatorios sin rellenar");
			}
		}

	}

	private void initializePosTerminalsGeneralParametersTable(
			PosTerminalGeneralParameterDao listPosTerminalGeneralParameter) {
		posTerminalGeneralParametersTable.setItems(null);

		ObservableList<SectionParam> additionalParametersGeneralParametersObsList = FXCollections
				.observableList(listPosTerminalGeneralParameter.getSectionParams());
		posTerminalGeneralParametersTable.setItems(additionalParametersGeneralParametersObsList);
		posTerminalGeneralParametersTable.refresh();
		posGeneralParametersTableNameColumn.setEditable(true);
		posGeneralParametersTableNameColumn
				.setCellValueFactory(new Callback<CellDataFeatures<SectionParam, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<SectionParam, String> additionalParameter) {
						return new SimpleStringProperty(additionalParameter.getValue().getName());
					}
				});

		posGeneralParametersTableValueColumn.setEditable(true);
		posGeneralParametersTableValueColumn
				.setCellValueFactory(new Callback<CellDataFeatures<SectionParam, String>, ObservableValue<String>>() {
					public ObservableValue<String> call(CellDataFeatures<SectionParam, String> additionalParameter) {
						return new SimpleStringProperty(additionalParameter.getValue().getValue());
					}
				});

	}

	public void setStage(Stage primaryStage) {
		stage = primaryStage;

	}

	private <E> void removeTableItem(String idItem, InterfaceDao<?> list, TableView<E> table) {
		list.removeItem(idItem);
		table.refresh();
		table.getSelectionModel().clearSelection();
		// loadCardType();
	}

	private <E> void addRemoveButtonToTable(TableView<E> table, String nameField, InterfaceDao<?> list) {
		Callback<TableColumn<E, Void>, TableCell<E, Void>> cellFactory = new Callback<TableColumn<E, Void>, TableCell<E, Void>>() {
			@Override
			public TableCell<E, Void> call(final TableColumn<E, Void> param) {
				final TableCell<E, Void> cell = new TableCell<E, Void>() {

					private final Button btn = new Button("X");

					{
						btn.setOnAction((ActionEvent event) -> {
							E data = getTableView().getItems().get(getIndex());
							try {
//								data.getClass().getDeclaredField(nameField.toLowerCase());

								Method method = data.getClass().getDeclaredMethod("get" + nameField);
								System.out.println(method.invoke(data));
//							method.invoke(data);
								removeTableItem(method.invoke(data) + "", list, table);
								System.out.println("selectedData: " + data);
								System.out.println("selectedIndex: " + getIndex());
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});
					}

					@Override
					public void updateItem(Void item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							setGraphic(btn);
						}
					}
				};
				return cell;
			}
		};

		TableColumn<E, Void> columnActions = new TableColumn<E, Void>();
		columnActions.setCellFactory(cellFactory);
		setMaxMinWidthActionsColumn(columnActions);
		if (table.getColumns().stream().filter(c -> c.getText().length() == 0).findAny().orElse(null) == null) {
			table.getColumns().add(columnActions);
		}
	}

}
