package openposMaker.utils;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class JSONSaveFileChooser {
	private static FileChooser fChooser = new FileChooser();
	
	public static String saveFile(Stage stage) {
		String fileJSON = "";
		fChooser.setTitle("Guardar fichero JSON");
		fChooser.getExtensionFilters().add((new ExtensionFilter("Fichero JSON", "*.json")));
		File file = fChooser.showSaveDialog(stage);
		if (file != null) {
			fileJSON = file.getPath();
			System.out.println("Ruta fichero a guardar: " + fileJSON);
		} else {
			throw new IllegalArgumentException("No se ha seleccionado fichero");
		}
		return fileJSON;
	}
	
}
