package openposMaker.utils;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONReader {
	
	static ObjectMapper oMapper = new ObjectMapper();
	
	public static <T> T readJSONFile(File src, Class<T> valueType) throws JsonParseException, JsonMappingException, IOException {
		return (T) oMapper.readValue(src,valueType);
	}
	

}
