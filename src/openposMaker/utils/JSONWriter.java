package openposMaker.utils;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONWriter {

	static ObjectMapper oMapper = new ObjectMapper();

	public static void writeJSONFile(File src, Object value)
			throws JsonParseException, JsonMappingException, IOException {
		oMapper.writeValue(src, value);
	}

}
