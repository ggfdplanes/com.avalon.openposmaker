package openposMaker.utils;

import java.io.File;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class JSONFileChooser {
	private static FileChooser fChooser = new FileChooser();
	
	public static String chooseFile(Stage stage) {
		String fileJSON = "";
		fChooser.setTitle("Seleccionar fichero JSON");
		fChooser.getExtensionFilters().add((new ExtensionFilter("Fichero JSON", "*.json")));
		File file = fChooser.showOpenDialog(stage);
		if (file != null) {
			fileJSON = file.getPath();
		} else {
			throw new IllegalArgumentException("No se ha seleccionado fichero");
		}
		return fileJSON;
	}
	
}
