package openposMaker.model.dao;

import java.util.List;
import java.util.ArrayList;
import openposMaker.model.entities.Employee;;

public class EmployeeDao implements InterfaceDao<Employee>{

	private List<Employee> employees = new ArrayList<>();

	public EmployeeDao() {

	}

	public void addItem(Employee employee) {
		if (findItem(employee.getId()) != null) {
			throw new RuntimeException("Ya existe empleado " + employee.getId());
		}
		this.employees.add(employee);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe empleado " + id + " para eliminar");
		}
		this.employees.removeIf(cType -> cType.getId().equals(id));
		System.out.println(employees);
	}

	public void updateItem(String oldEmployeeId, Employee newEmployee) {
		if (findItem(oldEmployeeId) == null) {
			throw new RuntimeException("No existe empleado " + oldEmployeeId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.employees.size(); i++) {
			Employee employee = this.employees.get(i);
			if (!oldEmployeeId.equals(employee.getId())) {
				if (employee.getId().equals(newEmployee.getId())) {
					throw new RuntimeException("Ya existe empleado con c�digo " + newEmployee.getId());
				}

			} else {
				index = i;
			}
		}
		this.employees.set(index, newEmployee);
	}

	public Employee findItem(String id) {
		return this.employees.stream().filter(ct -> ct.getId().equals(id)).findAny().orElse(null);
	}

	public List<Employee> getItems() {
		return this.employees;
	}

	public void setItems(List<Employee> employees) {
		this.employees = new ArrayList<>(employees);
	}

}
