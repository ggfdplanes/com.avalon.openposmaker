package openposMaker.model.dao;

import java.util.List;
import java.util.ArrayList;
import openposMaker.model.entities.Incidence;;

public class IncidenceDao implements InterfaceDao<Incidence> {

	private List<Incidence> incidences = new ArrayList<>();

	public IncidenceDao() {

	}

	public void addItem(Incidence incidencia) {
		if (findItem(incidencia.getCode()) != null) {
			throw new RuntimeException("Ya existe incidencia " + incidencia.getCode());
		}
		this.incidences.add(incidencia);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe incidencia " + id + " para eliminar");
		}
		this.incidences.removeIf(cType -> cType.getCode().equals(id));
		System.out.println(incidences);
	}

	public void updateItem(String oldIncidenceId, Incidence newIncidence) {
		if (findItem(oldIncidenceId) == null) {
			throw new RuntimeException("No existe incidencia " + oldIncidenceId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.incidences.size(); i++) {
			Incidence incidencia = this.incidences.get(i);
			if (!oldIncidenceId.equals(incidencia.getCode())) {
				if (incidencia.getCode().equals(newIncidence.getCode())) {
					throw new RuntimeException("Ya existe incidencia con c�digo " + newIncidence.getCode());
				}

			} else {
				index = i;
			}
		}
		this.incidences.set(index, newIncidence);
	}

	public Incidence findItem(String id) {
		return this.incidences.stream().filter(ct -> ct.getCode().equals(id)).findAny().orElse(null);
	}

	public List<Incidence> getItems() {
		return this.incidences;
	}

	public void setItems(List<Incidence> incidences) {
		this.incidences = new ArrayList<>(incidences);
	}

}
