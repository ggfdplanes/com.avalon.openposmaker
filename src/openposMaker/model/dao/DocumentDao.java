package openposMaker.model.dao;

import java.util.List;
import java.util.ArrayList;
import openposMaker.model.entities.Document;;

public class DocumentDao implements InterfaceDao<Document> {

	private List<Document> documents = new ArrayList<>();

	public DocumentDao() {

	}

	public void addItem(Document documento) {
		if (findItem(documento.getId()) != null) {
			throw new RuntimeException("Ya existe documento " + documento.getId());
		}
		this.documents.add(documento);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe documento " + id + " para eliminar");
		}
		this.documents.removeIf(cType -> cType.getId().equals(id));
		System.out.println(documents);
	}

	public void updateItem(String oldDocumentId, Document newDocument) {
		if (findItem(oldDocumentId) == null) {
			throw new RuntimeException("No existe documento " + oldDocumentId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.documents.size(); i++) {
			Document documento = this.documents.get(i);
			if (!oldDocumentId.equals(documento.getId())) {
				if (documento.getId().equals(newDocument.getId())) {
					throw new RuntimeException("Ya existe documento con c�digo " + newDocument.getId());
				}

			} else {
				index = i;
			}
		}
		this.documents.set(index, newDocument);
	}

	public Document findItem(String id) {
		return this.documents.stream().filter(ct -> ct.getId().equals(id)).findAny().orElse(null);
	}

	public List<Document> getItems() {
		return this.documents;
	}

	public void setItems(List<Document> documents) {
		this.documents = documents;
	}

}
