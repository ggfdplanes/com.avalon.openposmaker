package openposMaker.model.dao;

import java.util.List;
import java.util.ArrayList;
import openposMaker.model.entities.PosTerminal;;

public class PosTerminalDao implements InterfaceDao<PosTerminal>{

	private List<PosTerminal> posTerminals = new ArrayList<>();

	public PosTerminalDao() {

	}

	public void addItem(PosTerminal tpv) {
		if (findItem(tpv.getPosCode()) != null) {
			throw new RuntimeException("Ya existe TPV " + tpv.getPosCode());
		}
		this.posTerminals.add(tpv);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe TPV " + id + " para eliminar");
		}
		this.posTerminals.removeIf(cType -> cType.getPosCode().equals(id));
		System.out.println(posTerminals);
	}

	public void updateItem(String oldPosTerminalPosCode, PosTerminal newPosTerminal) {
		if (findItem(oldPosTerminalPosCode) == null) {
			throw new RuntimeException("No existe TPV " + oldPosTerminalPosCode + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.posTerminals.size(); i++) {
			PosTerminal TPV = this.posTerminals.get(i);
			if (!oldPosTerminalPosCode.equals(TPV.getPosCode())) {
				if (TPV.getPosCode().equals(newPosTerminal.getPosCode())) {
					throw new RuntimeException("Ya existe TPV con c�digo " + newPosTerminal.getPosCode());
				}

			} else {
				index = i;
			}
		}
		this.posTerminals.set(index, newPosTerminal);
	}

	public PosTerminal findItem(String id) {
		return this.posTerminals.stream().filter(ct -> ct.getPosCode().equals(id)).findAny().orElse(null);
	}

	public List<PosTerminal> getItems() {
		return this.posTerminals;
	}

	public void setItems(List<PosTerminal> posTerminals) {
		this.posTerminals = posTerminals;
	}

}
