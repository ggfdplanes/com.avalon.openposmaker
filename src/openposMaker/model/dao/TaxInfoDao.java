package openposMaker.model.dao;

import java.util.List;
import java.util.ArrayList;
import openposMaker.model.entities.TaxInfo;;

public class TaxInfoDao implements InterfaceDao<TaxInfo>{

	private List<TaxInfo> impuestos = new ArrayList<>();

	public TaxInfoDao() {

	}

	public void addItem(TaxInfo impuesto) {
		if (findItem(impuesto.getTaxId()) != null) {
			throw new RuntimeException("Ya existe impuesto " + impuesto.getTaxId());
		}
		this.impuestos.add(impuesto);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe impuesto " + id + " para eliminar");
		}
		this.impuestos.removeIf(cType -> cType.getTaxId().equals(id));
		System.out.println(impuestos);
	}

	public void updateItem(String oldTaxInfoTaxId, TaxInfo newTaxInfo) {
		if (findItem(oldTaxInfoTaxId) == null) {
			throw new RuntimeException("No existe impuesto " + oldTaxInfoTaxId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.impuestos.size(); i++) {
			TaxInfo impuesto = this.impuestos.get(i);
			if (!oldTaxInfoTaxId.equals(impuesto.getTaxId())) {
				if (impuesto.getTaxId().equals(newTaxInfo.getTaxId())) {
					throw new RuntimeException("Ya existe impuesto con c�digo " + newTaxInfo.getTaxId());
				}

			} else {
				index = i;
			}
		}
		this.impuestos.set(index, newTaxInfo);
	}

	public TaxInfo findItem(String id) {
		return this.impuestos.stream().filter(ct -> ct.getTaxId().equals(id)).findAny().orElse(null);
	}

	public List<TaxInfo> getItems() {
		return this.impuestos;
	}

	public void setItems(List<TaxInfo> impuestos) {
		this.impuestos = new ArrayList<>(impuestos);
	}

}
