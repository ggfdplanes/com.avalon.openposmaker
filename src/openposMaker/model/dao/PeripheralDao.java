package openposMaker.model.dao;

import java.util.ArrayList;
import java.util.List;

import openposMaker.model.entities.Peripheral;;

public class PeripheralDao implements InterfaceDao<Peripheral> {

	private List<Peripheral> peripherals = new ArrayList<>();

	public PeripheralDao() {

	}

	public void addItem(Peripheral periferico) {
		if (findItem(periferico.getId()) != null) {
			throw new RuntimeException("Ya existe perif�rico " + periferico.getId());
		}
		this.peripherals.add(periferico);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe perif�rico " + id + " para eliminar");
		}
		this.peripherals.removeIf(cType -> cType.getId().equals(id));
		System.out.println(peripherals);
	}

	public void updateItem(String oldPeripheralId, Peripheral newPeripheral) {
		if (findItem(oldPeripheralId) == null) {
			throw new RuntimeException("No existe perif�rico " + oldPeripheralId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.peripherals.size(); i++) {
			Peripheral periferico = this.peripherals.get(i);
			if (!oldPeripheralId.equals(periferico.getId())) {
				if (periferico.getId().equals(newPeripheral.getId())) {
					throw new RuntimeException("Ya existe perif�rico con c�digo " + newPeripheral.getId());
				}

			} else {
				index = i;
			}
		}
		this.peripherals.set(index, newPeripheral);
	}

	public Peripheral findItem(String id) {
		return this.peripherals.stream().filter(ct -> ct.getId().equals(id)).findAny().orElse(null);
	}

	public List<Peripheral> getItems() {
		return this.peripherals;
	}

	public void setItems(List<Peripheral> peripherals) {
		this.peripherals = peripherals;
	}

}
