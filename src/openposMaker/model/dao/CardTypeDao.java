package openposMaker.model.dao;

import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collections;

import openposMaker.model.entities.CardType;;

public class CardTypeDao implements InterfaceDao<CardType>{

	private List<CardType> cardTypes = new ArrayList<>();

	public CardTypeDao() {

	}

	public void addItem(CardType cardType) {
		if (findItem(cardType.getId()) != null) {
			throw new RuntimeException("Ya existe tipo de tarjeta " + cardType.getId());
		}
		this.cardTypes.add(cardType);
	}

	public void removeItem(String id) {
		if (findItem(id) == null) {
			throw new RuntimeException("No existe tipo de tarjeta " + id + " para eliminar");
		}
		this.cardTypes.removeIf(cType -> cType.getId().equals(id));
		System.out.println(cardTypes);
	}

	public void updateItem(String oldCardTypeId, CardType newCardType) {
		if (findItem(oldCardTypeId) == null) {
			throw new RuntimeException("No existe tipo de tarjeta " + oldCardTypeId + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.cardTypes.size(); i++) {
			CardType cardType = this.cardTypes.get(i);
			if (!oldCardTypeId.equals(cardType.getId())) {
				if (cardType.getId().equals(newCardType.getId())) {
					throw new RuntimeException("Ya existe otro tipo de tarjeta con c�digo " + newCardType.getId());
				}

			} else {
				index = i;
			}
		}
		this.cardTypes.set(index, newCardType);
	}

	public CardType findItem(String id) {
		return this.cardTypes.stream().filter(ct -> ct.getId().equals(id)).findAny().orElse(null);
	}

	public List<CardType> getItems() {
		return this.cardTypes;
	}

	public void setItems(List<CardType> cardTypes) {
		this.cardTypes = new ArrayList<>(cardTypes);
	}

}
