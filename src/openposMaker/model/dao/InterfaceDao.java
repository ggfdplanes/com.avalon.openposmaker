package openposMaker.model.dao;

import java.util.List;;

public interface InterfaceDao<E> {

	public void addItem(E item);

	public void removeItem(String id);

	public void updateItem(String oldItemId, E newItem);

	public E findItem(String id);

	public List<E> getItems();

	public void setItems(List<E> listItems);

}
