package openposMaker.model.dao;

import java.util.ArrayList;
import java.util.List;

import openposMaker.model.entities.SectionParam;;

public class PosTerminalGeneralParameterDao {

	private List<SectionParam> sectionParams = new ArrayList<>();

	public PosTerminalGeneralParameterDao() {

	}

	public void addSectionParam(SectionParam param) {
		if (findSectionParam(param.getName()) != null) {
			throw new RuntimeException("Ya existe par�metro " + param.getName());
		}
		this.sectionParams.add(param);
	}

	public void removeSectionParam(String id) {
		if (findSectionParam(id) == null) {
			throw new RuntimeException("No existe par�metro " + id + " para eliminar");
		}
		this.sectionParams.removeIf(cType -> cType.getName().equals(id));
		System.out.println(sectionParams);
	}

	public void updateSectionParam(String oldSectionParamName, SectionParam newSectionParam) {
		if (findSectionParam(oldSectionParamName) == null) {
			throw new RuntimeException("No existe par�metro " + oldSectionParamName + " para modificar");
		}
		int index = 0;
		for (int i = 0; i < this.sectionParams.size(); i++) {
			SectionParam param = this.sectionParams.get(i);
			if (!oldSectionParamName.equals(param.getName())) {
				if (param.getName().equals(newSectionParam.getName())) {
					throw new RuntimeException("Ya existe par�metro con c�digo " + newSectionParam.getName());
				}

			} else {
				index = i;
			}
		}
		this.sectionParams.set(index, newSectionParam);
	}

	public SectionParam findSectionParam(String id) {
		return this.sectionParams.stream().filter(ct -> ct.getName().equals(id)).findAny().orElse(null);
	}

	public List<SectionParam> getSectionParams() {
		return this.sectionParams;
	}

	public void setSectionParams(List<SectionParam> sectionParams) {
		this.sectionParams = sectionParams;
	}

}
