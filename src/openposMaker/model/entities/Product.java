package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "productCode", "fuellingModeId" })
public class Product implements Serializable {

	@JsonProperty("productCode")
	private String productCode;
	@JsonProperty("fuellingModeId")
	private String fuellingModeId;
	private final static long serialVersionUID = -9136341704276314381L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Product() {
	}

	/**
	 *
	 * @param productCode
	 * @param fuellingModeId
	 */
	public Product(String productCode, String fuellingModeId) {
		super();
		this.productCode = productCode;
		this.fuellingModeId = fuellingModeId;
	}

	@JsonProperty("productCode")
	public String getProductCode() {
		return productCode;
	}

	@JsonProperty("productCode")
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	@JsonProperty("fuellingModeId")
	public String getFuellingModeId() {
		return fuellingModeId;
	}

	@JsonProperty("fuellingModeId")
	public void setFuellingModeId(String fuellingModeId) {
		this.fuellingModeId = fuellingModeId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("productCode", productCode).append("fuellingModeId", fuellingModeId)
				.toString();
	}

}