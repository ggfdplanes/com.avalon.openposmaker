package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "StorageLocationID", "AvalonCustomerCompany", "openPOSServer", "Server",
		"additionalParameters" })
public class Pos2Bos implements Serializable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("StorageLocationID")
	private Integer storageLocationID;
	@JsonProperty("AvalonCustomerCompany")
	private Integer avalonCustomerCompany;
	@JsonProperty("openPOSServer")
	private Integer openPOSServer;
	@JsonProperty("Server")
	private String server;
	@JsonProperty("additionalParameters")
	private List<Pos2BosAdditionalParameter> additionalParameters = new ArrayList<Pos2BosAdditionalParameter>();
	private final static long serialVersionUID = 8983388906538861973L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Pos2Bos() {
	}

	/**
	 *
	 * @param openPOSServer
	 * @param server
	 * @param avalonCustomerCompany
	 * @param name
	 * @param additionalParameters
	 * @param id
	 * @param storageLocationID
	 */
	public Pos2Bos(String id, String name, Integer storageLocationID, Integer avalonCustomerCompany,
			Integer openPOSServer, String server, List<Pos2BosAdditionalParameter> additionalParameters) {
		super();
		this.id = id;
		this.name = name;
		this.storageLocationID = storageLocationID;
		this.avalonCustomerCompany = avalonCustomerCompany;
		this.openPOSServer = openPOSServer;
		this.server = server;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("StorageLocationID")
	public Integer getStorageLocationID() {
		return storageLocationID;
	}

	@JsonProperty("StorageLocationID")
	public void setStorageLocationID(Integer storageLocationID) {
		this.storageLocationID = storageLocationID;
	}

	@JsonProperty("AvalonCustomerCompany")
	public Integer getAvalonCustomerCompany() {
		return avalonCustomerCompany;
	}

	@JsonProperty("AvalonCustomerCompany")
	public void setAvalonCustomerCompany(Integer avalonCustomerCompany) {
		this.avalonCustomerCompany = avalonCustomerCompany;
	}

	@JsonProperty("openPOSServer")
	public Integer getOpenPOSServer() {
		return openPOSServer;
	}

	@JsonProperty("openPOSServer")
	public void setOpenPOSServer(Integer openPOSServer) {
		this.openPOSServer = openPOSServer;
	}

	@JsonProperty("Server")
	public String getServer() {
		return server;
	}

	@JsonProperty("Server")
	public void setServer(String server) {
		this.server = server;
	}

	@JsonProperty("additionalParameters")
	public List<Pos2BosAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<Pos2BosAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name)
				.append("storageLocationID", storageLocationID).append("avalonCustomerCompany", avalonCustomerCompany)
				.append("openPOSServer", openPOSServer).append("server", server)
				.append("additionalParameters", additionalParameters).toString();
	}

}

//-------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class Pos2BosAdditionalParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<Pos2BosAdditionalParameterParam> params = new ArrayList<Pos2BosAdditionalParameterParam>();
	private final static long serialVersionUID = 3871252231538103333L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Pos2BosAdditionalParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public Pos2BosAdditionalParameter(String section, List<Pos2BosAdditionalParameterParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<Pos2BosAdditionalParameterParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<Pos2BosAdditionalParameterParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

// --------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class Pos2BosAdditionalParameterParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -6775909530310167408L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Pos2BosAdditionalParameterParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public Pos2BosAdditionalParameterParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}