package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "operationModeId", "sellingModeList" })
public class OperationMode implements Serializable {

	@JsonProperty("operationModeId")
	private Integer operationModeId;
	@JsonProperty("sellingModeList")
	private List<Integer> sellingModeList = new ArrayList<Integer>();
	private final static long serialVersionUID = 319056910916254752L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public OperationMode() {
	}

	/**
	 *
	 * @param sellingModeList
	 * @param operationModeId
	 */
	public OperationMode(Integer operationModeId, List<Integer> sellingModeList) {
		super();
		this.operationModeId = operationModeId;
		this.sellingModeList = sellingModeList;
	}

	@JsonProperty("operationModeId")
	public Integer getOperationModeId() {
		return operationModeId;
	}

	@JsonProperty("operationModeId")
	public void setOperationModeId(Integer operationModeId) {
		this.operationModeId = operationModeId;
	}

	@JsonProperty("sellingModeList")
	public List<Integer> getSellingModeList() {
		return sellingModeList;
	}

	@JsonProperty("sellingModeList")
	public void setSellingModeList(List<Integer> sellingModeList) {
		this.sellingModeList = sellingModeList;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("operationModeId", operationModeId)
				.append("sellingModeList", sellingModeList).toString();
	}

}