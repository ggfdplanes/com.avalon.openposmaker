package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"code",
"name",
"decimalPositions"
})
public class MainCurrency implements Serializable
{

@JsonProperty("code")
private String code;
@JsonProperty("name")
private String name;
@JsonProperty("decimalPositions")
private Integer decimalPositions;
private final static long serialVersionUID = -6769582323680678421L;

/**
* No args constructor for use in serialization
*
*/
public MainCurrency() {
}

/**
*
* @param code
* @param name
* @param decimalPositions
*/
public MainCurrency(String code, String name, Integer decimalPositions) {
super();
this.code = code;
this.name = name;
this.decimalPositions = decimalPositions;
}

@JsonProperty("code")
public String getCode() {
return code;
}

@JsonProperty("code")
public void setCode(String code) {
this.code = code;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("decimalPositions")
public Integer getDecimalPositions() {
return decimalPositions;
}

@JsonProperty("decimalPositions")
public void setDecimalPositions(Integer decimalPositions) {
this.decimalPositions = decimalPositions;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("code", code).append("name", name).append("decimalPositions", decimalPositions).toString();
}

}