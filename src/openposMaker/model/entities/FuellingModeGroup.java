package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"products"
})
public class FuellingModeGroup implements Serializable
{

@JsonProperty("id")
private Integer id;
@JsonProperty("products")
private List<Product> products = new ArrayList<Product>();
private final static long serialVersionUID = -1976941601989216769L;

/**
* No args constructor for use in serialization
*
*/
public FuellingModeGroup() {
}

/**
*
* @param id
* @param products
*/
public FuellingModeGroup(Integer id, List<Product> products) {
super();
this.id = id;
this.products = products;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("products")
public List<Product> getProducts() {
return products;
}

@JsonProperty("products")
public void setProducts(List<Product> products) {
this.products = products;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("products", products).toString();
}

}