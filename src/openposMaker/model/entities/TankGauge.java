package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "protocolType", "additionalParameters", "ipAddress", "tcpPort", "tanksConnected" })
public class TankGauge implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("protocolType")
	private Integer protocolType;
	@JsonProperty("additionalParameters")
	private List<TankGaugeAdditionalParameter> additionalParameters = new ArrayList<TankGaugeAdditionalParameter>();
	@JsonProperty("ipAddress")
	private String ipAddress;
	@JsonProperty("tcpPort")
	private Integer tcpPort;
	@JsonProperty("tanksConnected")
	private List<TanksConnected> tanksConnected = new ArrayList<TanksConnected>();
	private final static long serialVersionUID = -6279630269520187542L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public TankGauge() {
	}

	/**
	 *
	 * @param tcpPort
	 * @param ipAddress
	 * @param additionalParameters
	 * @param tanksConnected
	 * @param id
	 * @param protocolType
	 */
	public TankGauge(Integer id, Integer protocolType, List<TankGaugeAdditionalParameter> additionalParameters, String ipAddress,
			Integer tcpPort, List<TanksConnected> tanksConnected) {
		super();
		this.id = id;
		this.protocolType = protocolType;
		this.additionalParameters = additionalParameters;
		this.ipAddress = ipAddress;
		this.tcpPort = tcpPort;
		this.tanksConnected = tanksConnected;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("protocolType")
	public Integer getProtocolType() {
		return protocolType;
	}

	@JsonProperty("protocolType")
	public void setProtocolType(Integer protocolType) {
		this.protocolType = protocolType;
	}

	@JsonProperty("additionalParameters")
	public List<TankGaugeAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<TankGaugeAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("ipAddress")
	public String getIpAddress() {
		return ipAddress;
	}

	@JsonProperty("ipAddress")
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	@JsonProperty("tcpPort")
	public Integer getTcpPort() {
		return tcpPort;
	}

	@JsonProperty("tcpPort")
	public void setTcpPort(Integer tcpPort) {
		this.tcpPort = tcpPort;
	}

	@JsonProperty("tanksConnected")
	public List<TanksConnected> getTanksConnected() {
		return tanksConnected;
	}

	@JsonProperty("tanksConnected")
	public void setTanksConnected(List<TanksConnected> tanksConnected) {
		this.tanksConnected = tanksConnected;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("protocolType", protocolType)
				.append("additionalParameters", additionalParameters).append("ipAddress", ipAddress)
				.append("tcpPort", tcpPort).append("tanksConnected", tanksConnected).toString();
	}

}

//---------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class TankGaugeAdditionalParameter implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = 3377029830769966434L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public TankGaugeAdditionalParameter() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public TankGaugeAdditionalParameter(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}

//-------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "tankId", "tankAddress", "additionalParameters" })
class TanksConnected implements Serializable {

	@JsonProperty("tankId")
	private Integer tankId;
	@JsonProperty("tankAddress")
	private String tankAddress;
	@JsonProperty("additionalParameters")
	private List<TanksConnectedAdditionalParameter> additionalParameters = new ArrayList<TanksConnectedAdditionalParameter>();
	private final static long serialVersionUID = 5210767814342486728L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public TanksConnected() {
	}

	/**
	 *
	 * @param tankAddress
	 * @param tankId
	 * @param additionalParameters
	 */
	public TanksConnected(Integer tankId, String tankAddress,
			List<TanksConnectedAdditionalParameter> additionalParameters) {
		super();
		this.tankId = tankId;
		this.tankAddress = tankAddress;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("tankId")
	public Integer getTankId() {
		return tankId;
	}

	@JsonProperty("tankId")
	public void setTankId(Integer tankId) {
		this.tankId = tankId;
	}

	@JsonProperty("tankAddress")
	public String getTankAddress() {
		return tankAddress;
	}

	@JsonProperty("tankAddress")
	public void setTankAddress(String tankAddress) {
		this.tankAddress = tankAddress;
	}

	@JsonProperty("additionalParameters")
	public List<TanksConnectedAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<TanksConnectedAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("tankId", tankId).append("tankAddress", tankAddress)
				.append("additionalParameters", additionalParameters).toString();
	}

}

//--------------------------



@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class TanksConnectedAdditionalParameter implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -949620775131513646L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public TanksConnectedAdditionalParameter() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public TanksConnectedAdditionalParameter(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}
