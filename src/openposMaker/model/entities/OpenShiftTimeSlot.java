package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shift", "from", "to" })
public class OpenShiftTimeSlot implements Serializable {

	@JsonProperty("shift")
	private Integer shift;
	@JsonProperty("from")
	private String from;
	@JsonProperty("to")
	private String to;
	private final static long serialVersionUID = -5140617494673704181L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public OpenShiftTimeSlot() {
	}

	/**
	 *
	 * @param shift
	 * @param from
	 * @param to
	 */
	public OpenShiftTimeSlot(Integer shift, String from, String to) {
		super();
		this.shift = shift;
		this.from = from;
		this.to = to;
	}

	@JsonProperty("shift")
	public Integer getShift() {
		return shift;
	}

	@JsonProperty("shift")
	public void setShift(Integer shift) {
		this.shift = shift;
	}

	@JsonProperty("from")
	public String getFrom() {
		return from;
	}

	@JsonProperty("from")
	public void setFrom(String from) {
		this.from = from;
	}

	@JsonProperty("to")
	public String getTo() {
		return to;
	}

	@JsonProperty("to")
	public void setTo(String to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("shift", shift).append("from", from).append("to", to).toString();
	}

}