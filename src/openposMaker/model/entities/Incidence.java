package openposMaker.model.entities;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "code", "name", "type", "account", "accountingConcept", "use", "modifiable" })
public class Incidence implements Serializable {

	public static final String[] INCIDENCE_TYPE_LIST = {"Retirada", "Pago", "Cobro", "F. Cobro", "F. Cobro (No totalizable)"};
	@JsonProperty("code")
	private String code;
	@JsonProperty("name")
	private String name;
	@JsonProperty("type")
	private String type;
	@JsonProperty("use")
	private String use;
	@JsonProperty("modifiable")
	private String modifiable;
	@JsonProperty("account")
	private String account;
	@JsonProperty("accountingConcept")
	private String accountingConcept;

	private final static long serialVersionUID = -2250345739847293478L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Incidence() {
	}



	

	/**
	 * @param code
	 * @param name
	 * @param type
	 * @param use
	 * @param modifiable
	 * @param account
	 * @param accountingConcept
	 */
	public Incidence(String code, String name, String type, String use, String modifiable, String account,
			String accountingConcept) {
		super();
		this.code = code;
		this.name = name;
		this.type = type;
		this.use = use;
		this.modifiable = modifiable;
		this.account = account;
		this.accountingConcept = accountingConcept;
	}


	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(String code) {
		this.code = code;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("use")
	public String getUse() {
		return use;
	}

	@JsonProperty("use")
	public void setUse(String use) {
		this.use = use;
	}

	@JsonProperty("account")
	public String getAccount() {
		return account;
	}

	@JsonProperty("account")
	public void setAccount(String account) {
		this.account = account;
	}

	@JsonProperty("accountingConcept")
	public String getAccountingConcept() {
		return accountingConcept;
	}

	@JsonProperty("accountingConcept")
	public void setAccountingConcept(String accountingConcept) {
		this.accountingConcept = accountingConcept;
	}

	@JsonProperty("modifiable")
	public String getModifiable() {
		return modifiable;
	}

	@JsonProperty("modifiable")
	public void setModifiable(String modifiable) {
		this.modifiable = modifiable;
	}
	
	public Incidence clone() {
		Incidence incidence = new Incidence();
		incidence.setCode(this.getCode());
		incidence.setName(this.getName());
		incidence.setType(this.getType());
		incidence.setUse(this.getUse());
		incidence.setModifiable(this.getModifiable());
		incidence.setAccount(this.getAccount());
		incidence.setAccountingConcept(this.getAccountingConcept());
		return incidence;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("code", code).append("name", name).append("type", type)
				.append("use", use).append("account", account).append("accountingConcept", accountingConcept)
				.append("modifiable", modifiable).toString();
	}

}