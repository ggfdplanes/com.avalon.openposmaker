package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "productId", "capacity" })
public class Tank implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("productId")
	private String productId;
	@JsonProperty("capacity")
	private Integer capacity;
	private final static long serialVersionUID = -1595969722479659098L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Tank() {
	}

	/**
	 *
	 * @param productId
	 * @param id
	 * @param capacity
	 */
	public Tank(Integer id, String productId, Integer capacity) {
		super();
		this.id = id;
		this.productId = productId;
		this.capacity = capacity;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("productId")
	public String getProductId() {
		return productId;
	}

	@JsonProperty("productId")
	public void setProductId(String productId) {
		this.productId = productId;
	}

	@JsonProperty("capacity")
	public Integer getCapacity() {
		return capacity;
	}

	@JsonProperty("capacity")
	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("productId", productId).append("capacity", capacity)
				.toString();
	}

}
