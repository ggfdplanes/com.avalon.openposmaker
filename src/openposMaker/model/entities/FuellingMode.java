package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "params" })
public class FuellingMode implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("params")
	private List<FuellingModeParam> params = new ArrayList<FuellingModeParam>();
	private final static long serialVersionUID = 8413783278920644095L;

/**
* No args constructor for use in serialization
*
*/
public FuellingMode() {
}

/**
*
* @param id
* @param params
*/
public FuellingMode(Integer id, List<FuellingModeParam> params) {
super();
this.id = id;
this.params = params;
}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("params")
	public List<FuellingModeParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<FuellingModeParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("params", params).toString();
	}

}


//-------------------------------------------------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })

class FuellingModeParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -6775909530310167408L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public FuellingModeParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public FuellingModeParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}