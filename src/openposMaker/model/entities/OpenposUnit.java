package openposMaker.model.entities;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "creationTimestamp", "companyInfo", "siteInfo", "seriesList", "incidencesList", "employees",
		"cardTypes", "taxInfo", "forecourtInfo", "posTerminals", "outdoorTerminals", "authTerminals", "tankGauges",
		"pricePanels", "pos2bos", "additionalParameters" })

public class OpenposUnit implements Serializable {

	@JsonProperty("creationTimestamp")
	private String creationTimestamp;
	@JsonProperty("companyInfo")
	private CompanyInfo companyInfo;
	@JsonProperty("siteInfo")
	private SiteInfo siteInfo;
	@JsonProperty("seriesList")
	private List<String> seriesList = new ArrayList<String>();
	@JsonProperty("incidencesList")
	private List<Incidence> incidencesList = new ArrayList<>();
	@JsonProperty("employees")
	private List<Employee> employees = new ArrayList<Employee>();
	@JsonProperty("cardTypes")
	private List<CardType> cardTypes = new ArrayList<CardType>();
	@JsonProperty("taxInfo")
	private List<TaxInfo> taxInfo = new ArrayList<TaxInfo>();
	@JsonProperty("forecourtInfo")
	private ForecourtInfo forecourtInfo;
	@JsonProperty("posTerminals")
	private List<PosTerminal> posTerminals = new ArrayList<PosTerminal>();
	@JsonProperty("outdoorTerminals")
	private List<OutdoorTerminal> outdoorTerminals = new ArrayList<OutdoorTerminal>();
	@JsonProperty("authTerminals")
	private List<AuthTerminal> authTerminals = new ArrayList<AuthTerminal>();
	@JsonProperty("tankGauges")
	private List<TankGauge> tankGauges = new ArrayList<TankGauge>();
	@JsonProperty("pricePanels")
	private List<PricePanel> pricePanels = new ArrayList<PricePanel>();
	@JsonProperty("pos2bos")
	private List<Pos2Bos> pos2bos = new ArrayList<Pos2Bos>();
	@JsonProperty("additionalParameters")
	private List<AdditionalParameter> additionalParameters = new ArrayList<AdditionalParameter>();
	private final static long serialVersionUID = -6769582323680678421L;

	public OpenposUnit() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String strDate = formatter.format(date);
		this.creationTimestamp = strDate;
	}

	public static OpenposUnit getDefaultInstance() {
		OpenposUnit openposUnit = new OpenposUnit();
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String strDate = formatter.format(date);

		openposUnit.creationTimestamp = strDate;
		openposUnit.companyInfo = CompanyInfo.getDefaultInstance();
		openposUnit.siteInfo = SiteInfo.getDefaultInstance();
		String companyCode = openposUnit.getCompanyInfo().getCompanyCode();
		openposUnit.seriesList.addAll(Arrays.asList("A" + companyCode, "AA" + companyCode, "AAL" + companyCode,
				"AFI" + companyCode, "AL" + companyCode, "AT" + companyCode, "FD" + companyCode, "FI" + companyCode,
				"FT" + companyCode, "INCI", "T" + companyCode, "ZZZZZZ", "zzzzzz"));
		openposUnit.forecourtInfo = ForecourtInfo.getDefaultInstance();

		try {
			List<String> tpvGeneralDefaultLines = Files
					.readAllLines(new File("src/openposMaker/def/TPV.INI.DEFAULT").toPath());
			List<SectionParam> listTpvIniGeneralParams = new ArrayList<SectionParam>();

			tpvGeneralDefaultLines.stream().map(line -> {
				String newLine = line.replace("@COMPANY_CODE", openposUnit.companyInfo.getCompanyCode());
				String paramName = newLine.substring(0, newLine.indexOf("="));
				String paramValue = newLine.substring(newLine.indexOf("=") + 1);
				SectionParam sP = new SectionParam(paramName, paramValue);
				listTpvIniGeneralParams.add(sP);
				return newLine;
			}).collect(Collectors.toList());
//			System.out.println(listTpvIniGeneralParams);
			Section section = new Section("GENERAL");
			section.setParams(listTpvIniGeneralParams);
			AdditionalParameter aP = new AdditionalParameter("\\APL\\satelite\\2.1\\ppv\\fich\\TPV.INI");
			aP.getSections().add(section);
			openposUnit.additionalParameters.add(aP);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return openposUnit;
	}

	@JsonProperty("creationTimestamp")
	public String getCreationTimestamp() {
		return creationTimestamp;
	}

	@JsonProperty("creationTimestamp")
	public void setCreationTimestamp(String creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	@JsonProperty("companyInfo")
	public CompanyInfo getCompanyInfo() {
		return companyInfo;
	}

	@JsonProperty("companyInfo")
	public void setCompanyInfo(CompanyInfo companyInfo) {
		this.companyInfo = companyInfo;
	}

	@JsonProperty("siteInfo")
	public SiteInfo getSiteInfo() {
		return siteInfo;
	}

	@JsonProperty("siteInfo")
	public void setSiteInfo(SiteInfo siteInfo) {
		this.siteInfo = siteInfo;
	}

	@JsonProperty("seriesList")
	public void setSeriesList(List<String> seriesList) {
		this.seriesList = seriesList;

	}

	@JsonProperty("seriesList")
	public List<String> getSeriesList() {
		return this.seriesList;

	}

	@JsonProperty("incidencesList")
	public List<Incidence> getIncidencesList() {
		return incidencesList;
	}

	@JsonProperty("incidencesList")
	public void setIncidencesList(List<Incidence> incidencesList) {
		this.incidencesList = incidencesList;
	}

	@JsonProperty("employees")
	public List<Employee> getEmployees() {
		return employees;
	}

	@JsonProperty("employees")
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@JsonProperty("cardTypes")
	public List<CardType> getCardTypes() {
		return cardTypes;
	}

	@JsonProperty("cardTypes")
	public void setCardTypes(List<CardType> cardTypes) {
		this.cardTypes = cardTypes;
	}

	@JsonProperty("taxInfo")
	public List<TaxInfo> getTaxInfo() {
		return taxInfo;
	}

	@JsonProperty("taxInfo")
	public void setTaxInfo(List<TaxInfo> taxInfo) {
		this.taxInfo = taxInfo;
	}

	@JsonProperty("forecourtInfo")
	public ForecourtInfo getForecourtInfo() {
		return forecourtInfo;
	}

	@JsonProperty("forecourtInfo")
	public void setForecourtInfo(ForecourtInfo forecourtInfo) {
		this.forecourtInfo = forecourtInfo;
	}

	@JsonProperty("posTerminals")
	public List<PosTerminal> getPosTerminals() {
		return posTerminals;
	}

	@JsonProperty("posTerminals")
	public void setPosTerminals(List<PosTerminal> posTerminals) {
		this.posTerminals = posTerminals;
	}

	@JsonProperty("outdoorTerminals")
	public List<OutdoorTerminal> getOutdoorTerminals() {
		return outdoorTerminals;
	}

	@JsonProperty("outdoorTerminals")
	public void setOutdoorTerminals(List<OutdoorTerminal> outdoorTerminals) {
		this.outdoorTerminals = outdoorTerminals;
	}

	@JsonProperty("authTerminals")
	public List<AuthTerminal> getAuthTerminals() {
		return authTerminals;
	}

	@JsonProperty("authTerminals")
	public void setAuthTerminals(List<AuthTerminal> authTerminals) {
		this.authTerminals = authTerminals;
	}

	@JsonProperty("tankGauges")
	public List<TankGauge> getTankGauges() {
		return tankGauges;
	}

	@JsonProperty("tankGauges")
	public void setTankGauges(List<TankGauge> tankGauges) {
		this.tankGauges = tankGauges;
	}

	@JsonProperty("pricePanels")
	public List<PricePanel> getPricePanels() {
		return pricePanels;
	}

	@JsonProperty("pricePanels")
	public void setPricePanels(List<PricePanel> pricePanels) {
		this.pricePanels = pricePanels;
	}

	@JsonProperty("pos2bos")
	public List<Pos2Bos> getPos2bos() {
		return pos2bos;
	}

	@JsonProperty("pos2bos")
	public void setPos2bos(List<Pos2Bos> pos2bos) {
		this.pos2bos = pos2bos;
	}

	@JsonProperty("additionalParameters")
	public List<AdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<AdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("creationTimestamp", creationTimestamp)
				.append("companyInfo", companyInfo).append("seriesList", seriesList)
				.append("incidencesList", incidencesList).append("employees", employees).append("cardTypes", cardTypes)
				.append("taxInfo", taxInfo).append("forecourtInfo", forecourtInfo).append("posTerminals", posTerminals)
				.append("outdoorTerminals", outdoorTerminals).append("authTerminals", authTerminals)
				.append("tankGauges", tankGauges).append("pricePanels", pricePanels).append("pos2bos", pos2bos)
				.append("additionalParameters", additionalParameters).toString();
	}

}