package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "posCode", "address", "terminalType", "documents", "peripherals", "openShiftTimeSlots",
		"additionalParameters" })
public class OutdoorTerminal implements Serializable {

	@JsonProperty("posCode")
	private String posCode;
	@JsonProperty("address")
	private String address;
	@JsonProperty("terminalType")
	private String terminalType;
	@JsonProperty("documents")
	private List<Document> documents = new ArrayList<Document>();
	@JsonProperty("peripherals")
	private List<Peripheral> peripherals = new ArrayList<Peripheral>();
	@JsonProperty("additionalParameters")
	private List<OutdoorTerminalAdditionalParameter> additionalParameters = new ArrayList<OutdoorTerminalAdditionalParameter>();
	private final static long serialVersionUID = -90746787777085075L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public OutdoorTerminal() {
	}

	/**
	 *
	 * @param openShiftTimeSlots
	 * @param peripherals
	 * @param documents
	 * @param posCode
	 * @param additionalParameters
	 * @param address
	 * @param terminalType
	 */
	public OutdoorTerminal(String posCode, String address, String terminalType, List<Document> documents,
			List<Peripheral> peripherals, List<OutdoorTerminalAdditionalParameter> additionalParameters) {
		super();
		this.posCode = posCode;
		this.address = address;
		this.terminalType = terminalType;
		this.documents = documents;
		this.peripherals = peripherals;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("posCode")
	public String getPosCode() {
		return posCode;
	}

	@JsonProperty("posCode")
	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	@JsonProperty("address")
	public String getaddress() {
		return address;
	}

	@JsonProperty("address")
	public void setaddress(String address) {
		this.address = address;
	}

	@JsonProperty("terminalType")
	public String getterminalType() {
		return terminalType;
	}

	@JsonProperty("terminalType")
	public void setterminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@JsonProperty("peripherals")
	public List<Peripheral> getPeripherals() {
		return peripherals;
	}

	@JsonProperty("peripherals")
	public void setPeripherals(List<Peripheral> peripherals) {
		this.peripherals = peripherals;
	}

	@JsonProperty("additionalParameters")
	public List<OutdoorTerminalAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<OutdoorTerminalAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("posCode", posCode).append("address", address)
				.append("terminalType", terminalType).append("documents", documents).append("peripherals", peripherals)
				.append("additionalParameters", additionalParameters).toString();
	}

}

//--------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class OutdoorTerminalAdditionalParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<OutdoorTerminalParam> params = new ArrayList<OutdoorTerminalParam>();
	private final static long serialVersionUID = 3871252231538103333L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public OutdoorTerminalAdditionalParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public OutdoorTerminalAdditionalParameter(String section, List<OutdoorTerminalParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<OutdoorTerminalParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<OutdoorTerminalParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

//-----------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class OutdoorTerminalParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -6775909530310167408L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public OutdoorTerminalParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public OutdoorTerminalParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}
