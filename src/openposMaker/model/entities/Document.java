package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "serie", "form", "numCopies", "registrationForm", "observations", "refundSerie",
		"refundForm", "refundNumCopies", "refundRegistrationForm", "refundObservations" })
public class Document implements Serializable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("serie")
	private String serie;
	@JsonProperty("form")
	private String form;
	@JsonProperty("numCopies")
	private Integer numCopies;
	@JsonProperty("registrationForm")
	private String registrationForm;
	@JsonProperty("observations")
	private List<String> observations = new ArrayList<String>();
	@JsonProperty("refundSerie")
	private String refundSerie;
	@JsonProperty("refundForm")
	private String refundForm;
	@JsonProperty("refundNumCopies")
	private Integer refundNumCopies;
	@JsonProperty("refundRegistrationForm")
	private String refundRegistrationForm;
	@JsonProperty("refundObservations")
	private List<String> refundObservations = new ArrayList<String>();
	private final static long serialVersionUID = -7676653020329892771L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Document() {
	}

	/**
	 *
	 * @param refundForm
	 * @param form
	 * @param numCopies
	 * @param refundSerie
	 * @param observations
	 * @param serie
	 * @param refundObservations
	 * @param id
	 * @param refundRegistrationForm
	 * @param registrationForm
	 * @param refundNumCopies
	 */
	public Document(String id, String serie, String form, Integer numCopies, String registrationForm,
			List<String> observations, String refundSerie, String refundForm, Integer refundNumCopies,
			String refundRegistrationForm, List<String> refundObservations) {
		super();
		this.id = id;
		this.serie = serie;
		this.form = form;
		this.numCopies = numCopies;
		this.registrationForm = registrationForm;
		this.observations = observations;
		this.refundSerie = refundSerie;
		this.refundForm = refundForm;
		this.refundNumCopies = refundNumCopies;
		this.refundRegistrationForm = refundRegistrationForm;
		this.refundObservations = refundObservations;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("serie")
	public String getSerie() {
		return serie;
	}

	@JsonProperty("serie")
	public void setSerie(String serie) {
		this.serie = serie;
	}

	@JsonProperty("form")
	public String getForm() {
		return form;
	}

	@JsonProperty("form")
	public void setForm(String form) {
		this.form = form;
	}

	@JsonProperty("numCopies")
	public Integer getNumCopies() {
		return numCopies;
	}

	@JsonProperty("numCopies")
	public void setNumCopies(Integer numCopies) {
		this.numCopies = numCopies;
	}

	@JsonProperty("registrationForm")
	public String getRegistrationForm() {
		return registrationForm;
	}

	@JsonProperty("registrationForm")
	public void setRegistrationForm(String registrationForm) {
		this.registrationForm = registrationForm;
	}

	@JsonProperty("observations")
	public List<String> getObservations() {
		return observations;
	}

	@JsonProperty("observations")
	public void setObservations(List<String> observations) {
		this.observations = observations;
	}

	@JsonProperty("refundSerie")
	public String getRefundSerie() {
		return refundSerie;
	}

	@JsonProperty("refundSerie")
	public void setRefundSerie(String refundSerie) {
		this.refundSerie = refundSerie;
	}

	@JsonProperty("refundForm")
	public String getRefundForm() {
		return refundForm;
	}

	@JsonProperty("refundForm")
	public void setRefundForm(String refundForm) {
		this.refundForm = refundForm;
	}

	@JsonProperty("refundNumCopies")
	public Integer getRefundNumCopies() {
		return refundNumCopies;
	}

	@JsonProperty("refundNumCopies")
	public void setRefundNumCopies(Integer refundNumCopies) {
		this.refundNumCopies = refundNumCopies;
	}

	@JsonProperty("refundRegistrationForm")
	public String getRefundRegistrationForm() {
		return refundRegistrationForm;
	}

	@JsonProperty("refundRegistrationForm")
	public void setRefundRegistrationForm(String refundRegistrationForm) {
		this.refundRegistrationForm = refundRegistrationForm;
	}

	@JsonProperty("refundObservations")
	public List<String> getRefundObservations() {
		return refundObservations;
	}

	@JsonProperty("refundObservations")
	public void setRefundObservations(List<String> refundObservations) {
		this.refundObservations = refundObservations;
	}
	
	public Document clone() {
		Document cloneDocument = new Document();
		cloneDocument.setId(this.getId());
		cloneDocument.setSerie(this.getSerie());
		cloneDocument.setForm(this.getForm());
		cloneDocument.setNumCopies(this.getNumCopies());
		cloneDocument.setRegistrationForm(this.getRegistrationForm());
		cloneDocument.setObservations(this.getObservations());
		cloneDocument.setRefundSerie(this.getRefundSerie());
		cloneDocument.setRefundForm(this.getRefundForm());
		cloneDocument.setRefundNumCopies(this.getRefundNumCopies());
		cloneDocument.setRefundRegistrationForm(this.getRefundRegistrationForm());
		cloneDocument.setRefundObservations(this.getRefundObservations());
		return cloneDocument;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("serie", serie).append("form", form)
				.append("numCopies", numCopies).append("registrationForm", registrationForm)
				.append("observations", observations).append("refundSerie", refundSerie)
				.append("refundForm", refundForm).append("refundNumCopies", refundNumCopies)
				.append("refundRegistrationForm", refundRegistrationForm)
				.append("refundObservations", refundObservations).toString();
	}

}