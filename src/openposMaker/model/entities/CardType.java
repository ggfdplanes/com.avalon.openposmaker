package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "function", "posUser", "posVehicle", "manageType", "authorisation", "centerType",
		"activateOperative", "pointsIncidence", "cardBins", "dataList" })
public class CardType implements Serializable {

	public static final String[] CARDTYPE_FUNCTION = { "CREDITO ESTAGAS", "PAGO", "CONTADO", "FIDELIDAD", "DINERO",
			"CREDITO AUTORIZADO", "PROMOCIONES" };
	public static final Map<String, CardType> GENERIC_CARTYPES = new HashMap<String, CardType>();

	static {
		GENERIC_CARTYPES.put("OCWS CREDITO",
				new CardType("CREDITO", "CREDITO OC", "1", 0, 0, "", "yes", "71", "", "", Arrays.asList("999999")));
		GENERIC_CARTYPES.put("OCWS CONTADO", new CardType("CONTADO", "CONTADO OC", "2", 712, 1316, "", "yes", "71", "",
				"", Arrays.asList("888888")));
		GENERIC_CARTYPES.put("H24",
				new CardType("H24", "H24", "1", 0, 0, "", "yes", "0", "", "", Arrays.asList("2222")));
		GENERIC_CARTYPES.put("EMV",
				new CardType("EMV", "EMV", "1", 0, 0, "", "yes", "47", "", "", Arrays.asList("4", "5", "6", "1111")));
		GENERIC_CARTYPES.put("CASHGUARD",
				new CardType("CASHGUARD", "CASHGUARD", "2", 101, 202, "", "yes", "95", "", "", Arrays.asList("1234")));
		GENERIC_CARTYPES.put("BP BANCARIA",
				new CardType("BANCABP", "BANCARIO BP", "1", 0, 0, "", "yes", "70", "", "", Arrays.asList("11115")));
		GENERIC_CARTYPES.put("BP ROUTEX",
				new CardType("ROUTEX", "TARJETA FLOTA", "1", 0, 0, "", "yes", "70", "99", "", Arrays.asList("11111")));
		GENERIC_CARTYPES.put("BP ROUTEX MANUAL", new CardType("ROUTEXM", "TARJ.FLOTA MANUAL", "1", 0, 0, "", "yes",
				"70", "99", "", Arrays.asList("11112")));
		GENERIC_CARTYPES.put("BP LOYALTY", new CardType("LOYALTYBP", "FIDELIDAD MiBP", "2", 712, 1316, "", "yes", "70",
				"", "", Arrays.asList("11113", "242", "27", "20", "21", "29")));
		GENERIC_CARTYPES.put("BP LOYALTY MANUAL", new CardType("LOYALTYBPM", "FIDELIDAD MiBP MANUAL", "2", 712, 1316,
				"", "yes", "70", "", "", Arrays.asList("11114")));
		GENERIC_CARTYPES.put("BP FIDELIDAD", new CardType("FIDELIDAD", "Fidelidad Virtual", "2", 712, 1316, "", "yes",
				"70", "", "", Arrays.asList("030000")));
	}

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("function")
	private String function;
	@JsonProperty("posUser")
	private Integer posUser;
	@JsonProperty("posVehicle")
	private Integer posVehicle;
	@JsonProperty("manageType")
	private String manageType;
	@JsonProperty("authorisation")
	private String authorisation;
	@JsonProperty("centerType")
	private String centerType;
	@JsonProperty("activateOperative")
	private String activateOperative;
	@JsonProperty("cardBins")
	private List<String> cardBins = new ArrayList<String>();
	@JsonProperty("dataList")
	private List<CardTypeDataList> dataList = new ArrayList<CardTypeDataList>();
	@JsonProperty("pointsIncidence")
	private String pointsIncidence;
	private final static long serialVersionUID = -4989998817745007430L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public CardType() {
	}

	/**
	 *
	 * @param cardBins
	 * @param function
	 * @param authorisation
	 * @param dataList
	 * @param name
	 * @param posVehicle
	 * @param id
	 * @param centerType
	 * @param posUser
	 */
	public CardType(String id, String name, String function, Integer posUser, Integer posVehicle, String manageType,
			String authorisation, String centerType, String activateOperative, String pointsIncidence) {
		super();
		this.id = id;
		this.name = name;
		this.function = function;
		this.posUser = posUser;
		this.posVehicle = posVehicle;
		this.manageType = manageType;
		this.authorisation = authorisation;
		this.centerType = centerType;
		this.activateOperative = activateOperative;
		this.pointsIncidence = pointsIncidence;
	}

	public CardType(String id, String name, String function, Integer posUser, Integer posVehicle, String manageType,
			String authorisation, String centerType, String activateOperative, String pointsIncidence,
			List<String> cardBins) {
		this(id, name, function, posUser, posVehicle, manageType, authorisation, centerType, activateOperative,
				pointsIncidence);
		this.cardBins = cardBins;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("function")
	public String getFunction() {
		return function;
	}

	@JsonProperty("function")
	public void setFunction(String function) {
		this.function = function;
	}

	@JsonProperty("posUser")
	public Integer getPosUser() {
		return posUser;
	}

	@JsonProperty("posUser")
	public void setPosUser(Integer posUser) {
		this.posUser = posUser;
	}

	@JsonProperty("posVehicle")
	public Integer getPosVehicle() {
		return posVehicle;
	}

	@JsonProperty("posVehicle")
	public void setPosVehicle(Integer posVehicle) {
		this.posVehicle = posVehicle;
	}

	@JsonProperty("authorisation")
	public String getAuthorisation() {
		return authorisation;
	}

	@JsonProperty("authorisation")
	public void setAuthorisation(String authorisation) {
		this.authorisation = authorisation;
	}

	@JsonProperty("centerType")
	public String getCenterType() {
		return centerType;
	}

	@JsonProperty("centerType")
	public void setCenterType(String centerType) {
		this.centerType = centerType;
	}

	@JsonProperty("cardBins")
	public List<String> getCardBins() {
		return cardBins;
	}

	@JsonProperty("cardBins")
	public void setCardBins(List<String> cardBins) {
		this.cardBins = cardBins;
	}

	@JsonProperty("dataList")
	public List<CardTypeDataList> getDataList() {
		return dataList;
	}

	@JsonProperty("dataList")
	public void setDataList(List<CardTypeDataList> dataList) {
		this.dataList = dataList;
	}

	public CardTypeDataList getDataListItem(String type) {
		return this.dataList.stream().filter(data -> data.getType().equals(type)).findAny().orElse(null);
	}

	public void removeDataListItem(String type) {
		this.dataList.removeIf(data -> data.getType().equals(type));
	}

	@JsonProperty("pointsIncidence")
	public String getPointsIncidence() {
		return pointsIncidence;
	}

	@JsonProperty("pointsIncidence")
	public void setPointsIncidence(String pointsIncidence) {
		this.pointsIncidence = pointsIncidence;
	}

	@JsonProperty("manageType")
	public String getManageType() {
		return manageType;
	}

	@JsonProperty("manageType")
	public void setManageType(String manageType) {
		this.manageType = manageType;
	}

	@JsonProperty("activateOperative")
	public String getActivateOperative() {
		return activateOperative;
	}

	@JsonProperty("activateOperative")
	public void setActivateOperative(String activateOperative) {
		this.activateOperative = activateOperative;
	}
	
	
	

	public static CardType clone(CardType cardType) {
		CardType cloneCardType = new CardType();
		cloneCardType.setId(cardType.getId());
		cloneCardType.setName(cardType.getName());
		cloneCardType.setFunction(cardType.getFunction());
		cloneCardType.setPosUser(cardType.getPosUser());
		cloneCardType.setPosVehicle(cardType.getPosVehicle());
		cloneCardType.setManageType(cardType.getManageType());
		cloneCardType.setAuthorisation(cardType.getAuthorisation());
		cloneCardType.setCenterType(cardType.getCenterType());
		cloneCardType.setActivateOperative(cardType.getActivateOperative());
		cloneCardType.setPointsIncidence(cardType.getPointsIncidence());
		cloneCardType.setCardBins(new ArrayList<String>(cardType.getCardBins()));
		cloneCardType.setDataList(new ArrayList<CardTypeDataList>(cardType.getDataList()));
		return cloneCardType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardType other = (CardType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("function", function)
				.append("posUser", posUser).append("posVehicle", posVehicle).append("manageType", manageType)
				.append("authorisation", authorisation).append("centerType", centerType)
				.append("activateOperative", activateOperative).append("cardBins", cardBins)
				.append("dataList", dataList).append("pointsIncidence", pointsIncidence).toString();
	}

}

//----------------------------------
