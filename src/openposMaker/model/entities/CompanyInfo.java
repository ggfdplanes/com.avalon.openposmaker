package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"companyCode",
"shortName",
"longName",
"addressType",
"address",
"addressNumber",
"postCode",
"city",
"currencyInfo",
"phoneNumber"
})
public class CompanyInfo implements Serializable
{

@JsonProperty("companyCode")
private String companyCode;
@JsonProperty("shortName")
private String shortName;
@JsonProperty("longName")
private String longName;
@JsonProperty("addressType")
private String addressType;
@JsonProperty("address")
private String address;
@JsonProperty("addressNumber")
private String addressNumber;
@JsonProperty("postCode")
private String postCode;
@JsonProperty("city")
private String city;
@JsonProperty("currencyInfo")
private CurrencyInfo currencyInfo;
@JsonProperty("phoneNumber")
private String phoneNumber;
private final static long serialVersionUID = 6516337451790397421L;

/**
* No args constructor for use in serialization
*
*/
public CompanyInfo() {
}

public static CompanyInfo getDefaultInstance() {
	CompanyInfo company = new CompanyInfo();
	company.setCompanyCode("001");
	company.setLongName("");
	company.currencyInfo = new CurrencyInfo();
	company.getCurrencyInfo().getMainCurrency().setCode("EU");
	company.getCurrencyInfo().getMainCurrency().setName("euro");
	company.getCurrencyInfo().getMainCurrency().setDecimalPositions(2);
	company.getCurrencyInfo().getSecondaryCurrency().setCode("EU");
	company.getCurrencyInfo().getSecondaryCurrency().setName("euro");
	company.getCurrencyInfo().getSecondaryCurrency().setDecimalPositions(2);
	company.getCurrencyInfo().getSecondaryCurrency().setValue(1);
	return company;
}

/**
*
* @param companyCode
* @param address
* @param phoneNumber
* @param city
* @param addressType
* @param addressNumber
* @param currencyInfo
* @param postCode
* @param shortName
* @param longName
*/
public CompanyInfo(String companyCode, String shortName, String longName, String addressType, String address, String addressNumber, String postCode, String city, CurrencyInfo currencyInfo, String phoneNumber) {
super();
this.companyCode = companyCode;
this.shortName = shortName;
this.longName = longName;
this.addressType = addressType;
this.address = address;
this.addressNumber = addressNumber;
this.postCode = postCode;
this.city = city;
this.currencyInfo = currencyInfo;
this.phoneNumber = phoneNumber;
}

@JsonProperty("companyCode")
public String getCompanyCode() {
return companyCode;
}

@JsonProperty("companyCode")
public void setCompanyCode(String companyCode) {
this.companyCode = companyCode;
}

@JsonProperty("shortName")
public String getShortName() {
return shortName;
}

@JsonProperty("shortName")
public void setShortName(String shortName) {
this.shortName = shortName;
}

@JsonProperty("longName")
public String getLongName() {
return longName;
}

@JsonProperty("longName")
public void setLongName(String longName) {
this.longName = longName;
}

@JsonProperty("addressType")
public String getAddressType() {
return addressType;
}

@JsonProperty("addressType")
public void setAddressType(String addressType) {
this.addressType = addressType;
}

@JsonProperty("address")
public String getAddress() {
return address;
}

@JsonProperty("address")
public void setAddress(String address) {
this.address = address;
}

@JsonProperty("addressNumber")
public String getAddressNumber() {
return addressNumber;
}

@JsonProperty("addressNumber")
public void setAddressNumber(String addressNumber) {
this.addressNumber = addressNumber;
}

@JsonProperty("postCode")
public String getPostCode() {
return postCode;
}

@JsonProperty("postCode")
public void setPostCode(String postCode) {
this.postCode = postCode;
}

@JsonProperty("city")
public String getCity() {
return city;
}

@JsonProperty("city")
public void setCity(String city) {
this.city = city;
}

@JsonProperty("currencyInfo")
public CurrencyInfo getCurrencyInfo() {
return currencyInfo;
}

@JsonProperty("currencyInfo")
public void setCurrencyInfo(CurrencyInfo currencyInfo) {
this.currencyInfo = currencyInfo;
}

@JsonProperty("phoneNumber")
public String getPhoneNumber() {
return phoneNumber;
}

@JsonProperty("phoneNumber")
public void setPhoneNumber(String phoneNumber) {
this.phoneNumber = phoneNumber;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("companyCode", companyCode).append("shortName", shortName).append("longName", longName).append("addressType", addressType).append("address", address).append("addressNumber", addressNumber).append("postCode", postCode).append("city", city).append("currencyInfo", currencyInfo).append("phoneNumber", phoneNumber).toString();
}

}