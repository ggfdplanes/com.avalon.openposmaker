package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"code",
"name",
"taxId",
"fcGradeId"
})
public class Grade implements Serializable
{

@JsonProperty("code")
private String code;
@JsonProperty("name")
private String name;
@JsonProperty("taxId")
private String taxId;
@JsonProperty("fcGradeId")
private String fcGradeId;
private final static long serialVersionUID = -4437139788749041076L;

/**
* No args constructor for use in serialization
*
*/
public Grade() {
}

/**
*
* @param code
* @param fcGradeId
* @param taxId
* @param name
*/
public Grade(String code, String name, String taxId, String fcGradeId) {
super();
this.code = code;
this.name = name;
this.taxId = taxId;
this.fcGradeId = fcGradeId;
}

@JsonProperty("code")
public String getCode() {
return code;
}

@JsonProperty("code")
public void setCode(String code) {
this.code = code;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("taxId")
public String getTaxId() {
return taxId;
}

@JsonProperty("taxId")
public void setTaxId(String taxId) {
this.taxId = taxId;
}

@JsonProperty("fcGradeId")
public String getFcGradeId() {
return fcGradeId;
}

@JsonProperty("fcGradeId")
public void setFcGradeId(String fcGradeId) {
this.fcGradeId = fcGradeId;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("code", code).append("name", name).append("taxId", taxId).append("fcGradeId", fcGradeId).toString();
}

}