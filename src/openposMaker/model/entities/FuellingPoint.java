package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "controllerType", "channel", "deviceId", "deviceType", "hoseList", "operationModeList" })
public class FuellingPoint implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("controllerType")
	private Integer controllerType;
	@JsonProperty("channel")
	private Integer channel;
	@JsonProperty("deviceId")
	private Integer deviceId;
	@JsonProperty("deviceType")
	private Integer deviceType;
	@JsonProperty("hoseList")
	private List<Hose> hoseList = new ArrayList<Hose>();
	@JsonProperty("operationModeList")
	private List<OperationMode> operationModeList = new ArrayList<OperationMode>();
	private final static long serialVersionUID = 4731836192089219487L;

	private final static Map<Integer, String> CONTROLLER_LIST;

	static {
		CONTROLLER_LIST = new HashMap<Integer, String>();
		CONTROLLER_LIST.put(0, "DOMS RS232");
		CONTROLLER_LIST.put(1, "CEPSA");
		CONTROLLER_LIST.put(2, "Control demo");
		CONTROLLER_LIST.put(3, "EinFrance");
		CONTROLLER_LIST.put(4, "Hispano-Ljungmans");
		CONTROLLER_LIST.put(5, "Droher");
		CONTROLLER_LIST.put(7, "DOMS IP");
	}

//
//#  0 - DOMS
//#  1 - CEPSA
//#  2 - Control demo (propio)
//#  3 - EinFrance
//#  4 - Hispano-Ljungmans
//#  5 - Droher
	/**
	 * No args constructor for use in serialization
	 *
	 */
	public FuellingPoint() {
	}

	/**
	 *
	 * @param deviceType
	 * @param controllerType
	 * @param channel
	 * @param hoseList
	 * @param id
	 * @param operationModeList
	 * @param deviceId
	 */
	public FuellingPoint(Integer id, Integer controllerType, Integer channel, Integer deviceId, Integer deviceType,
			List<Hose> hoseList, List<OperationMode> operationModeList) {
		super();
		this.id = id;
		this.controllerType = controllerType;
		this.channel = channel;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.hoseList = hoseList;
		this.operationModeList = operationModeList;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("controllerType")
	public Integer getControllerType() {
		return controllerType;
	}

	@JsonProperty("controllerType")
	public void setControllerType(Integer controllerType) {
		this.controllerType = controllerType;
	}

	@JsonProperty("channel")
	public Integer getChannel() {
		return channel;
	}

	@JsonProperty("channel")
	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	@JsonProperty("deviceId")
	public Integer getDeviceId() {
		return deviceId;
	}

	@JsonProperty("deviceId")
	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	@JsonProperty("deviceType")
	public Integer getDeviceType() {
		return deviceType;
	}

	@JsonProperty("deviceType")
	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

	@JsonProperty("hoseList")
	public List<Hose> getHoseList() {
		return hoseList;
	}

	@JsonProperty("hoseList")
	public void setHoseList(List<Hose> hoseList) {
		this.hoseList = hoseList;
	}

	@JsonProperty("operationModeList")
	public List<OperationMode> getOperationModeList() {
		return operationModeList;
	}

	@JsonProperty("operationModeList")
	public void setOperationModeList(List<OperationMode> operationModeList) {
		this.operationModeList = operationModeList;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("controllerType", controllerType)
				.append("channel", channel).append("deviceId", deviceId).append("deviceType", deviceType)
				.append("hoseList", hoseList).append("operationModeList", operationModeList).toString();
	}

}