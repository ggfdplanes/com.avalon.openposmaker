package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.deser.std.FromStringDeserializer;

import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "posId", "address", "registrationPos", "priceDecimals", "moneyDecimals", "volumeDecimals",
		"gradeList", "tankList", "fuellingModeList", "serviceModeList", "fuellingModeGroupList", "priceGroupList",
		"fcOperationModeList", "sellingModeList", "fuellingPointList", "additionalParameters",
		"nonConnectedHosesList" })
public class ForecourtInfo implements Serializable {

	@JsonProperty("posId")
	private Integer posId;
	@JsonProperty("address")
	private String address;
	@JsonProperty("registrationPos")
	private String registrationPos;
	@JsonProperty("priceDecimals")
	private Integer priceDecimals;
	@JsonProperty("moneyDecimals")
	private Integer moneyDecimals;
	@JsonProperty("volumeDecimals")
	private Integer volumeDecimals;
	@JsonProperty("gradeList")
	private List<Grade> gradeList = new ArrayList<Grade>();
	@JsonProperty("tankList")
	private List<Tank> tankList = new ArrayList<Tank>();
	@JsonProperty("fuellingModeList")
	private List<FuellingMode> fuellingModeList = new ArrayList<FuellingMode>();
	@JsonProperty("serviceModeList")
	private List<ServiceMode> serviceModeList = new ArrayList<ServiceMode>();
	@JsonProperty("fuellingModeGroupList")
	private List<FuellingModeGroup> fuellingModeGroupList = new ArrayList<FuellingModeGroup>();
	@JsonProperty("priceGroupList")
	private List<Integer> priceGroupList = new ArrayList<Integer>();
	@JsonProperty("fcOperationModeList")
	private List<FcOperationMode> fcOperationModeList = new ArrayList<FcOperationMode>();
	@JsonProperty("sellingModeList")
	private List<SellingMode> sellingModeList = new ArrayList<SellingMode>();
	@JsonProperty("fuellingPointList")
	private List<FuellingPoint> fuellingPointList = new ArrayList<FuellingPoint>();
	@JsonProperty("additionalParameters")
	private List<ForecourtAdditionalParameter> additionalParameters = new ArrayList<ForecourtAdditionalParameter>();
	@JsonProperty("nonConnectedHosesList")
	private List<NonConnectedHosesList> nonConnectedHosesList = new ArrayList<NonConnectedHosesList>();
	private final static long serialVersionUID = -3988679468234758492L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ForecourtInfo() {

	}
	
	public static ForecourtInfo getDefaultInstance() {
		ForecourtInfo fInfo = new ForecourtInfo();
		fInfo.gradeList.add(new Grade("90", "Diesel", "3", "90"));
		fInfo.gradeList.add(new Grade("94", "Diesel extra", "3", "94"));
		fInfo.gradeList.add(new Grade("95", "Gasolina 95", "3", "95"));
		fInfo.gradeList.add(new Grade("98", "Gasolina 98", "3", "98"));
		fInfo.gradeList.add(new Grade("92", "Gasoleo Bonificado", "3", "92"));
		fInfo.gradeList.add(new Grade("93", "Gasoleo C", "3", "93"));
		fInfo.gradeList.add(new Grade("96", "GLP", "3", "96"));
		fInfo.gradeList.add(new Grade("97", "Gasolina 95 Premium", "3", "97"));
		fInfo.gradeList.add(new Grade("99", "AdBlue", "3", "99"));
		fInfo.tankList.add(new Tank(1, "90", 30000));
		fInfo.tankList.add(new Tank(2, "94", 30000));
		fInfo.tankList.add(new Tank(3, "95", 30000));
		fInfo.tankList.add(new Tank(4, "98", 30000));
		return fInfo;
	}

	/**
	 *
	 * @param address
	 * @param fuellingModeList
	 * @param sellingModeList
	 * @param priceGroupList
	 * @param tankList
	 * @param volumeDecimals
	 * @param serviceModeList
	 * @param posId
	 * @param registrationPos
	 * @param moneyDecimals
	 * @param gradeList
	 * @param priceDecimals
	 * @param fuellingModeGroupList
	 * @param additionalParameters
	 * @param fcOperationModeList
	 * @param fuellingPointList
	 */
	public ForecourtInfo(Integer posId, String address, String registrationPos, Integer priceDecimals,
			Integer moneyDecimals, Integer volumeDecimals, List<Grade> gradeList, List<Tank> tankList,
			List<FuellingMode> fuellingModeList, List<ServiceMode> serviceModeList,
			List<FuellingModeGroup> fuellingModeGroupList, List<Integer> priceGroupList,
			List<FcOperationMode> fcOperationModeList, List<SellingMode> sellingModeList,
			List<FuellingPoint> fuellingPointList, List<ForecourtAdditionalParameter> additionalParameters,
			List<NonConnectedHosesList> nonConnectedHosesList) {
		super();
		this.posId = posId;
		this.address = address;
		this.registrationPos = registrationPos;
		this.priceDecimals = priceDecimals;
		this.moneyDecimals = moneyDecimals;
		this.volumeDecimals = volumeDecimals;
		this.gradeList = gradeList;
		this.tankList = tankList;
		this.fuellingModeList = fuellingModeList;
		this.serviceModeList = serviceModeList;
		this.fuellingModeGroupList = fuellingModeGroupList;
		this.priceGroupList = priceGroupList;
		this.fcOperationModeList = fcOperationModeList;
		this.sellingModeList = sellingModeList;
		this.fuellingPointList = fuellingPointList;
		this.additionalParameters = additionalParameters;
		this.nonConnectedHosesList = nonConnectedHosesList;
	}

	@JsonProperty("posId")
	public Integer getPosId() {
		return posId;
	}

	@JsonProperty("posId")
	public void setPosId(Integer posId) {
		this.posId = posId;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("registrationPos")
	public String getRegistrationPos() {
		return registrationPos;
	}

	@JsonProperty("registrationPos")
	public void setRegistrationPos(String registrationPos) {
		this.registrationPos = registrationPos;
	}

	@JsonProperty("priceDecimals")
	public Integer getPriceDecimals() {
		return priceDecimals;
	}

	@JsonProperty("priceDecimals")
	public void setPriceDecimals(Integer priceDecimals) {
		this.priceDecimals = priceDecimals;
	}

	@JsonProperty("moneyDecimals")
	public Integer getMoneyDecimals() {
		return moneyDecimals;
	}

	@JsonProperty("moneyDecimals")
	public void setMoneyDecimals(Integer moneyDecimals) {
		this.moneyDecimals = moneyDecimals;
	}

	@JsonProperty("volumeDecimals")
	public Integer getVolumeDecimals() {
		return volumeDecimals;
	}

	@JsonProperty("volumeDecimals")
	public void setVolumeDecimals(Integer volumeDecimals) {
		this.volumeDecimals = volumeDecimals;
	}

	@JsonProperty("gradeList")
	public List<Grade> getGradeList() {
		return gradeList;
	}

	@JsonProperty("gradeList")
	public void setGradeList(List<Grade> gradeList) {
		this.gradeList = gradeList;
	}

	@JsonProperty("tankList")
	public List<Tank> getTankList() {
		return tankList;
	}

	@JsonProperty("tankList")
	public void setTankList(List<Tank> tankList) {
		this.tankList = tankList;
	}

	@JsonProperty("fuellingModeList")
	public List<FuellingMode> getFuellingModeList() {
		return fuellingModeList;
	}

	@JsonProperty("fuellingModeList")
	public void setFuellingModeList(List<FuellingMode> fuellingModeList) {
		this.fuellingModeList = fuellingModeList;
	}

	@JsonProperty("serviceModeList")
	public List<ServiceMode> getServiceModeList() {
		return serviceModeList;
	}

	@JsonProperty("serviceModeList")
	public void setServiceModeList(List<ServiceMode> serviceModeList) {
		this.serviceModeList = serviceModeList;
	}

	@JsonProperty("fuellingModeGroupList")
	public List<FuellingModeGroup> getFuellingModeGroupList() {
		return fuellingModeGroupList;
	}

	@JsonProperty("fuellingModeGroupList")
	public void setFuellingModeGroupList(List<FuellingModeGroup> fuellingModeGroupList) {
		this.fuellingModeGroupList = fuellingModeGroupList;
	}

	@JsonProperty("priceGroupList")
	public List<Integer> getPriceGroupList() {
		return priceGroupList;
	}

	@JsonProperty("priceGroupList")
	public void setPriceGroupList(List<Integer> priceGroupList) {
		this.priceGroupList = priceGroupList;
	}

	@JsonProperty("fcOperationModeList")
	public List<FcOperationMode> getFcOperationModeList() {
		return fcOperationModeList;
	}

	@JsonProperty("fcOperationModeList")
	public void setFcOperationModeList(List<FcOperationMode> fcOperationModeList) {
		this.fcOperationModeList = fcOperationModeList;
	}

	@JsonProperty("sellingModeList")
	public List<SellingMode> getSellingModeList() {
		return sellingModeList;
	}

	@JsonProperty("sellingModeList")
	public void setSellingModeList(List<SellingMode> sellingModeList) {
		this.sellingModeList = sellingModeList;
	}

	@JsonProperty("fuellingPointList")
	public List<FuellingPoint> getFuellingPointList() {
		return fuellingPointList;
	}

	@JsonProperty("fuellingPointList")
	public void setFuellingPointList(List<FuellingPoint> fuellingPointList) {
		this.fuellingPointList = fuellingPointList;
	}

	@JsonProperty("additionalParameters")
	public List<ForecourtAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<ForecourtAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("nonConnectedHosesList")
	public List<NonConnectedHosesList> getNonConnectedHosesList() {
		return nonConnectedHosesList;
	}

	@JsonProperty("nonConnectedHosesList")
	public void setNonConnectedHosesList(List<NonConnectedHosesList> nonConnectedHosesList) {
		this.nonConnectedHosesList = nonConnectedHosesList;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("posId", posId).append("address", address)
				.append("registrationPos", registrationPos).append("priceDecimals", priceDecimals)
				.append("moneyDecimals", moneyDecimals).append("volumeDecimals", volumeDecimals)
				.append("gradeList", gradeList).append("tankList", tankList)
				.append("fuellingModeList", fuellingModeList).append("serviceModeList", serviceModeList)
				.append("fuellingModeGroupList", fuellingModeGroupList).append("priceGroupList", priceGroupList)
				.append("fcOperationModeList", fcOperationModeList).append("sellingModeList", sellingModeList)
				.append("fuellingPointList", fuellingPointList).append("additionalParameters", additionalParameters)
				.append("nonConnectedHosesList", nonConnectedHosesList).toString();
	}

}

///---------------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class ForecourtAdditionalParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<ForecourtInfoParam> params = new ArrayList<ForecourtInfoParam>();
	private final static long serialVersionUID = 7346295572080922143L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ForecourtAdditionalParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public ForecourtAdditionalParameter(String section, List<ForecourtInfoParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<ForecourtInfoParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<ForecourtInfoParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

//--------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })

class ForecourtInfoParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = 9151850607549490273L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ForecourtInfoParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public ForecourtInfoParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}

//---------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "openposId", "tankId" })
class NonConnectedHosesList implements Serializable {

	@JsonProperty("openposId")
	private Integer openposId;
	@JsonProperty("tankId")
	private Integer tankId;
	private final static long serialVersionUID = -2849781610329537808L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public NonConnectedHosesList() {
	}

	/**
	 *
	 * @param tankId
	 * @param openposId
	 */
	public NonConnectedHosesList(Integer openposId, Integer tankId) {
		super();
		this.openposId = openposId;
		this.tankId = tankId;
	}

	@JsonProperty("openposId")
	public Integer getOpenposId() {
		return openposId;
	}

	@JsonProperty("openposId")
	public void setOpenposId(Integer openposId) {
		this.openposId = openposId;
	}

	@JsonProperty("tankId")
	public Integer getTankId() {
		return tankId;
	}

	@JsonProperty("tankId")
	public void setTankId(Integer tankId) {
		this.tankId = tankId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("openposId", openposId).append("tankId", tankId).toString();
	}

}