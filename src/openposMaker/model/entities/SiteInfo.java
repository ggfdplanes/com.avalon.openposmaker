package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "siteCode", "siteName", "vatNumber", "address", "city", "postCode", "province", "numberOfShifts",
		"commissionist", "offerFamily", "serviceFamily", "discountItem", "greetingFirstLine", "greetingSecondLine",
		"priceWarningTime" })
public class SiteInfo implements Serializable {

	@JsonProperty("siteCode")
	private String siteCode;
	@JsonProperty("siteName")
	private String siteName;
	@JsonProperty("vatNumber")
	private String vatNumber;
	@JsonProperty("address")
	private String address;
	@JsonProperty("city")
	private String city;
	@JsonProperty("postCode")
	private String postCode;
	@JsonProperty("province")
	private String province;
	@JsonProperty("numberOfShifts")
	private Integer numberOfShifts;
	@JsonProperty("commissionist")
	private String commissionist;
	@JsonProperty("offerFamily")
	private String offerFamily;
	@JsonProperty("serviceFamily")
	private String serviceFamily;
	@JsonProperty("discountItem")
	private String discountItem;
	@JsonProperty("greetingFirstLine")
	private String greetingFirstLine;
	@JsonProperty("greetingSecondLine")
	private String greetingSecondLine;
	@JsonProperty("priceWarningTime")
	private Integer priceWarningTime;
	private final static long serialVersionUID = 7060208999238579889L;

	
	public SiteInfo() {
		
	}
	
	public static SiteInfo getDefaultInstance() {
		SiteInfo site = new SiteInfo();
		site.siteCode = "001";
		site.commissionist = "no";
		site.numberOfShifts = 3;
		site.greetingFirstLine = "~   BIENVENIDO AL  ~";
		site.greetingSecondLine = "~ ESTABLECIMIENTO  ~";
		site.offerFamily = "OF";
		site.serviceFamily = "SE";
		site.discountItem = "66660000";
		site.priceWarningTime = 3600;
		return site;
	}

	/**
	 *
	 * @param siteCode
	 * @param address
	 * @param city
	 * @param serviceFamily
	 * @param numberOfShifts
	 * @param commissionist
	 * @param discountItem
	 * @param siteName
	 * @param offerFamily
	 * @param greetingFirstLine
	 * @param province
	 * @param postCode
	 * @param greetingSecondLine
	 * @param priceWarningTime
	 * @param vatNumber
	 */
	public SiteInfo(String siteCode, String siteName, String vatNumber, String address, String city, String postCode,
			String province, Integer numberOfShifts, String commissionist, String offerFamily, String serviceFamily,
			String discountItem, String greetingFirstLine, String greetingSecondLine, Integer priceWarningTime) {
		super();
		this.siteCode = siteCode;
		this.siteName = siteName;
		this.vatNumber = vatNumber;
		this.address = address;
		this.city = city;
		this.postCode = postCode;
		this.province = province;
		this.numberOfShifts = numberOfShifts;
		this.commissionist = commissionist;
		this.offerFamily = offerFamily;
		this.serviceFamily = serviceFamily;
		this.discountItem = discountItem;
		this.greetingFirstLine = greetingFirstLine;
		this.greetingSecondLine = greetingSecondLine;
		this.priceWarningTime = priceWarningTime;
	}

	@JsonProperty("siteCode")
	public String getSiteCode() {
		return siteCode;
	}

	@JsonProperty("siteCode")
	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	@JsonProperty("siteName")
	public String getSiteName() {
		return siteName;
	}

	@JsonProperty("siteName")
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	@JsonProperty("vatNumber")
	public String getVatNumber() {
		return vatNumber;
	}

	@JsonProperty("vatNumber")
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("city")
	public String getCity() {
		return city;
	}

	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	@JsonProperty("postCode")
	public String getPostCode() {
		return postCode;
	}

	@JsonProperty("postCode")
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	@JsonProperty("province")
	public String getProvince() {
		return province;
	}

	@JsonProperty("province")
	public void setProvince(String province) {
		this.province = province;
	}

	@JsonProperty("numberOfShifts")
	public Integer getNumberOfShifts() {
		return numberOfShifts;
	}

	@JsonProperty("numberOfShifts")
	public void setNumberOfShifts(Integer numberOfShifts) {
		this.numberOfShifts = numberOfShifts;
	}

	@JsonProperty("commissionist")
	public String getCommissionist() {
		return commissionist;
	}

	@JsonProperty("commissionist")
	public void setCommissionist(String commissionist) {
		this.commissionist = commissionist;
	}

	@JsonProperty("offerFamily")
	public String getOfferFamily() {
		return offerFamily;
	}

	@JsonProperty("offerFamily")
	public void setOfferFamily(String offerFamily) {
		this.offerFamily = offerFamily;
	}

	@JsonProperty("serviceFamily")
	public String getServiceFamily() {
		return serviceFamily;
	}

	@JsonProperty("serviceFamily")
	public void setServiceFamily(String serviceFamily) {
		this.serviceFamily = serviceFamily;
	}

	@JsonProperty("discountItem")
	public String getDiscountItem() {
		return discountItem;
	}

	@JsonProperty("discountItem")
	public void setDiscountItem(String discountItem) {
		this.discountItem = discountItem;
	}

	@JsonProperty("greetingFirstLine")
	public String getGreetingFirstLine() {
		return greetingFirstLine;
	}

	@JsonProperty("greetingFirstLine")
	public void setGreetingFirstLine(String greetingFirstLine) {
		this.greetingFirstLine = greetingFirstLine;
	}

	@JsonProperty("greetingSecondLine")
	public String getGreetingSecondLine() {
		return greetingSecondLine;
	}

	@JsonProperty("greetingSecondLine")
	public void setGreetingSecondLine(String greetingSecondLine) {
		this.greetingSecondLine = greetingSecondLine;
	}

	@JsonProperty("priceWarningTime")
	public Integer getPriceWarningTime() {
		return priceWarningTime;
	}

	@JsonProperty("priceWarningTime")
	public void setPriceWarningTime(Integer priceWarningTime) {
		this.priceWarningTime = priceWarningTime;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("siteCode", siteCode).append("siteName", siteName)
				.append("vatNumber", vatNumber).append("address", address).append("city", city)
				.append("postCode", postCode).append("province", province).append("numberOfShifts", numberOfShifts)
				.append("commissionist", commissionist).append("offerFamily", offerFamily)
				.append("serviceFamily", serviceFamily).append("discountItem", discountItem)
				.append("greetingFirstLine", greetingFirstLine).append("greetingSecondLine", greetingSecondLine)
				.append("priceWarningTime", priceWarningTime).toString();
	}

}