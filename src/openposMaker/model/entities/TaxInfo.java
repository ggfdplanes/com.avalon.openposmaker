package openposMaker.model.entities;

import  java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "taxId", "name", "value" })
public class TaxInfo implements Serializable {

	public static final Map<String,List<TaxInfo>> GENERIC_TAX_TYPES = new HashMap<String, List<TaxInfo>>();
	
	static {
		List<TaxInfo> listTaxInfoGenericIVA = new ArrayList<TaxInfo>();
		listTaxInfoGenericIVA.add(new TaxInfo("0", "IVA EXENTO" , 0.0));
		listTaxInfoGenericIVA.add(new TaxInfo("1", "IVA SUPER REDUCIDO" , 4.0));
		listTaxInfoGenericIVA.add(new TaxInfo("2", "IVA REDUCIDO" , 10.0));
		listTaxInfoGenericIVA.add(new TaxInfo("3", "IVA NORMAL" , 21.0));
		List<TaxInfo> listTaxInfoGenericIGIC = new ArrayList<TaxInfo>();
		listTaxInfoGenericIGIC.add(new TaxInfo("4", "IGIC EXENTO" , 0.0));
		listTaxInfoGenericIGIC.add(new TaxInfo("5", "IGIC GENERAL" , 7.0));
		listTaxInfoGenericIGIC.add(new TaxInfo("6", "IGIC REDUCIDO" , 3.0));
		listTaxInfoGenericIGIC.add(new TaxInfo("7", "IGIC INCREMENTADO 1" , 9.5));
		listTaxInfoGenericIGIC.add(new TaxInfo("8", "IGIC INCREMENTADO 2" , 13.5));
		listTaxInfoGenericIGIC.add(new TaxInfo("9", "IGIC ESPECIAL" , 20.0));
		GENERIC_TAX_TYPES.put("IVA", listTaxInfoGenericIVA);
		GENERIC_TAX_TYPES.put("IGIC", listTaxInfoGenericIGIC);
	}
	
	@JsonProperty("taxId")
	private String taxId;
	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private Double value;
	private final static long serialVersionUID = 6489217704114356815L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public TaxInfo() {
	}

	/**
	 *
	 * @param taxId
	 * @param name
	 * @param value
	 */
	public TaxInfo(String taxId, String name, Double value) {
		super();
		this.taxId = taxId;
		this.name = name;
		this.value = value;
	}

	@JsonProperty("taxId")
	public String getTaxId() {
		return taxId;
	}

	@JsonProperty("taxId")
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public Double getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("taxId", taxId).append("name", name).append("value", value).toString();
	}

}