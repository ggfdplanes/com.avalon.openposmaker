package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "type", "driver", "port" })
public class Peripheral implements Serializable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("driver")
	private String driver;
	@JsonProperty("port")
	private String port;
	private final static long serialVersionUID = 8304126311147143967L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Peripheral() {
	}

	/**
	 *
	 * @param driver
	 * @param port
	 * @param id
	 * @param type
	 */
	public Peripheral(String id, String type, String driver, String port) {
		super();
		this.id = id;
		this.type = type;
		this.driver = driver;
		this.port = port;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("driver")
	public String getDriver() {
		return driver;
	}

	@JsonProperty("driver")
	public void setDriver(String driver) {
		this.driver = driver;
	}

	@JsonProperty("port")
	public String getPort() {
		return port;
	}

	@JsonProperty("port")
	public void setPort(String port) {
		this.port = port;
	}

	public Peripheral clone() {
		Peripheral clonePeripheral = new Peripheral();
		clonePeripheral.setId(this.getId());
		clonePeripheral.setType(this.type);
		clonePeripheral.setDriver(this.driver);
		clonePeripheral.setPort(this.port);
		return clonePeripheral;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("type", type).append("driver", driver)
				.append("port", port).toString();
	}

}