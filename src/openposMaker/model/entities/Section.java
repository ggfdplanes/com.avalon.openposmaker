package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
public class Section implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<SectionParam> params = new ArrayList<SectionParam>();
	private final static long serialVersionUID = 1503978982627407795L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Section() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public Section(String section) {
		super();
		this.section = section;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<SectionParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<SectionParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}