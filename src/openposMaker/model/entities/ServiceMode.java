package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"params"
})
public class ServiceMode implements Serializable
{

@JsonProperty("id")
private Integer id;
@JsonProperty("params")
private List<ServiceModeParam> params = new ArrayList<ServiceModeParam>();
private final static long serialVersionUID = -3947083871409583909L;

/**
* No args constructor for use in serialization
*
*/
public ServiceMode() {
}

/**
*
* @param id
* @param params
*/
public ServiceMode(Integer id, List<ServiceModeParam> params) {
super();
this.id = id;
this.params = params;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("params")
public List<ServiceModeParam> getParams() {
return params;
}

@JsonProperty("params")
public void setParams(List<ServiceModeParam> params) {
this.params = params;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("params", params).toString();
}

}

//-------------------------------------------------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })

class ServiceModeParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	
	private final static long serialVersionUID = 7313899982496829747L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public ServiceModeParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public ServiceModeParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}