package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "positions", "additionalParameters" })
public class PricePanel implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("positions")
	private List<Object> positions = new ArrayList<Object>();
	@JsonProperty("additionalParameters")
	private List<PricePanelAdditionalParameter> additionalParameters = new ArrayList<PricePanelAdditionalParameter>();
	private final static long serialVersionUID = -2392246812917392782L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public PricePanel() {
	}

	/**
	 *
	 * @param positions
	 * @param additionalParameters
	 * @param id
	 */
	public PricePanel(Integer id, List<Object> positions, List<PricePanelAdditionalParameter> additionalParameters) {
		super();
		this.id = id;
		this.positions = positions;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("positions")
	public List<Object> getPositions() {
		return positions;
	}

	@JsonProperty("positions")
	public void setPositions(List<Object> positions) {
		this.positions = positions;
	}

	@JsonProperty("additionalParameters")
	public List<PricePanelAdditionalParameter> getPricePanelAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setPricePanelAdditionalParameters(List<PricePanelAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("positions", positions)
				.append("additionalParameters", additionalParameters).toString();
	}

}

//--------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class PricePanelAdditionalParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<PricePanelParam> params = new ArrayList<PricePanelParam>();
	private final static long serialVersionUID = 3871252231538103333L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public PricePanelAdditionalParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public PricePanelAdditionalParameter(String section, List<PricePanelParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<PricePanelParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<PricePanelParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

//-----------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class PricePanelParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -6775909530310167408L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public PricePanelParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public PricePanelParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}