package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name" })
public class FcOperationMode implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	private final static long serialVersionUID = -6309602923322543737L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public FcOperationMode() {
	}

	/**
	 *
	 * @param name
	 * @param id
	 */
	public FcOperationMode(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).toString();
	}

}