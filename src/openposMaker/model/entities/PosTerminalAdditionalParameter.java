package openposMaker.model.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
public class PosTerminalAdditionalParameter implements Serializable {
	
	public static final Map<String, String> LIST_DEFAULT_ADDITIONAL_PARAMETERS;
	
	static {
		LIST_DEFAULT_ADDITIONAL_PARAMETERS = new HashMap<String, String>();
		LIST_DEFAULT_ADDITIONAL_PARAMETERS.put("OperMismoTurno", "1");
		LIST_DEFAULT_ADDITIONAL_PARAMETERS.put("CtrlMargenCierreTurno", "1");
		LIST_DEFAULT_ADDITIONAL_PARAMETERS.put("TicketObligatorio", "1");
		LIST_DEFAULT_ADDITIONAL_PARAMETERS.put("RegistroElectronico", "1");
		LIST_DEFAULT_ADDITIONAL_PARAMETERS.put("Trace", "A");
	}

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = 3377029830769966434L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public PosTerminalAdditionalParameter() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public PosTerminalAdditionalParameter(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}
