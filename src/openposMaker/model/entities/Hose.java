package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"openposId",
"tankId",
"totalType"
})
public class Hose implements Serializable
{

@JsonProperty("id")
private Integer id;
@JsonProperty("openposId")
private Integer openposId;
@JsonProperty("tankId")
private Integer tankId;
@JsonProperty("totalType")
private String totalType;
private final static long serialVersionUID = 2746522532014302956L;

/**
* No args constructor for use in serialization
*
*/
public Hose() {
}

/**
*
* @param tankId
* @param totalType
* @param id
* @param openposId
*/
public Hose(Integer id, Integer openposId, Integer tankId, String totalType) {
super();
this.id = id;
this.openposId = openposId;
this.tankId = tankId;
this.totalType = totalType;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("openposId")
public Integer getOpenposId() {
return openposId;
}

@JsonProperty("openposId")
public void setOpenposId(Integer openposId) {
this.openposId = openposId;
}

@JsonProperty("tankId")
public Integer getTankId() {
return tankId;
}

@JsonProperty("tankId")
public void setTankId(Integer tankId) {
this.tankId = tankId;
}

@JsonProperty("totalType")
public String getTotalType() {
return totalType;
}

@JsonProperty("totalType")
public void setTotalType(String totalType) {
this.totalType = totalType;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("id", id).append("openposId", openposId).append("tankId", tankId).append("totalType", totalType).toString();
}

}