package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"code",
"name",
"decimalPositions",
"value"
})
public class SecondaryCurrency implements Serializable
{

@JsonProperty("code")
private String code;
@JsonProperty("name")
private String name;
@JsonProperty("decimalPositions")
private Integer decimalPositions;
@JsonProperty("value")
private Integer value;
private final static long serialVersionUID = 4367181362636062307L;

/**
* No args constructor for use in serialization
*
*/
public SecondaryCurrency() {
}

/**
*
* @param code
* @param name
* @param value
* @param decimalPositions
*/
public SecondaryCurrency(String code, String name, Integer decimalPositions, Integer value) {
super();
this.code = code;
this.name = name;
this.decimalPositions = decimalPositions;
this.value = value;
}

@JsonProperty("code")
public String getCode() {
return code;
}

@JsonProperty("code")
public void setCode(String code) {
this.code = code;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("decimalPositions")
public Integer getDecimalPositions() {
return decimalPositions;
}

@JsonProperty("decimalPositions")
public void setDecimalPositions(Integer decimalPositions) {
this.decimalPositions = decimalPositions;
}

@JsonProperty("value")
public Integer getValue() {
return value;
}

@JsonProperty("value")
public void setValue(Integer value) {
this.value = value;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("code", code).append("name", name).append("decimalPositions", decimalPositions).append("value", value).toString();
}

}