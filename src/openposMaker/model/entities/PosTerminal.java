package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "posCode", "controlPumps", "controlAuth", "documents", "peripherals", "openShiftTimeSlots",
		"additionalParameters" })
public class PosTerminal implements Serializable {

	@JsonProperty("posCode")
	private String posCode;
	@JsonProperty("controlPumps")
	private String controlPumps;
	@JsonProperty("controlAuth")
	private String controlAuth;
	@JsonProperty("documents")
	private List<Document> documents = new ArrayList<Document>();
	@JsonProperty("peripherals")
	private List<Peripheral> peripherals = new ArrayList<Peripheral>();
	@JsonProperty("openShiftTimeSlots")
	private List<OpenShiftTimeSlot> openShiftTimeSlots = new ArrayList<OpenShiftTimeSlot>();
	@JsonProperty("additionalParameters")
	private List<PosTerminalAdditionalParameter> additionalParameters = new ArrayList<PosTerminalAdditionalParameter>();
	private final static long serialVersionUID = -90746787777085075L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public PosTerminal() {
	}

	/**
	 *
	 * @param openShiftTimeSlots
	 * @param peripherals
	 * @param documents
	 * @param posCode
	 * @param additionalParameters
	 * @param controlPumps
	 * @param controlAuth
	 */
	public PosTerminal(String posCode, String controlPumps, String controlAuth) {
		this.posCode = posCode;
		this.controlPumps = controlPumps;
		this.controlAuth = controlAuth;
	}
	
	public PosTerminal(String posCode, String controlPumps, String controlAuth, List<Document> documents,
			List<Peripheral> peripherals, List<OpenShiftTimeSlot> openShiftTimeSlots,
			List<PosTerminalAdditionalParameter> additionalParameters) {
		this(posCode, controlPumps, controlAuth);
		this.documents = documents;
		this.peripherals = peripherals;
		this.openShiftTimeSlots = openShiftTimeSlots;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("posCode")
	public String getPosCode() {
		return posCode;
	}

	@JsonProperty("posCode")
	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}

	@JsonProperty("controlPumps")
	public String getControlPumps() {
		return controlPumps;
	}

	@JsonProperty("controlPumps")
	public void setControlPumps(String controlPumps) {
		this.controlPumps = controlPumps;
	}

	@JsonProperty("controlAuth")
	public String getControlAuth() {
		return controlAuth;
	}

	@JsonProperty("controlAuth")
	public void setControlAuth(String controlAuth) {
		this.controlAuth = controlAuth;
	}

	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@JsonProperty("peripherals")
	public List<Peripheral> getPeripherals() {
		return peripherals;
	}

	@JsonProperty("peripherals")
	public void setPeripherals(List<Peripheral> peripherals) {
		this.peripherals = peripherals;
	}

	@JsonProperty("openShiftTimeSlots")
	public List<OpenShiftTimeSlot> getOpenShiftTimeSlots() {
		return openShiftTimeSlots;
	}

	@JsonProperty("openShiftTimeSlots")
	public void setOpenShiftTimeSlots(List<OpenShiftTimeSlot> openShiftTimeSlots) {
		this.openShiftTimeSlots = openShiftTimeSlots;
	}

	@JsonProperty("additionalParameters")
	public List<PosTerminalAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<PosTerminalAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}
	
	public PosTerminal clone() {
		PosTerminal clonePosTerminal = new PosTerminal();
		clonePosTerminal.setPosCode(this.getPosCode());
		clonePosTerminal.setControlPumps(this.getControlPumps());
		clonePosTerminal.setControlAuth(this.getControlAuth());
		clonePosTerminal.setDocuments(this.getDocuments());
		clonePosTerminal.setPeripherals(this.getPeripherals());
		clonePosTerminal.setOpenShiftTimeSlots(this.getOpenShiftTimeSlots());
		clonePosTerminal.setAdditionalParameters(this.getAdditionalParameters());
		return clonePosTerminal;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("posCode", posCode).append("controlPumps", controlPumps)
				.append("controlAuth", controlAuth).append("documents", documents).append("peripherals", peripherals)
				.append("openShiftTimeSlots", openShiftTimeSlots).append("additionalParameters", additionalParameters)
				.toString();
	}

}


