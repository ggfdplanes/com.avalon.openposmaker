package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "file", "sections" })
public class AdditionalParameter implements Serializable {

	@JsonProperty("file")
	private String file;
	@JsonProperty("sections")
	private List<Section> sections = new ArrayList<Section>();
	private final static long serialVersionUID = -3577063115883282499L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public AdditionalParameter() {
	}

	/**
	 *
	 * @param file
	 * @param sections
	 */
	public AdditionalParameter(String file) {
		super();
		this.file = file;
	}

	@JsonProperty("file")
	public String getFile() {
		return file;
	}

	@JsonProperty("file")
	public void setFile(String file) {
		this.file = file;
	}

	@JsonProperty("sections")
	public List<Section> getSections() {
		return sections;
	}

	@JsonProperty("sections")
	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("file", file).append("sections", sections).toString();
	}

}




