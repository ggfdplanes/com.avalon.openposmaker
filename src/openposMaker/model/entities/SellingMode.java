package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "serviceModeId", "priceGroupId", "fuellingModeGroupId" })
public class SellingMode implements Serializable {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("serviceModeId")
	private Integer serviceModeId;
	@JsonProperty("priceGroupId")
	private Integer priceGroupId;
	@JsonProperty("fuellingModeGroupId")
	private Integer fuellingModeGroupId;
	private final static long serialVersionUID = 1785896313596355364L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public SellingMode() {
	}

	/**
	 *
	 * @param priceGroupId
	 * @param name
	 * @param id
	 * @param serviceModeId
	 * @param fuellingModeGroupId
	 */
	public SellingMode(Integer id, String name, Integer serviceModeId, Integer priceGroupId,
			Integer fuellingModeGroupId) {
		super();
		this.id = id;
		this.name = name;
		this.serviceModeId = serviceModeId;
		this.priceGroupId = priceGroupId;
		this.fuellingModeGroupId = fuellingModeGroupId;
	}

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("serviceModeId")
	public Integer getServiceModeId() {
		return serviceModeId;
	}

	@JsonProperty("serviceModeId")
	public void setServiceModeId(Integer serviceModeId) {
		this.serviceModeId = serviceModeId;
	}

	@JsonProperty("priceGroupId")
	public Integer getPriceGroupId() {
		return priceGroupId;
	}

	@JsonProperty("priceGroupId")
	public void setPriceGroupId(Integer priceGroupId) {
		this.priceGroupId = priceGroupId;
	}

	@JsonProperty("fuellingModeGroupId")
	public Integer getFuellingModeGroupId() {
		return fuellingModeGroupId;
	}

	@JsonProperty("fuellingModeGroupId")
	public void setFuellingModeGroupId(Integer fuellingModeGroupId) {
		this.fuellingModeGroupId = fuellingModeGroupId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("serviceModeId", serviceModeId)
				.append("priceGroupId", priceGroupId).append("fuellingModeGroupId", fuellingModeGroupId).toString();
	}

}