package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "name", "vatNumber", "leftHanded" })
public class Employee implements Serializable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("vatNumber")
	private String vatNumber;
	@JsonProperty("leftHanded")
	private String leftHanded;
	private final static long serialVersionUID = 1149652656459239719L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public Employee() {
	}

	/**
	 *
	 * @param name
	 * @param leftHanded
	 * @param id
	 * @param vatNumber
	 */
	public Employee(String id, String name, String vatNumber, String leftHanded) {
		super();
		this.id = id;
		this.name = name;
		this.vatNumber = vatNumber;
		this.leftHanded = leftHanded;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("vatNumber")
	public String getVatNumber() {
		return vatNumber;
	}

	@JsonProperty("vatNumber")
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	@JsonProperty("leftHanded")
	public String getLeftHanded() {
		return leftHanded;
	}

	@JsonProperty("leftHanded")
	public void setLeftHanded(String leftHanded) {
		this.leftHanded = leftHanded;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("vatNumber", vatNumber)
				.append("leftHanded", leftHanded).toString();
	}

}