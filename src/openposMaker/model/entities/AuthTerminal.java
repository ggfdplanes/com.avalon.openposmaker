package openposMaker.model.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "centerType", "address", "serialId", "posList", "cardTypesList", "documents", "peripherals",
		"memParameters", "additionalParameters" })
public class AuthTerminal implements Serializable {

	@JsonProperty("id")
	private String id;
	@JsonProperty("centerType")
	private String centerType;
	@JsonProperty("address")
	private String address;
	@JsonProperty("serialId")
	private String serialId;
	@JsonProperty("posList")
	private List<String> posList = new ArrayList<String>();
	@JsonProperty("cardTypesList")
	private List<CardTypes> cardTypesList = new ArrayList<CardTypes>();
	@JsonProperty("documents")
	private List<Document> documents = new ArrayList<Document>();
	@JsonProperty("peripherals")
	private List<Peripheral> peripherals = new ArrayList<Peripheral>();
	@JsonProperty("memParameters")
	private List<MemParameter> memParameters = new ArrayList<MemParameter>();
	@JsonProperty("additionalParameters")
	private List<AuthTerminalAdditionalParameter> additionalParameters = new ArrayList<AuthTerminalAdditionalParameter>();
	private final static long serialVersionUID = -3229999241272650963L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public AuthTerminal() {
	}

	/**
	 *
	 * @param address
	 * @param peripherals
	 * @param serialId
	 * @param documents
	 * @param cardTypesList
	 * @param memParameters
	 * @param additionalParameters
	 * @param id
	 * @param centerType
	 * @param posList
	 */
	public AuthTerminal(String id, String centerType, String address, String serialId, List<String> posList,
			List<CardTypes> cardTypesList, List<Document> documents, List<Peripheral> peripherals,
			List<MemParameter> memParameters, List<AuthTerminalAdditionalParameter> additionalParameters) {
		super();
		this.id = id;
		this.centerType = centerType;
		this.address = address;
		this.serialId = serialId;
		this.posList = posList;
		this.cardTypesList = cardTypesList;
		this.documents = documents;
		this.peripherals = peripherals;
		this.memParameters = memParameters;
		this.additionalParameters = additionalParameters;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("centerType")
	public String getCenterType() {
		return centerType;
	}

	@JsonProperty("centerType")
	public void setCenterType(String centerType) {
		this.centerType = centerType;
	}

	@JsonProperty("address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("serialId")
	public String getSerialId() {
		return serialId;
	}

	@JsonProperty("serialId")
	public void setSerialId(String serialId) {
		this.serialId = serialId;
	}

	@JsonProperty("posList")
	public List<String> getPosList() {
		return posList;
	}

	@JsonProperty("posList")
	public void setPosList(List<String> posList) {
		this.posList = posList;
	}

	@JsonProperty("cardTypesList")
	public List<CardTypes> getCardTypesList() {
		return cardTypesList;
	}

	@JsonProperty("cardTypesList")
	public void setCardTypesList(List<CardTypes> cardTypesList) {
		this.cardTypesList = cardTypesList;
	}

	@JsonProperty("documents")
	public List<Document> getDocuments() {
		return documents;
	}

	@JsonProperty("documents")
	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	@JsonProperty("peripherals")
	public List<Peripheral> getPeripherals() {
		return peripherals;
	}

	@JsonProperty("peripherals")
	public void setPeripherals(List<Peripheral> peripherals) {
		this.peripherals = peripherals;
	}

	@JsonProperty("memParameters")
	public List<MemParameter> getMemParameters() {
		return memParameters;
	}

	@JsonProperty("memParameters")
	public void setMemParameters(List<MemParameter> memParameters) {
		this.memParameters = memParameters;
	}

	@JsonProperty("additionalParameters")
	public List<AuthTerminalAdditionalParameter> getAdditionalParameters() {
		return additionalParameters;
	}

	@JsonProperty("additionalParameters")
	public void setAdditionalParameters(List<AuthTerminalAdditionalParameter> additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("centerType", centerType).append("address", address)
				.append("serialId", serialId).append("posList", posList).append("cardTypesList", cardTypesList)
				.append("documents", documents).append("peripherals", peripherals)
				.append("memParameters", memParameters).append("additionalParameters", additionalParameters).toString();
	}

}


// -------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "function", "cardTypes" })
class CardTypes implements Serializable {

	@JsonProperty("function")
	private Integer function;
	@JsonProperty("cardTypes")
	private List<String> cardTypes = new ArrayList<String>();
	private final static long serialVersionUID = 5894314851886090683L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public CardTypes() {
	}

	/**
	 *
	 * @param cardTypes
	 * @param function
	 */
	public CardTypes(Integer function, List<String> cardTypes) {
		super();
		this.function = function;
		this.cardTypes = cardTypes;
	}

	@JsonProperty("function")
	public Integer getFunction() {
		return function;
	}

	@JsonProperty("function")
	public void setFunction(Integer function) {
		this.function = function;
	}

	@JsonProperty("cardTypes")
	public List<String> getCardTypes() {
		return cardTypes;
	}

	@JsonProperty("cardTypes")
	public void setCardTypes(List<String> cardTypes) {
		this.cardTypes = cardTypes;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("function", function).append("cardTypes", cardTypes).toString();
	}

}

// ---------------------------------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class MemParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<MemParameterParam> params = new ArrayList<MemParameterParam>();
	private final static long serialVersionUID = 2321460376411718570L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public MemParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public MemParameter(String section, List<MemParameterParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<MemParameterParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<MemParameterParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

//-------------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class MemParameterParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = -6775909530310167408L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public MemParameterParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public MemParameterParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}

//----------------------------------


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "section", "params" })
class AuthTerminalAdditionalParameter implements Serializable {

	@JsonProperty("section")
	private String section;
	@JsonProperty("params")
	private List<AdditionalParameterParam> params = new ArrayList<AdditionalParameterParam>();
	private final static long serialVersionUID = 6721831353758702700L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public AuthTerminalAdditionalParameter() {
	}

	/**
	 *
	 * @param section
	 * @param params
	 */
	public AuthTerminalAdditionalParameter(String section, List<AdditionalParameterParam> params) {
		super();
		this.section = section;
		this.params = params;
	}

	@JsonProperty("section")
	public String getSection() {
		return section;
	}

	@JsonProperty("section")
	public void setSection(String section) {
		this.section = section;
	}

	@JsonProperty("params")
	public List<AdditionalParameterParam> getParams() {
		return params;
	}

	@JsonProperty("params")
	public void setParams(List<AdditionalParameterParam> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("section", section).append("params", params).toString();
	}

}

//----------------------------------

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "value" })
class AdditionalParameterParam implements Serializable {

	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
	private final static long serialVersionUID = 9151850607549490273L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public AdditionalParameterParam() {
	}

	/**
	 *
	 * @param name
	 * @param value
	 */
	public AdditionalParameterParam(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("value")
	public String getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("value", value).toString();
	}

}
