package openposMaker.model.entities;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"mainCurrency",
"secondaryCurrency"
})
public class CurrencyInfo implements Serializable
{

@JsonProperty("mainCurrency")
private MainCurrency mainCurrency;
@JsonProperty("secondaryCurrency")
private SecondaryCurrency secondaryCurrency;
private final static long serialVersionUID = -8522582876945612190L;

/**
* No args constructor for use in serialization
*
*/
public CurrencyInfo() {
	this.mainCurrency = new MainCurrency();
	this.secondaryCurrency = new SecondaryCurrency();
}

/**
*
* @param mainCurrency
* @param secondaryCurrency
*/
public CurrencyInfo(MainCurrency mainCurrency, SecondaryCurrency secondaryCurrency) {
super();
this.mainCurrency = mainCurrency;
this.secondaryCurrency = secondaryCurrency;
}

@JsonProperty("mainCurrency")
public MainCurrency getMainCurrency() {
return mainCurrency;
}

@JsonProperty("mainCurrency")
public void setMainCurrency(MainCurrency mainCurrency) {
this.mainCurrency = mainCurrency;
}

@JsonProperty("secondaryCurrency")
public SecondaryCurrency getSecondaryCurrency() {
return secondaryCurrency;
}

@JsonProperty("secondaryCurrency")
public void setSecondaryCurrency(SecondaryCurrency secondaryCurrency) {
this.secondaryCurrency = secondaryCurrency;
}

@Override
public String toString() {
return new ToStringBuilder(this).append("mainCurrency", mainCurrency).append("secondaryCurrency", secondaryCurrency).toString();
}

}
