package openposMaker.model.entities;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "prompt" })

public class CardTypeDataList implements Serializable {

	public static final String TYPE_ODOMETER = "odometer";
	public static final String TYPE_LICENSE = "license";
	
	public static final String PROMPT_ODOMETER = "KILOMETROS";
	public static final String PROMPT_LICENSE = "MATRICULA";
	
	
	@JsonProperty("type")
	private String type;
	@JsonProperty("prompt")
	private String prompt;
	private final static long serialVersionUID = -7311658750535307256L;

	/**
	 * No args constructor for use in serialization
	 *
	 */
	public CardTypeDataList() {
	}

	/**
	 *
	 * @param type
	 * @param prompt
	 */
	public CardTypeDataList(String type, String prompt) {
		super();
		this.type = type;
		this.prompt = prompt;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("prompt")
	public String getPrompt() {
		return prompt;
	}

	@JsonProperty("prompt")
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("type", type).append("prompt", prompt).toString();
	}

}