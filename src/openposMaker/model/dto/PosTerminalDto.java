package openposMaker.model.dto;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class PosTerminalDto {
	
	public static final String[] LIST_PRINTER_PROCESS = {"LpCli","JavaPOS"};
	public static final String[] LIST_INVOICE_FORMATS = {"Ticket","Bandeja", "L�ser"};

	private SimpleStringProperty initialCode;
	private SimpleStringProperty maxNumber;
	private SimpleStringProperty initialIP;
	private SimpleStringProperty printerProcess;
	private SimpleStringProperty invoiceFormat;
	private SimpleStringProperty closeDocFormat;
	private SimpleBooleanProperty controlPumps;
	private SimpleBooleanProperty controlAuth;
	private SimpleBooleanProperty visor;
	private SimpleBooleanProperty drawer;
	
	
	public PosTerminalDto() {
		this.initialCode = new SimpleStringProperty("001");
		this.maxNumber = new SimpleStringProperty("1");
		this.initialIP = new SimpleStringProperty("70");
		this.printerProcess = new SimpleStringProperty(LIST_PRINTER_PROCESS[1]);
		this.invoiceFormat = new SimpleStringProperty(LIST_INVOICE_FORMATS[1]);
		this.closeDocFormat = new SimpleStringProperty(LIST_INVOICE_FORMATS[1]);
		this.controlPumps = new SimpleBooleanProperty(true);
		this.controlAuth = new SimpleBooleanProperty(true);
		this.drawer = new SimpleBooleanProperty(true);
		this.visor = new SimpleBooleanProperty(true);
	}

	public SimpleStringProperty getInitialCode() {
		return initialCode;
	}

	public void setInitialCode(SimpleStringProperty initialCode) {
		this.initialCode = initialCode;
	}

	public SimpleStringProperty getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(SimpleStringProperty maxNumber) {
		this.maxNumber = maxNumber;
	}

	public SimpleStringProperty getInitialIP() {
		return initialIP;
	}

	public void setInitialIP(SimpleStringProperty initialIP) {
		this.initialIP = initialIP;
	}

	public SimpleStringProperty getPrinterProcess() {
		return printerProcess;
	}

	public void setPrinterProcess(SimpleStringProperty printerProcess) {
		this.printerProcess = printerProcess;
	}

	public SimpleStringProperty getInvoiceFormat() {
		return invoiceFormat;
	}

	public void setInvoiceFormat(SimpleStringProperty invoiceFormat) {
		this.invoiceFormat = invoiceFormat;
	}
	
	

	public SimpleStringProperty getCloseDocFormat() {
		return closeDocFormat;
	}

	public void setCloseDocFormat(SimpleStringProperty closeDocFormat) {
		this.closeDocFormat = closeDocFormat;
	}

	public SimpleBooleanProperty getControlPumps() {
		return controlPumps;
	}

	public void setControlPumps(SimpleBooleanProperty controlPumps) {
		this.controlPumps = controlPumps;
	}

	public SimpleBooleanProperty getControlAuth() {
		return controlAuth;
	}

	public void setControlAuth(SimpleBooleanProperty controlAuth) {
		this.controlAuth = controlAuth;
	}

	public SimpleBooleanProperty getVisor() {
		return visor;
	}

	public void setVisor(SimpleBooleanProperty visor) {
		this.visor = visor;
	}

	public SimpleBooleanProperty getDrawer() {
		return drawer;
	}

	public void setDrawer(SimpleBooleanProperty drawer) {
		this.drawer = drawer;
	}
	
	
}
