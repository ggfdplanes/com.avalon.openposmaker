package openposMaker.model.dto;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import openposMaker.model.entities.SiteInfo;

public class SiteInfoDto {

	private SimpleStringProperty code;
	private SimpleStringProperty vatNumber;
	private SimpleStringProperty name;
	private SimpleStringProperty address;
	private SimpleStringProperty postCode;
	private SimpleStringProperty city;
	private SimpleStringProperty province;
	private SimpleStringProperty greetingFirstLine;
	private SimpleStringProperty greetingSecondLine;
	private SimpleStringProperty offerFamily;
	private SimpleStringProperty serviceFamily;
	private SimpleStringProperty discountItem;
	private SimpleStringProperty numberOfShifts;
	SimpleBooleanProperty commissionist;

	public SiteInfoDto() {
		this.code = new SimpleStringProperty();
		this.vatNumber = new SimpleStringProperty();
		this.name = new SimpleStringProperty();
		this.address = new SimpleStringProperty();
		this.postCode = new SimpleStringProperty();
		this.city = new SimpleStringProperty();
		this.province = new SimpleStringProperty();
		this.greetingFirstLine = new SimpleStringProperty();
		this.greetingSecondLine = new SimpleStringProperty();
		this.offerFamily = new SimpleStringProperty();
		this.serviceFamily = new SimpleStringProperty();
		this.discountItem = new SimpleStringProperty();
		this.numberOfShifts = new SimpleStringProperty("3");
		this.commissionist = new SimpleBooleanProperty(false);	
	}

	public SiteInfoDto(SiteInfo siteInfo) {
		this();
		this.code.set(siteInfo.getSiteCode());
		this.vatNumber.set(siteInfo.getVatNumber());
		this.name.set(siteInfo.getSiteName());
		this.address.set(siteInfo.getAddress());
		this.postCode.set(siteInfo.getPostCode());
		this.city.set(siteInfo.getCity());
		this.province.set(siteInfo.getProvince());
		this.greetingFirstLine.set(siteInfo.getGreetingFirstLine());
		this.greetingSecondLine.set(siteInfo.getGreetingSecondLine());
		this.offerFamily.set(siteInfo.getOfferFamily());
		this.serviceFamily.set(siteInfo.getServiceFamily());
		this.discountItem.set(siteInfo.getDiscountItem());
		this.numberOfShifts.set(siteInfo.getNumberOfShifts()+"");
		
		this.commissionist.set(siteInfo.getCommissionist().equals("yes") ? true : false);
	}

	public SimpleStringProperty getCode() {
		return code;
	}

	public void setCode(SimpleStringProperty code) {
		this.code = code;
	}

	public SimpleStringProperty getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(SimpleStringProperty vatNumber) {
		this.vatNumber = vatNumber;
	}

	public SimpleStringProperty getName() {
		return name;
	}

	public void setName(SimpleStringProperty name) {
		this.name = name;
	}

	public SimpleStringProperty getAddress() {
		return address;
	}

	public void setAddress(SimpleStringProperty address) {
		this.address = address;
	}

	public SimpleStringProperty getPostCode() {
		return postCode;
	}

	public void setPostCode(SimpleStringProperty postCode) {
		this.postCode = postCode;
	}

	public SimpleStringProperty getCity() {
		return city;
	}

	public void setCity(SimpleStringProperty city) {
		this.city = city;
	}

	public SimpleStringProperty getProvince() {
		return province;
	}

	public void setProvince(SimpleStringProperty province) {
		this.province = province;
	}

	public SimpleStringProperty getGreetingFirstLine() {
		return greetingFirstLine;
	}

	public void setGreetingFirstLine(SimpleStringProperty greetingFirstLine) {
		this.greetingFirstLine = greetingFirstLine;
	}

	public SimpleStringProperty getGreetingSecondLine() {
		return greetingSecondLine;
	}

	public void setGreetingSecondLine(SimpleStringProperty greetingSecondLine) {
		this.greetingSecondLine = greetingSecondLine;
	}

	public SimpleStringProperty getOfferFamily() {
		return offerFamily;
	}

	public void setOfferFamily(SimpleStringProperty offerFamily) {
		this.offerFamily = offerFamily;
	}

	public SimpleStringProperty getServiceFamily() {
		return serviceFamily;
	}

	public void setServiceFamily(SimpleStringProperty serviceFamily) {
		this.serviceFamily = serviceFamily;
	}

	public SimpleStringProperty getDiscountItem() {
		return discountItem;
	}

	public void setDiscountItem(SimpleStringProperty discountItem) {
		this.discountItem = discountItem;
	}

	public SimpleStringProperty getNumberOfShifts() {
		return numberOfShifts;
	}

	public void setNumberOfShifts(SimpleStringProperty numberOfShifts) {
		this.numberOfShifts = numberOfShifts;
	}

	public SimpleBooleanProperty getCommissionist() {
		return commissionist;
	}

	public void setCommissionist(SimpleBooleanProperty commissionist) {
		this.commissionist = commissionist;
	}

}
