package openposMaker.model.dto;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import openposMaker.model.entities.Incidence;

public class IncidenceDto {

//	private Incidence incidence;
	private SimpleStringProperty code;
	private SimpleStringProperty name;
	private SimpleStringProperty type;
	private SimpleBooleanProperty modifiable;
	private SimpleStringProperty account;
	private SimpleStringProperty accountingConcept;

	public IncidenceDto() {
		code = new SimpleStringProperty();
		name = new SimpleStringProperty();
		type = new SimpleStringProperty();
		modifiable = new SimpleBooleanProperty(true);
		account = new SimpleStringProperty();
		accountingConcept = new SimpleStringProperty();
		this.setIncidence(new Incidence("", "", "", "", "yes", "", ""));
//		incidence = new Incidence();
		
	}

//	public Incidence getIncidence() {
//		return incidence;
//	}

	public void setIncidence(Incidence incidence) {
		this.setCode(incidence.getCode());
		this.setName(incidence.getName());
		switch (incidence.getUse()) {
		case "cashMovement":
			if (incidence.getType().equals("+")) {
				this.setType(Incidence.INCIDENCE_TYPE_LIST[2]);
			} else {
				this.setType(Incidence.INCIDENCE_TYPE_LIST[1]);
				if (incidence.getAccountingConcept().equals("R")) {
					this.setType(Incidence.INCIDENCE_TYPE_LIST[0]);
				}
			}
			break;
		case "meanOfPayment":
			if (incidence.getType().equals("H")) {
				this.setType(Incidence.INCIDENCE_TYPE_LIST[3]);
			} else {
				this.setType(Incidence.INCIDENCE_TYPE_LIST[4]);
			}
			break;
		}
		this.setModifiable(incidence.getModifiable().equals("yes") ? true : false);
		this.setAccount(incidence.getAccount());
		this.setAccountingConcept(incidence.getAccountingConcept());
	}

	public SimpleStringProperty getCode() {
		return code;
	}

	public void setCode(String code) {
		if (code == null) {
			code = "";
		}
		this.code.set(code);
	}

	public SimpleStringProperty getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			name = "";
		}
		this.name.set(name);
	}

	public SimpleStringProperty getType() {
		return type;
	}

	public void setType(String type) {
		if (type != null) 
			this.type.set(type);
	}

	public SimpleBooleanProperty getModifiable() {
		return this.modifiable;
	}

	public void setModifiable(Boolean modifiable) {
		if (modifiable== null) {
			modifiable = true;
		}
		this.modifiable.set(modifiable);

	}

	public SimpleStringProperty getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		if (account == null) {
			account = "";
		}
		this.account.set(account);
	}

	public SimpleStringProperty getAccountingConcept() {
		return accountingConcept;
	}

	public void setAccountingConcept(String accountingConcept) {
		if (accountingConcept == null) {
			accountingConcept = "";
		}
		this.accountingConcept.set(accountingConcept);
	}

}
