package openposMaker.model.dto;

import javafx.beans.property.SimpleStringProperty;


public class EmployeeDto {

	private SimpleStringProperty codePrefix;
	private SimpleStringProperty initNumber;
	private SimpleStringProperty maxNumber;
	
	


	public EmployeeDto() {
		this.codePrefix = new SimpleStringProperty("001");
		this.initNumber = new SimpleStringProperty("1");
		this.maxNumber = new SimpleStringProperty("10");
	}
	
	public EmployeeDto(String siteCode) {
		this();
		this.getCodePrefix().set(siteCode);
	}

	public SimpleStringProperty getCodePrefix() {
		return codePrefix;
	}

	public void setCodePrefix(SimpleStringProperty formPref) {
		this.codePrefix = formPref;
	}

	public SimpleStringProperty getInitNumber() {
		return initNumber;
	}

	public void setInitNumber(SimpleStringProperty initNumber) {
		this.initNumber = initNumber;
	}

	public SimpleStringProperty getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(SimpleStringProperty maxNumber) {
		this.maxNumber = maxNumber;
	}

}
