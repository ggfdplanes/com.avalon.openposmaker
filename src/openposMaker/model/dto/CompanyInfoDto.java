package openposMaker.model.dto;

import javafx.beans.property.SimpleStringProperty;
import openposMaker.model.entities.CompanyInfo;
import openposMaker.model.entities.CurrencyInfo;

public class CompanyInfoDto {



	private SimpleStringProperty companyCode;
	private SimpleStringProperty shortName;
	private SimpleStringProperty longName;
	private SimpleStringProperty addressType;
	private SimpleStringProperty address;
	private SimpleStringProperty addressNumber;
	private SimpleStringProperty postCode;
	private SimpleStringProperty city;
	private SimpleStringProperty mainCurrencyCode;
	private SimpleStringProperty mainCurrencyName;
	private SimpleStringProperty mainCurrencyDecimalPositions;
	private SimpleStringProperty secondaryCurrencyCode;
	private SimpleStringProperty secondaryCurrencyName;
	private SimpleStringProperty secondaryCurrencyDecimalPositions;
	private SimpleStringProperty secondaryCurrencyValue;
	private SimpleStringProperty phoneNumber;

	public CompanyInfoDto() {
		companyCode = new SimpleStringProperty();
		shortName = new SimpleStringProperty();
		longName = new SimpleStringProperty();
		addressType = new SimpleStringProperty();
		address = new SimpleStringProperty();
		addressNumber = new SimpleStringProperty();
		postCode = new SimpleStringProperty();
		city = new SimpleStringProperty();
		phoneNumber = new SimpleStringProperty();
		mainCurrencyCode = new SimpleStringProperty();
		mainCurrencyName = new SimpleStringProperty();
		mainCurrencyDecimalPositions = new SimpleStringProperty();
		secondaryCurrencyCode = new SimpleStringProperty();
		secondaryCurrencyName = new SimpleStringProperty();
		secondaryCurrencyDecimalPositions = new SimpleStringProperty();
		secondaryCurrencyValue = new SimpleStringProperty();
	}

	public CompanyInfoDto(CompanyInfo company) {
		this();
		this.setCompanyCode(company.getCompanyCode());
		this.setShortName(company.getShortName());
		this.setLongName(company.getLongName());
		this.setAddressType(company.getAddressType());
		this.setAddress(company.getAddress());
		this.setAddressNumber(company.getAddressNumber());
		this.setPostCode(company.getPostCode());
		this.setCity(company.getCity());
		this.setPhoneNumber(company.getPhoneNumber());
		this.getMainCurrencyCode().set(company.getCurrencyInfo().getMainCurrency().getCode());
		this.getMainCurrencyName().set(company.getCurrencyInfo().getMainCurrency().getName());
		this.getMainCurrencyDecimalPositions()
				.set(company.getCurrencyInfo().getMainCurrency().getDecimalPositions() + "");
		this.getSecondaryCurrencyCode().set(company.getCurrencyInfo().getSecondaryCurrency().getCode());
		this.getSecondaryCurrencyName().set(company.getCurrencyInfo().getSecondaryCurrency().getName());
		this.getSecondaryCurrencyDecimalPositions()
				.set(company.getCurrencyInfo().getSecondaryCurrency().getDecimalPositions() + "");
		this.getSecondaryCurrencyValue().set(company.getCurrencyInfo().getSecondaryCurrency().getValue() + "");
	
	}



	public SimpleStringProperty getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode.set(companyCode);
	}

	public SimpleStringProperty getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName.set(shortName);
	}

	public SimpleStringProperty getLongName() {
		return longName;
	}

	public void setLongName(String longName) {
		this.longName.set(longName);
	}

	public SimpleStringProperty getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType.set(addressType);
	}

	public SimpleStringProperty getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address.set(address);
	}

	public SimpleStringProperty getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber.set(addressNumber);
	}

	public SimpleStringProperty getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode.set(postCode);
	}

	public SimpleStringProperty getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city.set(city);
	}

	public SimpleStringProperty getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber.set(phoneNumber);
	}

	public SimpleStringProperty getMainCurrencyCode() {
		return mainCurrencyCode;
	}

	public void setMainCurrencyCode(SimpleStringProperty mainCurrencyCode) {
		this.mainCurrencyCode = mainCurrencyCode;
	}

	public SimpleStringProperty getMainCurrencyName() {
		return mainCurrencyName;
	}

	public void setMainCurrencyName(SimpleStringProperty mainCurrencyName) {
		this.mainCurrencyName = mainCurrencyName;
	}

	public SimpleStringProperty getMainCurrencyDecimalPositions() {
		return mainCurrencyDecimalPositions;
	}

	public void setMainCurrencyDecimalPositions(SimpleStringProperty mainCurrencyDecimalPositions) {
		this.mainCurrencyDecimalPositions = mainCurrencyDecimalPositions;
	}

	public SimpleStringProperty getSecondaryCurrencyCode() {
		return secondaryCurrencyCode;
	}

	public void setSecondaryCurrencyCode(SimpleStringProperty secondaryCurrencyCode) {
		this.secondaryCurrencyCode = secondaryCurrencyCode;
	}

	public SimpleStringProperty getSecondaryCurrencyName() {
		return secondaryCurrencyName;
	}

	public void setSecondaryCurrencyName(SimpleStringProperty secondaryCurrencyName) {
		this.secondaryCurrencyName = secondaryCurrencyName;
	}

	public SimpleStringProperty getSecondaryCurrencyDecimalPositions() {
		return secondaryCurrencyDecimalPositions;
	}

	public void setSecondaryCurrencyDecimalPositions(SimpleStringProperty secondaryCurrencyDecimalPositions) {
		this.secondaryCurrencyDecimalPositions = secondaryCurrencyDecimalPositions;
	}

	public SimpleStringProperty getSecondaryCurrencyValue() {
		return secondaryCurrencyValue;
	}

	public void setSecondaryCurrencyValue(SimpleStringProperty secondaryCurrencyValue) {
		this.secondaryCurrencyValue = secondaryCurrencyValue;
	}

}