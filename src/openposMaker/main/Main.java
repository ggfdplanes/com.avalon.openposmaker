package openposMaker.main;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			String fxmlMainFilePath =  "/openposMaker/view/fxml/Main.fxml";
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlMainFilePath));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			
			String cssFilePath = "/openposMaker/res/css/application.css";
			scene.getStylesheets().add(getClass().getResource(cssFilePath).toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("OpenPOS Maker");
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
